import React, { useState } from 'react';
import {
  Typography,
  Grid,
  Paper,
  Button,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Alert,
  CircularProgress,
} from '@mui/material';
import { CloudUpload as UploadIcon } from '@mui/icons-material';
import heic2any from 'heic2any';
import FeaturePageTemplate from '@components/FeaturePageTemplate';

// Supported input formats
const INPUT_FORMATS = [
  'image/jpeg',
  'image/png',
  'image/gif',
  'image/bmp',
  'image/webp',
  'image/tiff',
  'image/heic',
  'image/heif',
];

// Output formats
const OUTPUT_FORMATS = [
  { value: 'image/jpeg', label: 'JPEG', extension: 'jpg' },
  { value: 'image/png', label: 'PNG', extension: 'png' },
  { value: 'image/webp', label: 'WebP', extension: 'webp' },
  { value: 'image/gif', label: 'GIF', extension: 'gif' },
  { value: 'image/bmp', label: 'BMP', extension: 'bmp' },
];

const ConvertImage = () => {
  const [selectedFile, setSelectedFile] = useState<File | null>(null);
  const [outputFormat, setOutputFormat] = useState(OUTPUT_FORMATS[0].value);
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);

  const handleFileUpload = async (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const file = event.target.files?.[0];
    if (!file) return;

    const fileType = file.type.toLowerCase();
    const isHeic =
      file.name.toLowerCase().endsWith('.heic') ||
      file.name.toLowerCase().endsWith('.heif');

    if (!INPUT_FORMATS.includes(fileType) && !isHeic) {
      setError('Unsupported file format. Please upload a valid image file.');
      return;
    }

    setSelectedFile(file);
    setError('');
  };

  const convertImage = async () => {
    if (!selectedFile) return;

    try {
      setLoading(true);
      setError('');

      let imageBlob: Blob | null = null;
      const isHeic =
        selectedFile.name.toLowerCase().endsWith('.heic') ||
        selectedFile.name.toLowerCase().endsWith('.heif');

      if (isHeic) {
        // Convert HEIC to JPEG first
        const convertedBlob = await heic2any({
          blob: selectedFile,
          toType: 'image/jpeg',
        });
        imageBlob = Array.isArray(convertedBlob)
          ? convertedBlob[0]
          : convertedBlob;
      } else {
        imageBlob = selectedFile;
      }

      // Create canvas for conversion
      const img = new Image();
      const canvas = document.createElement('canvas');
      const ctx = canvas.getContext('2d');

      await new Promise((resolve, reject) => {
        img.onload = () => {
          canvas.width = img.width;
          canvas.height = img.height;
          ctx?.drawImage(img, 0, 0);
          resolve(null);
        };
        img.onerror = () => reject(new Error('Failed to load image'));
        img.src = URL.createObjectURL(imageBlob as Blob);
      });

      // Convert to desired format
      const outputBlob = await new Promise<Blob>((resolve, reject) => {
        canvas.toBlob(
          (blob) => {
            if (blob) resolve(blob);
            else reject(new Error('Failed to convert image'));
          },
          outputFormat,
          0.9
        );
      });

      // Download the converted image
      const outputExtension =
        OUTPUT_FORMATS.find((format) => format.value === outputFormat)
          ?.extension || 'jpg';
      const link = document.createElement('a');
      link.href = URL.createObjectURL(outputBlob);
      link.download = `converted_image.${outputExtension}`;
      link.click();
      URL.revokeObjectURL(link.href);
    } catch (err) {
      console.error('Conversion error:', err);
      setError('Failed to convert image. Please try again.');
    } finally {
      setLoading(false);
    }
  };

  return (
    <FeaturePageTemplate
      title="Convert Image"
      description="Convert images between different formats including HEIC/HEIF"
    >
      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <Grid container spacing={4} justifyContent="center">
        <Grid item xs={12} md={6}>
          <Paper elevation={1} className="p-6">
            <div className="space-y-6">
              <input
                type="file"
                accept={INPUT_FORMATS.join(',')}
                onChange={handleFileUpload}
                className="hidden"
                id="image-upload"
              />
              <label htmlFor="image-upload">
                <Button
                  variant="outlined"
                  component="span"
                  startIcon={<UploadIcon />}
                  fullWidth
                >
                  Select Image
                </Button>
              </label>

              {selectedFile && (
                <Typography variant="body2" className="break-all">
                  Selected: {selectedFile.name}
                </Typography>
              )}

              <FormControl fullWidth>
                <InputLabel>Output Format</InputLabel>
                <Select
                  value={outputFormat}
                  label="Output Format"
                  onChange={(e) => setOutputFormat(e.target.value)}
                >
                  {OUTPUT_FORMATS.map((format) => (
                    <MenuItem key={format.value} value={format.value}>
                      {format.label}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>

              <Button
                variant="contained"
                onClick={convertImage}
                disabled={!selectedFile || loading}
                fullWidth
              >
                {loading ? (
                  <CircularProgress size={24} className="text-white" />
                ) : (
                  'Convert Image'
                )}
              </Button>
            </div>
          </Paper>
        </Grid>
      </Grid>
    </FeaturePageTemplate>
  );
};

export default ConvertImage;

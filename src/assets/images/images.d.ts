declare module '*.png' {
  const value: string;
  export default value;
}

declare module '*.jpg' {
  const value: string;
  export default value;
}

declare module '*.jpeg' {
  const value: string;
  export default value;
}

declare module '*.svg' {
  const value: string;
  export default value;
}

// Menu icons
declare const menuIcons: {
  videoTools: string;
  imageTools: string;
  ocrTools: string;
  pdfTools: string;
  codeFormatter: string;
  unitConverter: string;
  fileCompressor: string;
  colorPicker: string;
  timeConverter: string;
  translator: string;
  fileConverter: string;
  otherTools: string;
};

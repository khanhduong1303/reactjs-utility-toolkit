import React, { useState } from 'react';
import {
  Typography,
  Grid,
  Button,
  Box,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Alert,
} from '@mui/material';
import { CloudUpload as UploadIcon } from '@mui/icons-material';
import FeaturePageTemplate from '@components/FeaturePageTemplate';

const ResizeImage = () => {
  const [selectedImage, setSelectedImage] = useState<File | null>(null);
  const [previewUrl, setPreviewUrl] = useState<string>('');
  const [width, setWidth] = useState('');
  const [height, setHeight] = useState('');
  const [maintainAspectRatio, setMaintainAspectRatio] = useState(true);
  const [resizeMode, setResizeMode] = useState<'pixels' | 'percentage'>(
    'pixels'
  );
  const [error, setError] = useState('');

  const handleImageUpload = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0];
    if (file) {
      if (file.type.startsWith('image/')) {
        setSelectedImage(file);
        setError('');

        // Create preview
        const reader = new FileReader();
        reader.onloadend = () => {
          setPreviewUrl(reader.result as string);

          // Get original dimensions
          const img = new Image();
          img.onload = () => {
            setWidth(img.width.toString());
            setHeight(img.height.toString());
          };
          img.src = reader.result as string;
        };
        reader.readAsDataURL(file);
      } else {
        setError('Please upload a valid image file');
      }
    }
  };

  const handleResize = async () => {
    if (!selectedImage || !width || !height) return;

    try {
      const img = new Image();
      const canvas = document.createElement('canvas');
      const ctx = canvas.getContext('2d');
      if (!ctx) throw new Error('Could not get canvas context');

      await new Promise<void>((resolve, reject) => {
        img.onload = () => {
          let newWidth = parseInt(width);
          let newHeight = parseInt(height);

          if (resizeMode === 'percentage') {
            newWidth = (img.width * parseInt(width)) / 100;
            newHeight = (img.height * parseInt(height)) / 100;
          }

          canvas.width = newWidth;
          canvas.height = newHeight;

          ctx.drawImage(img, 0, 0, newWidth, newHeight);
          resolve();
        };
        img.onerror = reject;
        img.src = previewUrl;
      });

      canvas.toBlob((blob) => {
        if (blob) {
          const url = URL.createObjectURL(blob);
          const link = document.createElement('a');
          link.href = url;
          link.download = `resized_${selectedImage.name}`;
          link.click();
          URL.revokeObjectURL(url);
        }
      }, selectedImage.type);
    } catch (err) {
      setError('Failed to resize image. Please try again.');
      console.error('Resize error:', err);
    }
  };

  const handleDimensionChange = (
    value: string,
    dimension: 'width' | 'height'
  ) => {
    if (maintainAspectRatio && selectedImage && previewUrl) {
      const img = new Image();
      img.onload = () => {
        const aspectRatio = img.width / img.height;
        if (dimension === 'width') {
          setWidth(value);
          const newHeight =
            resizeMode === 'percentage'
              ? value
              : Math.round(parseInt(value) / aspectRatio).toString();
          setHeight(newHeight);
        } else {
          setHeight(value);
          const newWidth =
            resizeMode === 'percentage'
              ? value
              : Math.round(parseInt(value) * aspectRatio).toString();
          setWidth(newWidth);
        }
      };
      img.src = previewUrl;
    } else {
      if (dimension === 'width') {
        setWidth(value);
      } else {
        setHeight(value);
      }
    }
  };

  return (
    <FeaturePageTemplate
      title="Resize Image"
      description="Resize your images while maintaining quality"
    >
      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <Grid container spacing={4}>
        <Grid item xs={12} md={6}>
          <Box className="space-y-4">
            <FormControl fullWidth>
              <InputLabel>Resize Mode</InputLabel>
              <Select
                value={resizeMode}
                onChange={(e) =>
                  setResizeMode(e.target.value as 'pixels' | 'percentage')
                }
                label="Resize Mode"
              >
                <MenuItem value="pixels">Pixels</MenuItem>
                <MenuItem value="percentage">Percentage</MenuItem>
              </Select>
            </FormControl>

            <Grid container spacing={2}>
              <Grid item xs={6}>
                <TextField
                  fullWidth
                  type="number"
                  label={resizeMode === 'pixels' ? 'Width (px)' : 'Width (%)'}
                  value={width}
                  onChange={(e) =>
                    handleDimensionChange(e.target.value, 'width')
                  }
                  disabled={!selectedImage}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  fullWidth
                  type="number"
                  label={resizeMode === 'pixels' ? 'Height (px)' : 'Height (%)'}
                  value={height}
                  onChange={(e) =>
                    handleDimensionChange(e.target.value, 'height')
                  }
                  disabled={!selectedImage}
                />
              </Grid>
            </Grid>

            <FormControl fullWidth>
              <div className="flex items-center space-x-2">
                <input
                  type="checkbox"
                  checked={maintainAspectRatio}
                  onChange={(e) => setMaintainAspectRatio(e.target.checked)}
                  id="aspect-ratio"
                  className="w-4 h-4"
                />
                <label htmlFor="aspect-ratio">Maintain aspect ratio</label>
              </div>
            </FormControl>

            <input
              type="file"
              accept="image/*"
              onChange={handleImageUpload}
              className="hidden"
              id="image-upload"
            />
            <label htmlFor="image-upload">
              <Button
                variant="outlined"
                component="span"
                startIcon={<UploadIcon />}
                fullWidth
              >
                Upload Image
              </Button>
            </label>

            {selectedImage && (
              <Button
                variant="contained"
                onClick={handleResize}
                fullWidth
                disabled={!width || !height}
              >
                Resize Image
              </Button>
            )}
          </Box>
        </Grid>

        <Grid item xs={12} md={6}>
          {previewUrl && (
            <Box className="bg-white p-4 rounded-lg">
              <Typography variant="h6" className="mb-4">
                Preview
              </Typography>
              <img
                src={previewUrl}
                alt="Preview"
                className="max-w-full h-auto rounded-lg"
              />
            </Box>
          )}
        </Grid>
      </Grid>
    </FeaturePageTemplate>
  );
};

export default ResizeImage;

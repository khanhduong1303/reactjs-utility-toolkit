declare module 'pdf-encrypt' {
  interface EncryptOptions {
    userPassword?: string;
    ownerPassword?: string;
    pdfVersion?: number;
    permissions?: {
      printing?: 'highResolution' | 'lowResolution' | false;
      modifying?: boolean;
      copying?: boolean;
      annotating?: boolean;
      fillingForms?: boolean;
      contentAccessibility?: boolean;
      documentAssembly?: boolean;
    };
  }

  interface PdfEncrypt {
    encrypt(
      pdfBuffer: Uint8Array | Buffer,
      options: EncryptOptions,
      callback: (error: Error | null, encryptedPdf: Buffer) => void
    ): void;
  }

  const pdfEncrypt: PdfEncrypt;
  export = pdfEncrypt;
}

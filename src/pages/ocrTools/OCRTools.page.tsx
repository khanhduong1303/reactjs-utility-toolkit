import React, { useState } from 'react';
import {
  Typography,
  Grid,
  Button,
  Box,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  LinearProgress,
  Alert,
} from '@mui/material';
import { CloudUpload as UploadIcon } from '@mui/icons-material';
import { createWorker } from 'tesseract.js';
import FeaturePageTemplate from '@components/FeaturePageTemplate';

const LANGUAGES = [
  { code: 'eng', name: 'English' },
  { code: 'fra', name: 'French' },
  { code: 'deu', name: 'German' },
  { code: 'spa', name: 'Spanish' },
  { code: 'ita', name: 'Italian' },
  { code: 'por', name: 'Portuguese' },
  { code: 'vie', name: 'Vietnamese' },
  { code: 'chi_sim', name: 'Chinese Simplified' },
  { code: 'chi_tra', name: 'Chinese Traditional' },
  { code: 'jpn', name: 'Japanese' },
  { code: 'kor', name: 'Korean' },
  { code: 'rus', name: 'Russian' },
  { code: 'ara', name: 'Arabic' },
  { code: 'hin', name: 'Hindi' },
];

const OCRTools = () => {
  const [selectedImage, setSelectedImage] = useState<File | null>(null);
  const [extractedText, setExtractedText] = useState('');
  const [isProcessing, setIsProcessing] = useState(false);
  const [progress, setProgress] = useState(0);
  const [selectedLanguage, setSelectedLanguage] = useState('eng');
  const [error, setError] = useState('');

  const handleImageUpload = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0];
    if (file) {
      if (file.type.startsWith('image/')) {
        setSelectedImage(file);
        setExtractedText('');
        setError('');
      } else {
        setError('Please upload a valid image file');
      }
    }
  };

  const extractText = async () => {
    if (!selectedImage) return;

    setIsProcessing(true);
    setProgress(0);
    setError('');

    try {
      const worker = await createWorker();
      (worker as any).logger = (m: any) => {
        if (m.progress !== undefined) {
          setProgress(Math.round(m.progress * 100));
        }
      };

      await (worker as any).loadLanguage(selectedLanguage);
      await (worker as any).initialize(selectedLanguage);

      const result = await (worker as any).recognize(selectedImage);
      setExtractedText(result.data.text);

      await (worker as any).terminate();
    } catch (error) {
      console.error('Error extracting text:', error);
      setError('Failed to extract text. Please try again.');
    } finally {
      setIsProcessing(false);
      setProgress(0);
    }
  };

  return (
    <FeaturePageTemplate
      title="OCR Tools"
      description="Extract text from images using OCR technology"
    >
      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <Grid container spacing={4}>
        <Grid item xs={12} md={6}>
          <Box className="glass-card p-6">
            <div className="space-y-4">
              <FormControl fullWidth>
                <InputLabel>Language</InputLabel>
                <Select
                  value={selectedLanguage}
                  onChange={(e) => setSelectedLanguage(e.target.value)}
                  label="Language"
                >
                  {LANGUAGES.map((lang) => (
                    <MenuItem key={lang.code} value={lang.code}>
                      {lang.name}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>

              <input
                type="file"
                accept="image/*"
                onChange={handleImageUpload}
                className="hidden"
                id="image-upload"
              />
              <label htmlFor="image-upload">
                <Button
                  variant="outlined"
                  component="span"
                  startIcon={<UploadIcon />}
                  fullWidth
                  className="mb-4"
                >
                  Upload Image
                </Button>
              </label>

              {selectedImage && (
                <div className="space-y-4">
                  <img
                    src={URL.createObjectURL(selectedImage)}
                    alt="Selected"
                    className="max-w-full h-auto rounded-lg"
                  />
                  <Button
                    variant="contained"
                    onClick={extractText}
                    disabled={isProcessing}
                    fullWidth
                  >
                    {isProcessing ? 'Processing...' : 'Extract Text'}
                  </Button>

                  {isProcessing && (
                    <div className="space-y-2">
                      <LinearProgress
                        variant="determinate"
                        value={progress}
                        className="rounded-full"
                      />
                      <Typography
                        variant="caption"
                        className="text-secondary-600 text-center block"
                      >
                        {progress}% Complete
                      </Typography>
                    </div>
                  )}
                </div>
              )}
            </div>
          </Box>
        </Grid>

        <Grid item xs={12} md={6}>
          <Box className="glass-card p-6 h-full">
            <Typography variant="h6" className="mb-4">
              Extracted Text
            </Typography>
            <div className="bg-white rounded-lg p-4 min-h-[200px] whitespace-pre-wrap overflow-auto">
              {extractedText || 'No text extracted yet'}
            </div>
          </Box>
        </Grid>
      </Grid>
    </FeaturePageTemplate>
  );
};

export default OCRTools;

import React, { useRef } from 'react';
import { Box, Button, Dialog } from '@mui/material';
import ReactImagePickerEditor, {
  ImagePickerConf,
} from 'react-image-picker-editor';
import 'react-image-picker-editor/dist/index.css';
import SampleImageSrc from '@assets/images/resize-image-sample.png';
import FeaturePageTemplate from '@components/FeaturePageTemplate';

const config2: ImagePickerConf = {
  borderRadius: '8px',
  language: 'en',
  width: '100vw',
  height: '70vh',
  objectFit: 'contain',
  compressInitial: null,
  hideDeleteBtn: true,
};

interface IProps {
  open?: boolean;
  onClose?: () => void;
  onBack?: () => void;
  file?: File;
  isDialog?: boolean;
}

const EditImage = ({
  open = false,
  onClose,
  onBack,
  file,
  isDialog = false,
}: IProps) => {
  const editorRef = useRef<any>(null);

  const handleBack = () => {
    onBack?.();
  };

  const handleSave = () => {
    if (editorRef.current) {
      try {
        const dataUrl = editorRef.current.getImageScaledToCanvas().toDataURL();
        const link = document.createElement('a');
        link.download = 'edited-image.png';
        link.href = dataUrl;
        link.click();
      } catch (err) {
        // Handle error silently
      }
    }
  };

  const content = (
    <>
      <ReactImagePickerEditor
        config={config2}
        imageSrcProp={file ? URL.createObjectURL(file) : SampleImageSrc}
        imageChanged={() => {}}
      />
      <Box display="flex" justifyContent="space-between" p={2}>
        <Button onClick={handleBack} variant="contained" color="primary">
          Back
        </Button>
        <Button onClick={handleSave} variant="contained" color="primary">
          Save
        </Button>
      </Box>
    </>
  );

  if (isDialog) {
    return (
      <Dialog open={open} fullWidth maxWidth="md" onClose={onClose}>
        {content}
      </Dialog>
    );
  }

  return (
    <FeaturePageTemplate
      title="Edit Image"
      description="Edit your images with our easy-to-use image editor. Crop, rotate, and adjust your images with just a few clicks."
    >
      <Box my={2}>{content}</Box>
    </FeaturePageTemplate>
  );
};

export default EditImage;

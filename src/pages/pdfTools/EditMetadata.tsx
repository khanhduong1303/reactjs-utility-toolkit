import React, { useState } from 'react';
import { Button, Box, Alert, Grid, TextField } from '@mui/material';
import { CloudUpload as UploadIcon } from '@mui/icons-material';
import { PDFDocument } from 'pdf-lib';
import FeaturePageTemplate from '@components/FeaturePageTemplate';
import Notification from '@components/Notification';

interface Metadata {
  title: string;
  author: string;
  subject: string;
  keywords: string;
  producer: string;
  creator: string;
}

const EditMetadata = () => {
  const [selectedFile, setSelectedFile] = useState<File | null>(null);
  const [error, setError] = useState('');
  const [saving, setSaving] = useState(false);
  const [notification, setNotification] = useState({
    open: false,
    message: '',
  });
  const [metadata, setMetadata] = useState<Metadata>({
    title: '',
    author: '',
    subject: '',
    keywords: '',
    producer: '',
    creator: '',
  });

  const handleFileUpload = async (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const file = event.target.files?.[0];
    if (file) {
      if (file.type === 'application/pdf') {
        try {
          const fileData = await file.arrayBuffer();
          const pdfDoc = await PDFDocument.load(fileData);

          setMetadata({
            title: pdfDoc.getTitle() || '',
            author: pdfDoc.getAuthor() || '',
            subject: pdfDoc.getSubject() || '',
            keywords: pdfDoc.getKeywords() || '',
            producer: pdfDoc.getProducer() || '',
            creator: pdfDoc.getCreator() || '',
          });

          setSelectedFile(file);
          setError('');
        } catch (err) {
          setError('Failed to load PDF. Please try again.');
          console.error('Loading error:', err);
        }
      } else {
        setError('Please upload a valid PDF file');
      }
    }
  };

  const handleMetadataChange =
    (field: keyof Metadata) => (event: React.ChangeEvent<HTMLInputElement>) => {
      setMetadata((prev) => ({
        ...prev,
        [field]: event.target.value,
      }));
    };

  const saveMetadata = async () => {
    if (!selectedFile) return;

    try {
      setSaving(true);
      setError('');

      const fileData = await selectedFile.arrayBuffer();
      const pdfDoc = await PDFDocument.load(fileData);

      pdfDoc.setTitle(metadata.title);
      pdfDoc.setAuthor(metadata.author);
      pdfDoc.setSubject(metadata.subject);
      pdfDoc.setKeywords(metadata.keywords.split(','));
      pdfDoc.setProducer(metadata.producer);
      pdfDoc.setCreator(metadata.creator);

      const pdfBytes = await pdfDoc.save();
      const blob = new Blob([pdfBytes], { type: 'application/pdf' });
      const url = URL.createObjectURL(blob);
      const link = document.createElement('a');
      link.href = url;
      link.download = `edited_${selectedFile.name}`;
      link.click();
      URL.revokeObjectURL(url);
    } catch (err) {
      setError('Failed to save metadata. Please try again.');
      console.error('Saving error:', err);
    } finally {
      setSaving(false);
    }
  };

  return (
    <FeaturePageTemplate
      title="Edit PDF Metadata"
      description="Edit document properties of your PDF file"
    >
      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <Grid container spacing={4} justifyContent="center">
        <Grid item xs={12} md={6}>
          <Box className="space-y-6">
            <input
              type="file"
              accept=".pdf"
              onChange={handleFileUpload}
              className="hidden"
              id="pdf-upload"
            />
            <label htmlFor="pdf-upload">
              <Button
                variant="outlined"
                component="span"
                startIcon={<UploadIcon />}
                fullWidth
              >
                Upload PDF
              </Button>
            </label>

            {selectedFile && (
              <>
                <TextField
                  fullWidth
                  label="Title"
                  value={metadata.title}
                  onChange={handleMetadataChange('title')}
                />
                <TextField
                  fullWidth
                  label="Author"
                  value={metadata.author}
                  onChange={handleMetadataChange('author')}
                />
                <TextField
                  fullWidth
                  label="Subject"
                  value={metadata.subject}
                  onChange={handleMetadataChange('subject')}
                />
                <TextField
                  fullWidth
                  label="Keywords"
                  value={metadata.keywords}
                  onChange={handleMetadataChange('keywords')}
                  helperText="Separate keywords with commas"
                />
                <TextField
                  fullWidth
                  label="Producer"
                  value={metadata.producer}
                  onChange={handleMetadataChange('producer')}
                />
                <TextField
                  fullWidth
                  label="Creator"
                  value={metadata.creator}
                  onChange={handleMetadataChange('creator')}
                />

                <Button
                  variant="contained"
                  onClick={saveMetadata}
                  fullWidth
                  disabled={saving}
                >
                  {saving ? 'Saving...' : 'Save Metadata'}
                </Button>
              </>
            )}
          </Box>
        </Grid>
      </Grid>

      <Notification
        open={notification.open}
        message={notification.message}
        onClose={() => setNotification({ ...notification, open: false })}
      />
    </FeaturePageTemplate>
  );
};

export default EditMetadata;

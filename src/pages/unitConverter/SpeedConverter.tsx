import React, { useState, useEffect } from 'react';
import {
  Typography,
  Grid,
  Paper,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from '@mui/material';
import FeaturePageTemplate from '@components/FeaturePageTemplate';

interface Unit {
  value: string;
  label: string;
  toBase: number; // conversion to meters per second
}

const SPEED_UNITS: Unit[] = [
  { value: 'mps', label: 'Meters per Second (m/s)', toBase: 1 },
  { value: 'kph', label: 'Kilometers per Hour (km/h)', toBase: 1 / 3.6 },
  { value: 'mph', label: 'Miles per Hour (mph)', toBase: 0.44704 },
  { value: 'fps', label: 'Feet per Second (ft/s)', toBase: 0.3048 },
  { value: 'knot', label: 'Knots (kn)', toBase: 0.514444 },
  { value: 'mach', label: 'Mach Number (M)', toBase: 340.29 }, // at sea level, 15°C
];

const SpeedConverter = () => {
  const [inputValue, setInputValue] = useState<string>('');
  const [inputUnit, setInputUnit] = useState<string>('kph');
  const [outputUnit, setOutputUnit] = useState<string>('mph');
  const [outputValue, setOutputValue] = useState<string>('');

  const convert = (value: string, from: string, to: string) => {
    const numValue = parseFloat(value);
    if (isNaN(numValue)) return '';

    const fromUnit = SPEED_UNITS.find((u) => u.value === from);
    const toUnit = SPEED_UNITS.find((u) => u.value === to);
    if (!fromUnit || !toUnit) return '';

    // Convert to base unit (meters per second) then to target unit
    const baseValue = numValue * fromUnit.toBase;
    const result = baseValue / toUnit.toBase;
    return result.toFixed(3);
  };

  useEffect(() => {
    const result = convert(inputValue, inputUnit, outputUnit);
    setOutputValue(result);
  }, [inputValue, inputUnit, outputUnit]);

  return (
    <FeaturePageTemplate
      title="Speed Converter"
      description="Convert between different units of speed"
    >
      <Grid container spacing={4} justifyContent="center">
        <Grid item xs={12} md={8}>
          <Paper elevation={1} className="p-6">
            <Grid container spacing={3}>
              <Grid item xs={12} md={6}>
                <FormControl fullWidth>
                  <InputLabel>From</InputLabel>
                  <Select
                    value={inputUnit}
                    label="From"
                    onChange={(e) => setInputUnit(e.target.value)}
                  >
                    {SPEED_UNITS.map((unit) => (
                      <MenuItem key={unit.value} value={unit.value}>
                        {unit.label}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>

              <Grid item xs={12} md={6}>
                <FormControl fullWidth>
                  <InputLabel>To</InputLabel>
                  <Select
                    value={outputUnit}
                    label="To"
                    onChange={(e) => setOutputUnit(e.target.value)}
                  >
                    {SPEED_UNITS.map((unit) => (
                      <MenuItem key={unit.value} value={unit.value}>
                        {unit.label}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>

              <Grid item xs={12} md={6}>
                <TextField
                  fullWidth
                  label="Enter Value"
                  type="number"
                  value={inputValue}
                  onChange={(e) => setInputValue(e.target.value)}
                  placeholder="Enter a number"
                />
              </Grid>

              <Grid item xs={12} md={6}>
                <TextField
                  fullWidth
                  label="Result"
                  value={outputValue}
                  InputProps={{ readOnly: true }}
                />
              </Grid>
            </Grid>

            <Typography className="mt-6 text-sm text-gray-600">
              Note: Results are rounded to 3 decimal places for precision. Mach
              number is calculated at sea level, 15°C.
            </Typography>
          </Paper>
        </Grid>
      </Grid>
    </FeaturePageTemplate>
  );
};

export default SpeedConverter;

import React from 'react';
import AdSense from './index';

const InFeedAd: React.FC = () => (
  <AdSense
    slot="5962811095"
    format="fluid"
    layout="in-article"
    className="my-8"
  />
);

export default InFeedAd;

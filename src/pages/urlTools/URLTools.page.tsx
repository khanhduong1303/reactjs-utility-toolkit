import React, { useState } from 'react';
import {
  Typography,
  Grid,
  Paper,
  TextField,
  Button,
  Alert,
} from '@mui/material';
import FeaturePageTemplate from '@components/FeaturePageTemplate';

interface ParsedURL {
  protocol: string;
  host: string;
  hostname: string;
  port: string;
  pathname: string;
  search: string;
  searchParams: Record<string, string>;
  hash: string;
  origin: string;
}

const URLTools = () => {
  const [inputURL, setInputURL] = useState('');
  const [parsedURL, setParsedURL] = useState<ParsedURL | null>(null);
  const [error, setError] = useState('');

  const parseURL = (url: string) => {
    try {
      const parsed = new URL(url);
      setParsedURL({
        protocol: parsed.protocol,
        host: parsed.host,
        hostname: parsed.hostname,
        port: parsed.port,
        pathname: parsed.pathname,
        search: parsed.search,
        searchParams: Object.fromEntries(parsed.searchParams),
        hash: parsed.hash,
        origin: parsed.origin,
      });
      setError('');
    } catch {
      setError('Invalid URL format');
      setParsedURL(null);
    }
  };

  const handleEncode = () => {
    try {
      setInputURL(encodeURIComponent(inputURL));
      setError('');
    } catch {
      setError('Failed to encode URL');
    }
  };

  const handleDecode = () => {
    try {
      setInputURL(decodeURIComponent(inputURL));
      setError('');
    } catch {
      setError('Failed to decode URL');
    }
  };

  const handleNormalize = () => {
    try {
      const url = new URL(inputURL);
      setInputURL(url.toString());
      setError('');
    } catch {
      setError('Invalid URL format');
    }
  };

  const handleExtractQueryParams = () => {
    try {
      const url = new URL(inputURL);
      const params = Object.fromEntries(url.searchParams);
      setParsedURL((prev) => ({
        ...(prev as ParsedURL),
        searchParams: params,
      }));
      setError('');
    } catch {
      setError('Failed to extract query parameters');
    }
  };

  return (
    <FeaturePageTemplate
      title="URL Tools"
      description="URL parsing, encoding, decoding, and analysis tools"
    >
      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <Grid container spacing={4}>
        <Grid item xs={12}>
          <Paper elevation={1} className="p-6">
            <div className="space-y-4">
              <TextField
                fullWidth
                label="URL"
                value={inputURL}
                onChange={(e) => setInputURL(e.target.value)}
                placeholder="https://example.com/path?param=value#hash"
              />

              <div className="space-x-2">
                <Button variant="outlined" onClick={() => parseURL(inputURL)}>
                  Parse URL
                </Button>
                <Button variant="outlined" onClick={handleEncode}>
                  Encode URL
                </Button>
                <Button variant="outlined" onClick={handleDecode}>
                  Decode URL
                </Button>
                <Button variant="outlined" onClick={handleNormalize}>
                  Normalize URL
                </Button>
                <Button variant="outlined" onClick={handleExtractQueryParams}>
                  Extract Query Parameters
                </Button>
              </div>
            </div>
          </Paper>
        </Grid>

        {parsedURL && (
          <Grid item xs={12}>
            <Paper elevation={1} className="p-6">
              <Typography variant="h6" className="mb-4">
                Parsed URL Components
              </Typography>

              <Grid container spacing={2}>
                {Object.entries(parsedURL).map(([key, value]) => (
                  <Grid item xs={12} sm={6} key={key}>
                    <TextField
                      fullWidth
                      label={key.charAt(0).toUpperCase() + key.slice(1)}
                      value={
                        typeof value === 'object'
                          ? JSON.stringify(value, null, 2)
                          : value
                      }
                      multiline={typeof value === 'object'}
                      rows={typeof value === 'object' ? 4 : 1}
                      InputProps={{
                        readOnly: true,
                      }}
                    />
                  </Grid>
                ))}
              </Grid>
            </Paper>
          </Grid>
        )}
      </Grid>
    </FeaturePageTemplate>
  );
};

export default URLTools;

import React from 'react';
import VideoConverter from '@components/VideoConverter';
import { Box } from '@mui/material';
import FeaturePageTemplate from '@components/FeaturePageTemplate';

const ConvertVideo = () => {
  return (
    <FeaturePageTemplate
      title="Convert Video"
      description="Convert your videos to different formats with high quality"
    >
      <Box>
        <VideoConverter />
      </Box>
    </FeaturePageTemplate>
  );
};

export default ConvertVideo;

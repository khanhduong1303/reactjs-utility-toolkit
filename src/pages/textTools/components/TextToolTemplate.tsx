import React, { ReactNode } from 'react';
import { Container, Typography, Paper } from '@mui/material';
import { BannerAd, InArticleAd } from '@components/AdSense/components';

interface TextToolTemplateProps {
  title: string;
  description?: string;
  children: ReactNode;
}

const TextToolTemplate: React.FC<TextToolTemplateProps> = ({
  title,
  description,
  children,
}) => {
  return (
    <Container maxWidth="lg" className="py-8 animate-fade-in">
      <div className="text-center space-y-2 mb-8">
        <Typography variant="h4" className="gradient-text font-bold">
          {title}
        </Typography>
        {description && (
          <Typography variant="body1" className="text-secondary-600">
            {description}
          </Typography>
        )}
        <BannerAd />
      </div>

      <div className="grid grid-cols-1 md:grid-cols-12 gap-6">
        <div className="md:col-span-9">
          <Paper elevation={1} className="p-6">
            {children}
          </Paper>
        </div>
        <div className="md:col-span-3">
          <BannerAd />
          <div className="mt-4">
            <InArticleAd />
          </div>
        </div>
      </div>

      <div className="mt-8">
        <BannerAd />
      </div>
    </Container>
  );
};

export default TextToolTemplate;

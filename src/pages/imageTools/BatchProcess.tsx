import React, { useState } from 'react';
import {
  Typography,
  Button,
  Box,
  Alert,
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  IconButton,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Chip,
  LinearProgress,
} from '@mui/material';
import {
  CloudUpload as UploadIcon,
  Delete as DeleteIcon,
  ArrowUpward as MoveUpIcon,
  ArrowDownward as MoveDownIcon,
} from '@mui/icons-material';
import FeaturePageTemplate from '@components/FeaturePageTemplate';
import Notification from '@components/Notification';

interface ImageFile {
  id: string;
  file: File;
}

const OPERATIONS = [
  { value: 'resize', label: 'Resize' },
  { value: 'compress', label: 'Compress' },
  { value: 'convert', label: 'Convert Format' },
  { value: 'rotate', label: 'Rotate' },
  { value: 'watermark', label: 'Add Watermark' },
  { value: 'filter', label: 'Apply Filter' },
];

const BatchProcess = () => {
  const [selectedFiles, setSelectedFiles] = useState<ImageFile[]>([]);
  const [error, setError] = useState('');
  const [processing, setProcessing] = useState(false);
  const [selectedOperations, setSelectedOperations] = useState<string[]>([]);
  const [progress, setProgress] = useState(0);
  const [notification, setNotification] = useState({
    open: false,
    message: '',
  });

  const handleFileUpload = (event: React.ChangeEvent<HTMLInputElement>) => {
    const files = event.target.files;
    if (files) {
      const newFiles = Array.from(files).map((file) => ({
        id: Math.random().toString(36).substr(2, 9),
        file,
      }));
      setSelectedFiles((prev) => [...prev, ...newFiles]);
      setError('');
    }
  };

  const handleRemoveFile = (id: string) => {
    setSelectedFiles((prev) => prev.filter((file) => file.id !== id));
  };

  const handleMoveFile = (id: string, direction: 'up' | 'down') => {
    const index = selectedFiles.findIndex((file) => file.id === id);
    if (
      (direction === 'up' && index > 0) ||
      (direction === 'down' && index < selectedFiles.length - 1)
    ) {
      const newFiles = [...selectedFiles];
      const temp = newFiles[index];
      newFiles[index] = newFiles[index + (direction === 'up' ? -1 : 1)];
      newFiles[index + (direction === 'up' ? -1 : 1)] = temp;
      setSelectedFiles(newFiles);
    }
  };

  const handleOperationChange = (event: any) => {
    setSelectedOperations(event.target.value as string[]);
  };

  const handleProcess = async () => {
    if (selectedFiles.length === 0) {
      setError('Please select at least one file');
      return;
    }
    if (selectedOperations.length === 0) {
      setError('Please select at least one operation');
      return;
    }

    // Demo implementation
    setProcessing(true);
    setProgress(0);

    for (let i = 0; i <= 100; i += 10) {
      await new Promise((resolve) => setTimeout(resolve, 200));
      setProgress(i);
    }

    setProcessing(false);
    setProgress(0);
    setNotification({
      open: true,
      message:
        'This is a demo version. Batch processing is not implemented yet.',
    });
  };

  return (
    <FeaturePageTemplate
      title="Batch Process"
      description="Process multiple images at once with various operations"
    >
      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <Box className="space-y-6">
        <input
          type="file"
          accept="image/*"
          onChange={handleFileUpload}
          className="hidden"
          id="image-upload"
          multiple
        />
        <label htmlFor="image-upload">
          <Button
            variant="outlined"
            component="span"
            startIcon={<UploadIcon />}
            fullWidth
          >
            Upload Images
          </Button>
        </label>

        {selectedFiles.length > 0 && (
          <>
            <Box>
              <Typography gutterBottom>
                Selected Files ({selectedFiles.length})
              </Typography>
              <List>
                {selectedFiles.map((file, index) => (
                  <ListItem key={file.id}>
                    <ListItemText
                      primary={file.file.name}
                      secondary={`${(file.file.size / 1024 / 1024).toFixed(
                        2
                      )} MB`}
                    />
                    <ListItemSecondaryAction>
                      <IconButton
                        edge="end"
                        onClick={() => handleMoveFile(file.id, 'up')}
                        disabled={index === 0}
                      >
                        <MoveUpIcon />
                      </IconButton>
                      <IconButton
                        edge="end"
                        onClick={() => handleMoveFile(file.id, 'down')}
                        disabled={index === selectedFiles.length - 1}
                      >
                        <MoveDownIcon />
                      </IconButton>
                      <IconButton
                        edge="end"
                        onClick={() => handleRemoveFile(file.id)}
                      >
                        <DeleteIcon />
                      </IconButton>
                    </ListItemSecondaryAction>
                  </ListItem>
                ))}
              </List>
            </Box>

            <FormControl fullWidth>
              <InputLabel>Operations</InputLabel>
              <Select
                multiple
                value={selectedOperations}
                onChange={handleOperationChange}
                label="Operations"
                renderValue={(selected) => (
                  <Box sx={{ display: 'flex', flexWrap: 'wrap', gap: 0.5 }}>
                    {(selected as string[]).map((value) => (
                      <Chip
                        key={value}
                        label={
                          OPERATIONS.find((op) => op.value === value)?.label
                        }
                      />
                    ))}
                  </Box>
                )}
              >
                {OPERATIONS.map((operation) => (
                  <MenuItem key={operation.value} value={operation.value}>
                    {operation.label}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            {processing && (
              <Box>
                <Typography gutterBottom>Processing...</Typography>
                <LinearProgress variant="determinate" value={progress} />
              </Box>
            )}

            <Button
              variant="contained"
              onClick={handleProcess}
              fullWidth
              disabled={
                processing ||
                selectedFiles.length === 0 ||
                selectedOperations.length === 0
              }
            >
              {processing ? 'Processing Images...' : 'Process Images'}
            </Button>
          </>
        )}
      </Box>

      <Notification
        open={notification.open}
        message={notification.message}
        onClose={() => setNotification({ ...notification, open: false })}
      />
    </FeaturePageTemplate>
  );
};

export default BatchProcess;

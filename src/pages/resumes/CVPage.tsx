import {
  CSSProperties,
  useCallback,
  useState,
} from 'react';
import { Typography, Box, Link, useMediaQuery } from '@mui/material';
import './CVPage.css';
import ImgAvatar from '@assets/images/cv-avatar.png';
import AvatarRotated from '@components/Avatar.components';
import {
  Email,
  Home,
  LinkedIn,
  Phone,
  Twitter,
  Language,
} from '@mui/icons-material';
import Rating from '@components/Rating.components';
import ExperienceItem from './ExperienceItem';
import { MAIN_COLORS, DATA } from './constants';
import { CVContext } from './hooks';

const Contact = ({ data, icon }: { data: any; icon: any }) => {
  if (!data) return null;
  return (
    <Box sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
      {icon}
      {typeof data === 'string' ? (
        <Typography style={{ fontSize: 10, marginLeft: 6 }}>{data}</Typography>
      ) : (
        <Link
          target="_blank"
          href={data.link}
          style={{ fontSize: 10, marginLeft: 6, color: 'white' }}
        >
          {data.name}
        </Link>
      )}
    </Box>
  );
};

const Resume = () => {
  const [primaryColor] = useState(MAIN_COLORS[0]);
  const [data] = useState(DATA);
  const isMobile = useMediaQuery('(max-width:600px)');

  const renderSkills = useCallback(() => {
    return data.skills.map((gr, idx) => {
      return (
        <Box key={idx}>
          <Typography style={{ fontSize: 12, marginTop: 4 }}>
            {gr.groupTitle}
          </Typography>
          {gr.items.map((it, idx) => (
            <Box
              key={idx}
              sx={{
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 1,
              }}
            >
              <Typography style={{ fontSize: 10, flex: 1 }}>
                {it.name}
              </Typography>
              <Rating
                rate={it.experience}
                nums={5}
                size={8}
                color={primaryColor}
              />
            </Box>
          ))}
        </Box>
      );
    });
  }, [data.skills, primaryColor]);

  const renderExperience = useCallback(() => {
    return data.experience.map((it, idx) => (
      <ExperienceItem
        key={idx}
        data={it}
        showLine={idx !== data.experience.length - 1}
      />
    ));
  }, [data.experience]);

  const renderEducation = useCallback(() => {
    return data.education.map((it, idx) => (
      <ExperienceItem
        key={idx}
        data={it}
        showLine={idx !== data.education.length - 1}
      />
    ));
  }, [data.education]);

  const renderContact = useCallback(() => {
    const icSize = 16;
    return (
      <Box style={{ marginTop: 4 }}>
        <Contact
          data={data.contact.address}
          icon={<Home style={{ fontSize: icSize }} />}
        />
        <Contact
          data={data.contact.phone}
          icon={<Phone style={{ fontSize: icSize }} />}
        />
        <Contact
          data={data.contact.email}
          icon={<Email style={{ fontSize: icSize }} />}
        />
        <Contact
          data={data.contact.linkedIn}
          icon={<LinkedIn style={{ fontSize: icSize }} />}
        />
        <Contact
          data={data.contact.twitter}
          icon={<Twitter style={{ fontSize: icSize }} />}
        />
        <Contact
          data={data.contact.web}
          icon={<Language style={{ fontSize: icSize }} />}
        />
      </Box>
    );
  }, [data.contact]);

  return (
    <CVContext.Provider value={{ primaryColor }}>
      <Box
        sx={{
          display: 'flex',
          justifyContent: 'center',
          width: '100%',
          minHeight: '100vh',
          padding: '12px',
        }}
      >
        <Box
          className="container"
          sx={
            !isMobile
              ? {
                  boxShadow: 10,
                  borderBottom: `10px solid ${primaryColor}`,
                }
              : null
          }
        >
          {/* Left Column */}
          <Box
            style={{
              backgroundColor: '#000',
              color: '#fff',
              minWidth: 200,
            }}
          >
            <Box
              style={{
                height: '100%',
                marginRight: 4,
                marginLeft: 8,
                borderRight: `2px solid ${primaryColor}`,
              }}
            >
              <Box
                style={{
                  display: 'flex',
                  justifyContent: 'center',
                  padding: '12px 0 12px 0',
                }}
              >
                <AvatarRotated
                  avatarProps={{ src: ImgAvatar }}
                  frameColor={primaryColor}
                  size={120}
                />
              </Box>

              <Typography style={{ ...styles.groupTitle, color: primaryColor }}>
                CONTACT
              </Typography>
              {renderContact()}

              <Typography style={{ ...styles.groupTitle, color: primaryColor }}>
                SKILLS
              </Typography>
              {renderSkills()}
            </Box>
          </Box>

          {/* Right Column */}
          <Box
            style={{
              backgroundColor: '#f9f9f9',
              minWidth: 600,
              padding: '24px',
            }}
          >
            <Typography variant="h4" style={{ fontWeight: 'bold' }}>
              {data.name?.toUpperCase()}
            </Typography>
            <Typography variant="subtitle1">
              {data.title?.toUpperCase()}
            </Typography>
            <Typography style={{ marginTop: 14, fontSize: 12 }}>
              {data.about}
            </Typography>
            {/* Add experience, education, and skills sections here */}
            <Typography style={{ ...styles.groupTitle, color: primaryColor }}>
              EXPERIENCE
            </Typography>
            {renderExperience()}

            <Typography style={{ ...styles.groupTitle, color: primaryColor }}>
              EDUCATION
            </Typography>
            {renderEducation()}
          </Box>
        </Box>
      </Box>
    </CVContext.Provider>
  );
};

export default Resume;

const styles: { [key: string]: CSSProperties } = {
  groupTitle: {
    fontSize: 14,
    marginTop: 12,
  },
};

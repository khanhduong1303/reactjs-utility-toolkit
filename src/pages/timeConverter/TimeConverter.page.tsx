import React, { useState, useEffect, useMemo } from 'react';
import {
  Box,
  TextField,
  MenuItem,
  Grid,
  Paper,
  Typography,
} from '@mui/material';
import { DateTime } from 'luxon';
import '@formatjs/intl-datetimeformat/polyfill';
import '@formatjs/intl-datetimeformat/locale-data/en';
import '@formatjs/intl-datetimeformat/add-all-tz';
import FeaturePageTemplate from '@components/FeaturePageTemplate';

interface TimeZone {
  value: string;
  label: string;
}

const TimeConverter = () => {
  const [date, setDate] = useState<string>(
    DateTime.now().toFormat('yyyy-MM-dd')
  );
  const [time, setTime] = useState<string>(DateTime.now().toFormat('HH:mm'));
  const [sourceZone, setSourceZone] = useState<TimeZone | null>(null);
  const [targetZone, setTargetZone] = useState<TimeZone | null>(null);
  const [convertedTime, setConvertedTime] = useState<string>('');

  const timeZones: TimeZone[] = useMemo(() => {
    try {
      return (Intl as any).supportedValuesOf('timeZone').map(
        (zone: string): TimeZone => ({
          value: zone,
          label: zone.replace(/_/g, ' '),
        })
      );
    } catch (error) {
      console.error('Error getting time zones:', error);
      return [];
    }
  }, []);

  useEffect(() => {
    // Set default source zone to local time zone
    const localZone = DateTime.local().zoneName;
    if (localZone) {
      const defaultZone = timeZones.find((zone) => zone.value === localZone);
      if (defaultZone) {
        setSourceZone(defaultZone);
      }
    }
  }, [timeZones]);

  useEffect(() => {
    if (date && time && sourceZone && targetZone) {
      const sourceDateTime = DateTime.fromFormat(
        `${date} ${time}`,
        'yyyy-MM-dd HH:mm',
        {
          zone: sourceZone.value,
        }
      );

      if (sourceDateTime.isValid) {
        const targetDateTime = sourceDateTime.setZone(targetZone.value);
        setConvertedTime(
          `${targetDateTime.toFormat(
            'yyyy-MM-dd HH:mm'
          )} (${targetDateTime.toFormat('ZZZZ')})`
        );
      } else {
        setConvertedTime('Invalid date/time');
      }
    }
  }, [date, time, sourceZone, targetZone]);

  return (
    <FeaturePageTemplate
      title="Time Converter"
      description="Convert time between different time zones"
    >
      <Grid container spacing={4} justifyContent="center">
        <Grid item xs={12} md={8}>
          <Paper elevation={1} className="p-6">
            <Grid container spacing={3}>
              <Grid item xs={12} md={6}>
                <TextField
                  label="Date"
                  type="date"
                  value={date}
                  onChange={(e) => setDate(e.target.value)}
                  fullWidth
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  label="Time"
                  type="time"
                  value={time}
                  onChange={(e) => setTime(e.target.value)}
                  fullWidth
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  select
                  label="Source Time Zone"
                  value={sourceZone?.value || ''}
                  onChange={(e) => {
                    const zone = timeZones.find(
                      (z: TimeZone) => z.value === e.target.value
                    );
                    setSourceZone(zone || null);
                  }}
                  fullWidth
                >
                  {timeZones.map((zone: TimeZone) => (
                    <MenuItem key={zone.value} value={zone.value}>
                      {zone.label}
                    </MenuItem>
                  ))}
                </TextField>
              </Grid>
              <Grid item xs={12}>
                <TextField
                  select
                  label="Target Time Zone"
                  value={targetZone?.value || ''}
                  onChange={(e) => {
                    const zone = timeZones.find(
                      (z: TimeZone) => z.value === e.target.value
                    );
                    setTargetZone(zone || null);
                  }}
                  fullWidth
                >
                  {timeZones.map((zone: TimeZone) => (
                    <MenuItem key={zone.value} value={zone.value}>
                      {zone.label}
                    </MenuItem>
                  ))}
                </TextField>
              </Grid>
            </Grid>

            <Box className="mt-6 p-4 bg-gray-50 rounded-lg">
              <Typography variant="h6" className="mb-2">
                Converted Time
              </Typography>
              <Typography className="text-lg">
                {convertedTime || 'Select time zones to see conversion'}
              </Typography>
            </Box>
          </Paper>
        </Grid>
      </Grid>
    </FeaturePageTemplate>
  );
};

export default TimeConverter;

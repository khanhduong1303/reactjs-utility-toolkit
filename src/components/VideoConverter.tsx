import React, { useState, useRef, useEffect } from 'react';
import {
  Button,
  Select,
  MenuItem,
  FormControl,
  InputLabel,
  Box,
  Typography,
  Alert,
  CircularProgress,
  Grid,
  IconButton,
} from '@mui/material';
import { useDropzone } from 'react-dropzone';
import { PlayArrow, Pause } from '@mui/icons-material';

interface VideoSettings {
  format: string;
  quality: string;
}

const VideoConverter: React.FC = () => {
  const [video, setVideo] = useState<File | null>(null);
  const [converting, setConverting] = useState(false);
  const [progress, setProgress] = useState(0);
  const [previewUrl, setPreviewUrl] = useState<string>('');
  const [error, setError] = useState<string>('');
  const [isPlaying, setIsPlaying] = useState(false);
  const videoRef = useRef<HTMLVideoElement>(null);

  const [settings, setSettings] = useState<VideoSettings>({
    format: 'mp4',
    quality: 'high',
  });

  const formats = [
    { value: 'mp4', label: 'MP4', type: 'video/mp4' },
    { value: 'webm', label: 'WebM', type: 'video/webm' },
    { value: 'mov', label: 'MOV', type: 'video/quicktime' },
    { value: 'avi', label: 'AVI', type: 'video/x-msvideo' },
  ];

  const qualities = [
    { value: 'high', label: 'High Quality', bitrate: 5000000 },
    { value: 'medium', label: 'Medium Quality', bitrate: 2500000 },
    { value: 'low', label: 'Low Quality', bitrate: 1000000 },
  ];

  useEffect(() => {
    return () => {
      if (previewUrl) {
        URL.revokeObjectURL(previewUrl);
      }
    };
  }, [previewUrl]);

  const onDrop = (acceptedFiles: File[]) => {
    const file = acceptedFiles[0];
    if (file && file.type.startsWith('video/')) {
      setVideo(file);
      const url = URL.createObjectURL(file);
      setPreviewUrl(url);
      setError('');
    } else {
      setError('Please upload a valid video file');
    }
  };

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop,
    accept: { 'video/*': [] },
    maxFiles: 1,
  });

  const handleSettingChange = (setting: keyof VideoSettings, value: any) => {
    setSettings((prev) => ({ ...prev, [setting]: value }));
  };

  const handlePlayPause = () => {
    if (videoRef.current) {
      if (isPlaying) {
        videoRef.current.pause();
      } else {
        videoRef.current.play();
      }
      setIsPlaying(!isPlaying);
    }
  };

  const convertVideo = async () => {
    if (!video || !videoRef.current) return;

    try {
      setConverting(true);
      setProgress(0);
      setError('');

      // Create a new blob from the video file
      const videoBlob = new Blob([await video.arrayBuffer()], {
        type: video.type,
      });
      const originalName = video.name.split('.')[0];

      // Create download link with original video
      const a = document.createElement('a');
      a.href = URL.createObjectURL(videoBlob);
      a.download = `${originalName}-converted.${settings.format}`;
      a.click();

      setConverting(false);
      setProgress(100);
    } catch (error: any) {
      console.error('Error during conversion:', error);
      setError(error.message || 'Error converting video. Please try again.');
      setConverting(false);
    }
  };

  return (
    <Box className="w-full max-w-4xl mx-auto p-4 md:p-6 space-y-4 md:space-y-6">
      {error && (
        <Alert severity="error" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <div
        {...getRootProps()}
        className={`border-2 border-dashed rounded-lg p-4 md:p-8 text-center cursor-pointer transition-colors
          ${
            isDragActive
              ? 'border-blue-500 bg-blue-50'
              : 'border-gray-300 hover:border-blue-400'
          }`}
      >
        <input {...getInputProps()} />
        <Typography variant="h6" className="mb-2">
          {isDragActive
            ? 'Drop the video here'
            : 'Drag & drop a video file here, or click to select'}
        </Typography>
        <Typography variant="body2" color="textSecondary">
          Supports various video formats (MP4, WebM, MOV, AVI)
        </Typography>
      </div>

      {previewUrl && (
        <Box className="space-y-4">
          <Box className="aspect-video w-full bg-black rounded-lg overflow-hidden">
            <video
              ref={videoRef}
              src={previewUrl}
              className="w-full h-full object-contain"
              controls
            />
          </Box>

          <Grid container spacing={2} alignItems="center" className="px-2">
            <Grid item>
              <IconButton onClick={handlePlayPause}>
                {isPlaying ? <Pause /> : <PlayArrow />}
              </IconButton>
            </Grid>
          </Grid>
        </Box>
      )}

      {video && (
        <Box className="space-y-4 bg-gray-50 p-4 md:p-6 rounded-lg">
          <Typography variant="h6" className="mb-4">
            Conversion Settings
          </Typography>

          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <FormControl fullWidth>
                <InputLabel>Output Format</InputLabel>
                <Select
                  value={settings.format}
                  onChange={(e) =>
                    handleSettingChange('format', e.target.value)
                  }
                  label="Output Format"
                >
                  {formats.map((format) => (
                    <MenuItem key={format.value} value={format.value}>
                      {format.label}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormControl fullWidth>
                <InputLabel>Quality</InputLabel>
                <Select
                  value={settings.quality}
                  onChange={(e) =>
                    handleSettingChange('quality', e.target.value)
                  }
                  label="Quality"
                >
                  {qualities.map((quality) => (
                    <MenuItem key={quality.value} value={quality.value}>
                      {quality.label}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>
          </Grid>

          <Button
            variant="contained"
            color="primary"
            onClick={convertVideo}
            disabled={converting}
            fullWidth
            className="mt-4"
          >
            {converting ? (
              <Box className="flex items-center justify-center">
                <CircularProgress size={24} className="mr-2" />
                <span>Converting... {Math.round(progress)}%</span>
              </Box>
            ) : (
              'Download Video'
            )}
          </Button>
        </Box>
      )}
    </Box>
  );
};

export default VideoConverter;

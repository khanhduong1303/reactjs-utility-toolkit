import React, { useState } from 'react';
import { Typography, Button, Box, Alert } from '@mui/material';
import { CloudUpload as UploadIcon } from '@mui/icons-material';
import FeaturePageTemplate from '@components/FeaturePageTemplate';
import Notification from '@components/Notification';

const CropImage = () => {
  const [selectedFile, setSelectedFile] = useState<File | null>(null);
  const [error, setError] = useState('');
  const [processing, setProcessing] = useState(false);
  const [notification, setNotification] = useState({
    open: false,
    message: '',
  });

  const handleFileUpload = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0];
    if (file) {
      if (file.type.startsWith('image/')) {
        setSelectedFile(file);
        setError('');
      } else {
        setError('Please upload a valid image file');
      }
    }
  };

  const handleCrop = async () => {
    setProcessing(true);
    await new Promise((resolve) => setTimeout(resolve, 2000));
    setProcessing(false);
    setNotification({
      open: true,
      message: 'This is a demo version. Image cropping is not implemented yet.',
    });
  };

  return (
    <FeaturePageTemplate
      title="Crop Image"
      description="Crop and resize your images"
    >
      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <Box className="space-y-6">
        <input
          type="file"
          accept="image/*"
          onChange={handleFileUpload}
          style={{ display: 'none' }}
          id="image-upload"
        />
        <label htmlFor="image-upload">
          <Button
            variant="outlined"
            component="span"
            startIcon={<UploadIcon />}
            fullWidth
          >
            Upload Image
          </Button>
        </label>

        {selectedFile && (
          <>
            <Typography variant="body2">
              Selected file: {selectedFile.name}
            </Typography>

            <Button
              variant="contained"
              onClick={handleCrop}
              disabled={processing}
              fullWidth
            >
              {processing ? 'Cropping...' : 'Crop Image'}
            </Button>
          </>
        )}
      </Box>

      <Notification
        open={notification.open}
        message={notification.message}
        onClose={() => setNotification({ ...notification, open: false })}
      />
    </FeaturePageTemplate>
  );
};

export default CropImage;

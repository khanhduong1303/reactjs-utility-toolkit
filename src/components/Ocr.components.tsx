import React, { useEffect, useState } from 'react';
import { Container, Button, Typography, Box, Grid } from '@mui/material';
import { createWorker } from 'tesseract.js';

const OcrComponent = () => {
  const [images] = useState<string[]>([]);
  const [texts, setTexts] = useState<string[]>([]);
  const [worker, setWorker] = useState<any>(null);
  const [, setOcrText] = useState('');

  useEffect(() => {
    const initializeWorker = async () => {
      const workerInstance = await createWorker();
      await (workerInstance as any).loadLanguage('vie');
      await (workerInstance as any).initialize('vie');
      setWorker(workerInstance);
    };

    initializeWorker();
    return () => {
      worker?.terminate();
    };
  }, [worker]);

  const handleImageChange = async (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    if (event.target.files && event.target.files[0]) {
      try {
        const file = event.target.files[0];
        const localWorker = await createWorker();
        await (localWorker as any).loadLanguage('eng');
        await (localWorker as any).initialize('eng');
        const {
          data: { text },
        } = await localWorker.recognize(file);
        await localWorker.terminate();
        setOcrText(text);
      } catch (err) {
        setOcrText('Error processing image. Please try again.');
      }
    }
  };

  const handleOcr = async () => {
    if (!worker) return;

    const ocrPromises = images.map((image) => {
      return worker
        .recognize(image)
        .then(({ data: { text } }: { data: { text: string } }) =>
          extractTotalNumber(text)
        );
    });

    Promise.all(ocrPromises).then((texts) =>
      setTexts(texts.filter((t): t is string => t !== null))
    );
  };

  const extractTotalNumber = (ocrText: string) => {
    const lines = ocrText.split('\n').filter(Boolean);
    const lastLine = lines[lines.length - 1];
    const totalNumber = lastLine.match(/\d[\d,.]*/g);
    if (totalNumber) {
      return totalNumber[0];
    }
    return null;
  };

  return (
    <Container>
      <Box my={4}>
        <input
          accept="image/*, .heic"
          style={{ display: 'none' }}
          id="raised-button-file"
          type="file"
          onChange={handleImageChange}
          multiple
        />
        <label htmlFor="raised-button-file">
          <Button variant="contained" component="span">
            Upload Images
          </Button>
        </label>
        <Grid container spacing={2} style={{ marginTop: '20px' }}>
          {images.map((image, index) => (
            <Grid item xs={12} sm={6} md={4} key={index}>
              <img
                src={image}
                alt={`uploaded ${index}`}
                style={{ width: '100%' }}
              />
            </Grid>
          ))}
        </Grid>
        <Button
          variant="contained"
          color="primary"
          onClick={handleOcr}
          style={{ marginTop: '20px' }}
          disabled={!worker}
        >
          Perform OCR
        </Button>
        <Typography variant="h6" style={{ marginTop: '20px' }}>
          Extracted Texts:
        </Typography>
        {texts.map((text, index) => (
          <Typography key={index}>{`Image ${index + 1}: ${
            text || 'No total number found'
          }`}</Typography>
        ))}
      </Box>
    </Container>
  );
};

export default OcrComponent;

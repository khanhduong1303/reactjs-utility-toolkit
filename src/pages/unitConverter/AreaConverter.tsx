import React, { useState, useEffect } from 'react';
import {
  Typography,
  Grid,
  Paper,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from '@mui/material';
import FeaturePageTemplate from '@components/FeaturePageTemplate';

interface Unit {
  value: string;
  label: string;
  toBase: number; // conversion to square meters
}

const AREA_UNITS: Unit[] = [
  { value: 'm2', label: 'Square Meters (m²)', toBase: 1 },
  { value: 'km2', label: 'Square Kilometers (km²)', toBase: 1000000 },
  { value: 'cm2', label: 'Square Centimeters (cm²)', toBase: 0.0001 },
  { value: 'mm2', label: 'Square Millimeters (mm²)', toBase: 0.000001 },
  { value: 'ha', label: 'Hectares (ha)', toBase: 10000 },
  { value: 'acre', label: 'Acres', toBase: 4046.86 },
  { value: 'ft2', label: 'Square Feet (ft²)', toBase: 0.092903 },
  { value: 'yd2', label: 'Square Yards (yd²)', toBase: 0.836127 },
  { value: 'in2', label: 'Square Inches (in²)', toBase: 0.00064516 },
];

const AreaConverter = () => {
  const [inputValue, setInputValue] = useState<string>('');
  const [inputUnit, setInputUnit] = useState<string>('m2');
  const [outputUnit, setOutputUnit] = useState<string>('km2');
  const [outputValue, setOutputValue] = useState<string>('');

  const convert = (value: string, from: string, to: string) => {
    const numValue = parseFloat(value);
    if (isNaN(numValue)) return '';

    const fromUnit = AREA_UNITS.find((u) => u.value === from);
    const toUnit = AREA_UNITS.find((u) => u.value === to);
    if (!fromUnit || !toUnit) return '';

    // Convert to base unit (square meters) then to target unit
    const baseValue = numValue * fromUnit.toBase;
    const result = baseValue / toUnit.toBase;
    return result.toFixed(6);
  };

  useEffect(() => {
    const result = convert(inputValue, inputUnit, outputUnit);
    setOutputValue(result);
  }, [inputValue, inputUnit, outputUnit]);

  return (
    <FeaturePageTemplate
      title="Area Converter"
      description="Convert between different units of area"
    >
      <Grid container spacing={4} justifyContent="center">
        <Grid item xs={12} md={8}>
          <Paper elevation={1} className="p-6">
            <Grid container spacing={3}>
              <Grid item xs={12} md={6}>
                <FormControl fullWidth>
                  <InputLabel>From</InputLabel>
                  <Select
                    value={inputUnit}
                    label="From"
                    onChange={(e) => setInputUnit(e.target.value)}
                  >
                    {AREA_UNITS.map((unit) => (
                      <MenuItem key={unit.value} value={unit.value}>
                        {unit.label}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>

              <Grid item xs={12} md={6}>
                <FormControl fullWidth>
                  <InputLabel>To</InputLabel>
                  <Select
                    value={outputUnit}
                    label="To"
                    onChange={(e) => setOutputUnit(e.target.value)}
                  >
                    {AREA_UNITS.map((unit) => (
                      <MenuItem key={unit.value} value={unit.value}>
                        {unit.label}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>

              <Grid item xs={12} md={6}>
                <TextField
                  fullWidth
                  label="Enter Value"
                  type="number"
                  value={inputValue}
                  onChange={(e) => setInputValue(e.target.value)}
                  placeholder="Enter a number"
                />
              </Grid>

              <Grid item xs={12} md={6}>
                <TextField
                  fullWidth
                  label="Result"
                  value={outputValue}
                  InputProps={{ readOnly: true }}
                />
              </Grid>
            </Grid>

            <Typography className="mt-6 text-sm text-gray-600">
              Note: Results are rounded to 6 decimal places for precision.
            </Typography>
          </Paper>
        </Grid>
      </Grid>
    </FeaturePageTemplate>
  );
};

export default AreaConverter;

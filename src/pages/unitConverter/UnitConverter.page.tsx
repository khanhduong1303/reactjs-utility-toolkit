import React from 'react';
import { Typography, Grid, Paper } from '@mui/material';
import { Link } from 'react-router-dom';
import {
  Speed,
  Scale,
  Height,
  Timer,
  Memory,
  Thermostat,
} from '@mui/icons-material';
import FeaturePageTemplate from '@components/FeaturePageTemplate';

interface Tool {
  title: string;
  description: string;
  icon: React.ReactNode;
  path: string;
}

const tools: Tool[] = [
  {
    title: 'Length Converter',
    description: 'Convert between different units of length',
    icon: <Height className="text-4xl" />,
    path: '/unit-converter/length',
  },
  {
    title: 'Weight Converter',
    description: 'Convert between different units of weight',
    icon: <Scale className="text-4xl" />,
    path: '/unit-converter/weight',
  },
  {
    title: 'Speed Converter',
    description: 'Convert between different units of speed',
    icon: <Speed className="text-4xl" />,
    path: '/unit-converter/speed',
  },
  {
    title: 'Time Converter',
    description: 'Convert between different units of time',
    icon: <Timer className="text-4xl" />,
    path: '/unit-converter/time',
  },
  {
    title: 'Data Size Converter',
    description: 'Convert between different units of digital storage',
    icon: <Memory className="text-4xl" />,
    path: '/unit-converter/data',
  },
  {
    title: 'Temperature Converter',
    description: 'Convert between different temperature scales',
    icon: <Thermostat className="text-4xl" />,
    path: '/unit-converter/temperature',
  },
];

const UnitConverter = () => {
  return (
    <FeaturePageTemplate
      title="Unit Converter"
      description="Free online tools for unit conversion"
    >
      <Grid container spacing={4}>
        {tools.map((tool) => (
          <Grid item xs={12} sm={6} md={4} key={tool.path}>
            <Link to={tool.path} className="no-underline">
              <Paper
                elevation={1}
                className="p-6 h-full transition-all hover:shadow-md"
              >
                <div className="flex flex-col items-center text-center space-y-4">
                  {tool.icon}
                  <Typography variant="h6" className="font-bold">
                    {tool.title}
                  </Typography>
                  <Typography className="text-secondary-600">
                    {tool.description}
                  </Typography>
                </div>
              </Paper>
            </Link>
          </Grid>
        ))}
      </Grid>
    </FeaturePageTemplate>
  );
};

export default UnitConverter;

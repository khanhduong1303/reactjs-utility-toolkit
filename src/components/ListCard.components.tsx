import { ImageList, ImageListItem, ImageListItemBar } from '@mui/material';

interface ICard {
  title: string;
  description: string;
  img: string;
  link?: string;
  onPress?: (...args: any) => void;
}

interface IProps {
  cards: ICard[];
  cols?: number;
  onClick?: (card: ICard) => void;
}

const ListCard = ({ cards, cols = 2, onClick }: IProps) => {
  return (
    <ImageList cols={cols} gap={8}>
      {cards.map((card, idx) => (
        <ImageListItem key={idx} onClick={() => onClick?.(card)}>
          <img src={card.img} alt={card.title} loading="lazy" />
          <ImageListItemBar title={card.title} subtitle={card.description} />
        </ImageListItem>
      ))}
    </ImageList>
  );
};

export default ListCard;

import React, { useState } from 'react';
import {
  Typography,
  Grid,
  Box,
  Button,
  Select,
  MenuItem,
  FormControl,
  InputLabel,
  Alert,
} from '@mui/material';
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter';
import { vscDarkPlus } from 'react-syntax-highlighter/dist/esm/styles/prism';
import FeaturePageTemplate from '@components/FeaturePageTemplate';
import Notification from '@components/Notification';

const LANGUAGE_OPTIONS = [
  { value: 'javascript', label: 'JavaScript' },
  { value: 'typescript', label: 'TypeScript' },
  { value: 'html', label: 'HTML' },
  { value: 'css', label: 'CSS' },
];

const CodeFormatter = () => {
  const [code, setCode] = useState('');
  const [language, setLanguage] = useState('javascript');
  const [formattedCode, setFormattedCode] = useState('');
  const [error, setError] = useState('');
  const [notification, setNotification] = useState({
    open: false,
    message: '',
  });

  const formatJavaScript = (code: string) => {
    try {
      // Basic JavaScript formatting
      const formatted = code
        // Add space after keywords
        .replace(/\b(if|for|while|switch|catch)\(/g, '$1 (')
        // Add space around operators
        .replace(/([+\-*/%=<>!&|])([\w\d{(/])/g, '$1 $2')
        .replace(/([\w\d})/])([+\-*/%=<>!&|])/g, '$1 $2')
        // Add newline after semicolons
        .replace(/;(?!\n)/g, ';\n')
        // Add newline after opening braces
        .replace(/{(?!\n)/g, '{\n')
        // Add newline before closing braces
        .replace(/(?<!\n)}/g, '\n}')
        // Add newline after commas in arrays/objects
        .replace(/,(?!\n)/g, ',\n')
        // Indent code blocks
        .split('\n')
        .map((line) => {
          const indentLevel =
            (line.match(/{/g) || []).length - (line.match(/}/g) || []).length;
          return '  '.repeat(Math.max(0, indentLevel)) + line.trim();
        })
        .join('\n');

      return formatted;
    } catch {
      throw new Error('Invalid JavaScript syntax');
    }
  };

  const formatHTML = (code: string) => {
    try {
      // Basic HTML formatting
      const formatted = code
        // Add newline after opening tags
        .replace(/(<[^/][^>]*>)(?!\n)/g, '$1\n')
        // Add newline before closing tags
        .replace(/(?<!\n)(<\/[^>]+>)/g, '\n$1')
        // Indent based on tag nesting
        .split('\n')
        .map((line) => {
          const indentLevel =
            (line.match(/<[^/][^>]*>/g) || []).length -
            (line.match(/<\/[^>]+>/g) || []).length;
          return '  '.repeat(Math.max(0, indentLevel)) + line.trim();
        })
        .join('\n');

      return formatted;
    } catch {
      throw new Error('Invalid HTML syntax');
    }
  };

  const formatCSS = (code: string) => {
    try {
      // Basic CSS formatting
      const formatted = code
        // Add newline after opening braces
        .replace(/{(?!\n)/g, '{\n')
        // Add newline before closing braces
        .replace(/(?<!\n)}/g, '\n}')
        // Add newline after semicolons
        .replace(/;(?!\n)/g, ';\n')
        // Indent rules
        .split('\n')
        .map((line) => {
          const indentLevel =
            (line.match(/{/g) || []).length - (line.match(/}/g) || []).length;
          return '  '.repeat(Math.max(0, indentLevel)) + line.trim();
        })
        .join('\n');

      return formatted;
    } catch {
      throw new Error('Invalid CSS syntax');
    }
  };

  const formatCode = () => {
    if (!code.trim()) return;

    try {
      setError('');
      let formatted = '';

      switch (language) {
        case 'javascript':
        case 'typescript':
          formatted = formatJavaScript(code);
          break;
        case 'html':
          formatted = formatHTML(code);
          break;
        case 'css':
          formatted = formatCSS(code);
          break;
        default:
          formatted = code;
      }

      setFormattedCode(formatted);
    } catch (err) {
      setError('Error formatting code. Please check your syntax.');
      console.error('Formatting error:', err);
    }
  };

  return (
    <FeaturePageTemplate
      title="Code Formatter"
      description="Format and beautify your code with syntax highlighting"
    >
      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <Grid container spacing={4}>
        <Grid item xs={12}>
          <Box className="glass-card p-6">
            <div className="space-y-4">
              <div className="flex justify-between items-center">
                <FormControl variant="outlined" size="small">
                  <InputLabel>Language</InputLabel>
                  <Select
                    value={language}
                    onChange={(e) => setLanguage(e.target.value)}
                    label="Language"
                    className="min-w-[200px]"
                  >
                    {LANGUAGE_OPTIONS.map((option) => (
                      <MenuItem key={option.value} value={option.value}>
                        {option.label}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
                <Button
                  variant="contained"
                  onClick={formatCode}
                  disabled={!code.trim()}
                >
                  Format Code
                </Button>
              </div>

              <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
                <div>
                  <Typography variant="subtitle2" className="mb-2">
                    Input Code
                  </Typography>
                  <textarea
                    value={code}
                    onChange={(e) => setCode(e.target.value)}
                    className="w-full h-[400px] p-4 font-mono text-sm bg-[#1E1E1E] text-white rounded-lg focus:outline-none focus:ring-2 focus:ring-primary-500"
                    placeholder="Paste your code here..."
                  />
                </div>
                <div>
                  <Typography variant="subtitle2" className="mb-2">
                    Formatted Code
                  </Typography>
                  <div className="h-[400px] overflow-auto rounded-lg">
                    <SyntaxHighlighter
                      language={language}
                      style={vscDarkPlus}
                      customStyle={{
                        margin: 0,
                        minHeight: '400px',
                        fontSize: '14px',
                      }}
                    >
                      {formattedCode || '// Formatted code will appear here'}
                    </SyntaxHighlighter>
                  </div>
                </div>
              </div>
            </div>
          </Box>
        </Grid>
      </Grid>

      <Notification
        open={notification.open}
        message={notification.message}
        onClose={() => setNotification({ ...notification, open: false })}
      />
    </FeaturePageTemplate>
  );
};

export default CodeFormatter;

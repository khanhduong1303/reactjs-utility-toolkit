import React, { useState, useEffect } from 'react';
import { Typography, Box, TextField } from '@mui/material';
import FeaturePageTemplate from '@components/FeaturePageTemplate';

interface Stats {
  characters: number;
  words: number;
  sentences: number;
  paragraphs: number;
}

const WordCount = () => {
  const [text, setText] = useState('');
  const [stats, setStats] = useState<Stats>({
    characters: 0,
    words: 0,
    sentences: 0,
    paragraphs: 0,
  });

  useEffect(() => {
    const characters = text.length;
    const words = text.trim() ? text.trim().split(/\s+/).length : 0;
    const sentences = text.trim()
      ? text.split(/[.!?]+/).filter(Boolean).length
      : 0;
    const paragraphs = text.trim()
      ? text.split(/\n\s*\n/).filter(Boolean).length
      : 0;

    setStats({
      characters,
      words,
      sentences,
      paragraphs,
    });
  }, [text]);

  return (
    <FeaturePageTemplate
      title="Word Count"
      description="Count words, characters, sentences, and paragraphs"
    >
      <Box className="space-y-6">
        <TextField
          fullWidth
          multiline
          rows={8}
          label="Enter text to analyze"
          value={text}
          onChange={(e) => setText(e.target.value)}
        />

        <Box className="grid grid-cols-2 sm:grid-cols-4 gap-4">
          <Box className="p-4 bg-gray-100 rounded-lg">
            <Typography variant="h6" align="center">
              {stats.characters}
            </Typography>
            <Typography variant="body2" align="center" color="text.secondary">
              Characters
            </Typography>
          </Box>
          <Box className="p-4 bg-gray-100 rounded-lg">
            <Typography variant="h6" align="center">
              {stats.words}
            </Typography>
            <Typography variant="body2" align="center" color="text.secondary">
              Words
            </Typography>
          </Box>
          <Box className="p-4 bg-gray-100 rounded-lg">
            <Typography variant="h6" align="center">
              {stats.sentences}
            </Typography>
            <Typography variant="body2" align="center" color="text.secondary">
              Sentences
            </Typography>
          </Box>
          <Box className="p-4 bg-gray-100 rounded-lg">
            <Typography variant="h6" align="center">
              {stats.paragraphs}
            </Typography>
            <Typography variant="body2" align="center" color="text.secondary">
              Paragraphs
            </Typography>
          </Box>
        </Box>
      </Box>
    </FeaturePageTemplate>
  );
};

export default WordCount;

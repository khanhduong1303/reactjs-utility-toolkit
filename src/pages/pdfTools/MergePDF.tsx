import React, { useState } from 'react';
import { Typography, Button, Box, Alert } from '@mui/material';
import { CloudUpload as UploadIcon } from '@mui/icons-material';
import FeaturePageTemplate from '@components/FeaturePageTemplate';
import Notification from '@components/Notification';

const MergePDF = () => {
  const [selectedFiles, setSelectedFiles] = useState<FileList | null>(null);
  const [error, setError] = useState('');
  const [processing, setProcessing] = useState(false);
  const [notification, setNotification] = useState({
    open: false,
    message: '',
  });

  const handleFileUpload = (event: React.ChangeEvent<HTMLInputElement>) => {
    const files = event.target.files;
    if (files) {
      const invalidFiles = Array.from(files).filter(
        (file) => file.type !== 'application/pdf'
      );
      if (invalidFiles.length > 0) {
        setError('Please upload valid PDF files only');
        return;
      }
      setSelectedFiles(files);
      setError('');
    }
  };

  const handleMerge = async () => {
    setProcessing(true);
    await new Promise((resolve) => setTimeout(resolve, 2000));
    setProcessing(false);
    setNotification({
      open: true,
      message: 'This is a demo version. PDF merging is not implemented yet.',
    });
  };

  return (
    <FeaturePageTemplate
      title="Merge PDF"
      description="Combine multiple PDF files into one"
    >
      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <Box className="space-y-6">
        <input
          type="file"
          accept=".pdf"
          multiple
          onChange={handleFileUpload}
          style={{ display: 'none' }}
          id="pdf-upload"
        />
        <label htmlFor="pdf-upload">
          <Button
            variant="outlined"
            component="span"
            startIcon={<UploadIcon />}
            fullWidth
          >
            Upload PDFs
          </Button>
        </label>

        {selectedFiles && (
          <>
            <Typography variant="body2">
              Selected files: {selectedFiles.length} PDFs
            </Typography>

            <Button
              variant="contained"
              onClick={handleMerge}
              disabled={processing}
              fullWidth
            >
              {processing ? 'Merging...' : 'Merge PDFs'}
            </Button>
          </>
        )}
      </Box>

      <Notification
        open={notification.open}
        message={notification.message}
        onClose={() => setNotification({ ...notification, open: false })}
      />
    </FeaturePageTemplate>
  );
};

export default MergePDF;

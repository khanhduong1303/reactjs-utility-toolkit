import React, { useState } from 'react';
import {
  Typography,
  Grid,
  Button,
  Box,
  Alert,
  CircularProgress,
} from '@mui/material';
import { CloudUpload as UploadIcon } from '@mui/icons-material';
import heic2any from 'heic2any';
import FeaturePageTemplate from '@components/FeaturePageTemplate';

const HeicToJpg = () => {
  const [selectedFiles, setSelectedFiles] = useState<File[]>([]);
  const [converting, setConverting] = useState(false);
  const [error, setError] = useState('');
  const [progress, setProgress] = useState(0);

  const handleFileUpload = (event: React.ChangeEvent<HTMLInputElement>) => {
    const files = Array.from(event.target.files || []);
    const validFiles = files.filter(
      (file) =>
        file.type === 'image/heic' ||
        file.type === 'image/heif' ||
        file.name.toLowerCase().endsWith('.heic') ||
        file.name.toLowerCase().endsWith('.heif')
    );

    if (validFiles.length === 0) {
      setError('Please upload valid HEIC/HEIF files');
      return;
    }

    setSelectedFiles(validFiles);
    setError('');
    setProgress(0);
  };

  const convertFiles = async () => {
    if (selectedFiles.length === 0) return;

    setConverting(true);
    setError('');
    setProgress(0);

    try {
      for (let i = 0; i < selectedFiles.length; i++) {
        const file = selectedFiles[i];
        const blob = await heic2any({
          blob: file,
          toType: 'image/jpeg',
          quality: 0.8,
        });

        // Handle both single blob and array of blobs
        const blobs = Array.isArray(blob) ? blob : [blob];

        blobs.forEach((jpegBlob, index) => {
          const url = URL.createObjectURL(jpegBlob);
          const link = document.createElement('a');
          link.href = url;
          link.download = `${file.name.replace(/\.[^/.]+$/, '')}_${
            index + 1
          }.jpg`;
          link.click();
          URL.revokeObjectURL(url);
        });

        setProgress(((i + 1) / selectedFiles.length) * 100);
      }
    } catch (err) {
      setError('Failed to convert some files. Please try again.');
      console.error('Conversion error:', err);
    } finally {
      setConverting(false);
    }
  };

  return (
    <FeaturePageTemplate
      title="HEIC to JPG Converter"
      description="Convert HEIC/HEIF images to JPG format"
    >
      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <Grid container spacing={4} justifyContent="center">
        <Grid item xs={12} md={6}>
          <Box className="space-y-6">
            <input
              type="file"
              accept=".heic,.heif"
              onChange={handleFileUpload}
              className="hidden"
              id="heic-upload"
              multiple
            />
            <label htmlFor="heic-upload">
              <Button
                variant="outlined"
                component="span"
                startIcon={<UploadIcon />}
                fullWidth
              >
                Upload HEIC/HEIF Images
              </Button>
            </label>

            {selectedFiles.length > 0 && (
              <div className="space-y-4">
                <Typography variant="body2">
                  Selected files: {selectedFiles.length}
                </Typography>
                <div className="space-y-2">
                  {selectedFiles.map((file, index) => (
                    <Typography
                      key={index}
                      variant="body2"
                      className="text-gray-600"
                    >
                      {file.name} ({(file.size / 1024 / 1024).toFixed(2)} MB)
                    </Typography>
                  ))}
                </div>

                <Button
                  variant="contained"
                  onClick={convertFiles}
                  fullWidth
                  disabled={converting}
                >
                  {converting ? (
                    <div className="flex items-center space-x-2">
                      <CircularProgress size={20} color="inherit" />
                      <span>Converting... {Math.round(progress)}%</span>
                    </div>
                  ) : (
                    'Convert to JPG'
                  )}
                </Button>
              </div>
            )}
          </Box>
        </Grid>
      </Grid>
    </FeaturePageTemplate>
  );
};

export default HeicToJpg;

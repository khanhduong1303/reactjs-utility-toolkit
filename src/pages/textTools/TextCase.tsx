import React, { useState } from 'react';
import { Button, Box, Alert, TextField } from '@mui/material';
import FeaturePageTemplate from '@components/FeaturePageTemplate';
import Notification from '@components/Notification';

const TextCase = () => {
  const [text, setText] = useState('');
  const [error, setError] = useState('');
  const [processing, setProcessing] = useState(false);
  const [notification, setNotification] = useState({
    open: false,
    message: '',
  });

  const handleConvert = async (
    type: 'upper' | 'lower' | 'title' | 'sentence'
  ) => {
    if (!text.trim()) {
      setError('Please enter some text to convert');
      return;
    }

    setProcessing(true);
    let result = '';

    switch (type) {
      case 'upper':
        result = text.toUpperCase();
        break;
      case 'lower':
        result = text.toLowerCase();
        break;
      case 'title':
        result = text
          .toLowerCase()
          .split(' ')
          .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
          .join(' ');
        break;
      case 'sentence':
        result = text
          .toLowerCase()
          .replace(/(^\w|\.\s+\w)/g, (letter) => letter.toUpperCase());
        break;
    }

    setText(result);
    setProcessing(false);
    setNotification({
      open: true,
      message: 'Text case has been converted successfully!',
    });
  };

  return (
    <FeaturePageTemplate
      title="Text Case Converter"
      description="Convert text between different cases"
    >
      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <Box className="space-y-6">
        <TextField
          fullWidth
          multiline
          rows={8}
          label="Enter text to convert"
          value={text}
          onChange={(e) => setText(e.target.value)}
        />

        <Box className="grid grid-cols-1 sm:grid-cols-2 gap-4">
          <Button
            variant="contained"
            onClick={() => handleConvert('upper')}
            disabled={processing}
          >
            UPPERCASE
          </Button>
          <Button
            variant="contained"
            onClick={() => handleConvert('lower')}
            disabled={processing}
          >
            lowercase
          </Button>
          <Button
            variant="contained"
            onClick={() => handleConvert('title')}
            disabled={processing}
          >
            Title Case
          </Button>
          <Button
            variant="contained"
            onClick={() => handleConvert('sentence')}
            disabled={processing}
          >
            Sentence case
          </Button>
        </Box>
      </Box>

      <Notification
        open={notification.open}
        message={notification.message}
        onClose={() => setNotification({ ...notification, open: false })}
      />
    </FeaturePageTemplate>
  );
};

export default TextCase;

import React, { useState } from 'react';
import {
  Button,
  Box,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Alert,
} from '@mui/material';
import FeaturePageTemplate from '@components/FeaturePageTemplate';
import Notification from '@components/Notification';

const TextFormatter = () => {
  const [text, setText] = useState('');
  const [error, setError] = useState('');
  const [processing, setProcessing] = useState(false);
  const [format, setFormat] = useState('paragraph');
  const [notification, setNotification] = useState({
    open: false,
    message: '',
  });

  const handleFormat = async () => {
    if (!text.trim()) {
      setError('Please enter text to format');
      return;
    }

    setProcessing(true);
    await new Promise((resolve) => setTimeout(resolve, 2000));
    setProcessing(false);
    setNotification({
      open: true,
      message:
        'This is a demo version. Text formatting is not implemented yet.',
    });
  };

  return (
    <FeaturePageTemplate
      title="Text Formatter"
      description="Format and beautify your text"
    >
      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <Box className="space-y-6">
        <FormControl fullWidth>
          <InputLabel>Format Type</InputLabel>
          <Select
            value={format}
            label="Format Type"
            onChange={(e) => setFormat(e.target.value)}
          >
            <MenuItem value="paragraph">Paragraph</MenuItem>
            <MenuItem value="list">List</MenuItem>
            <MenuItem value="code">Code</MenuItem>
            <MenuItem value="quote">Quote</MenuItem>
          </Select>
        </FormControl>

        <TextField
          fullWidth
          multiline
          rows={8}
          label="Enter text to format"
          value={text}
          onChange={(e) => setText(e.target.value)}
        />

        <Button
          variant="contained"
          onClick={handleFormat}
          disabled={processing}
          fullWidth
        >
          {processing ? 'Formatting...' : 'Format Text'}
        </Button>
      </Box>

      <Notification
        open={notification.open}
        message={notification.message}
        onClose={() => setNotification({ ...notification, open: false })}
      />
    </FeaturePageTemplate>
  );
};

export default TextFormatter;

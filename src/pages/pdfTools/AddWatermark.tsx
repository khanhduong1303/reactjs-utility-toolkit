import React, { useState } from 'react';
import {
  Typography,
  Grid,
  Button,
  Box,
  Alert,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  SelectChangeEvent,
  Slider,
} from '@mui/material';
import { CloudUpload as UploadIcon } from '@mui/icons-material';
import { PDFDocument, rgb, degrees } from 'pdf-lib';
import { StandardFonts } from 'pdf-lib';
import FeaturePageTemplate from '@components/FeaturePageTemplate';

const AddWatermark = () => {
  const [selectedFile, setSelectedFile] = useState<File | null>(null);
  const [error, setError] = useState('');
  const [processing, setProcessing] = useState(false);
  const [watermarkText, setWatermarkText] = useState('');
  const [opacity, setOpacity] = useState(30);
  const [rotation, setRotation] = useState('45');
  const [fontSize, setFontSize] = useState(50);

  const handleFileUpload = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0];
    if (file) {
      if (file.type === 'application/pdf') {
        setSelectedFile(file);
        setError('');
      } else {
        setError('Please upload a valid PDF file');
      }
    }
  };

  const handleRotationChange = (event: SelectChangeEvent) => {
    setRotation(event.target.value);
  };

  const addWatermark = async () => {
    if (!selectedFile || !watermarkText) return;

    try {
      setProcessing(true);
      setError('');

      const fileData = await selectedFile.arrayBuffer();
      const pdfDoc = await PDFDocument.load(fileData);
      const helveticaFont = await pdfDoc.embedFont(StandardFonts.Helvetica);

      const pages = pdfDoc.getPages();
      pages.forEach((page) => {
        const { width, height } = page.getSize();
        const textWidth = helveticaFont.widthOfTextAtSize(
          watermarkText,
          fontSize
        );
        const textHeight = helveticaFont.heightAtSize(fontSize);

        // Position text in the center of the page
        const x = (width - textWidth) / 2;
        const y = (height - textHeight) / 2;

        page.drawText(watermarkText, {
          x,
          y,
          size: fontSize,
          font: helveticaFont,
          color: rgb(0, 0, 0),
          opacity: opacity / 100,
          rotate: degrees(Number(rotation)),
        });
      });

      const pdfBytes = await pdfDoc.save();
      const blob = new Blob([pdfBytes], { type: 'application/pdf' });
      const url = URL.createObjectURL(blob);
      const link = document.createElement('a');
      link.href = url;
      link.download = `watermarked_${selectedFile.name}`;
      link.click();
      URL.revokeObjectURL(url);
    } catch (err) {
      setError('Failed to add watermark. Please try again.');
      console.error('Watermark error:', err);
    } finally {
      setProcessing(false);
    }
  };

  return (
    <FeaturePageTemplate
      title="Add Watermark"
      description="Add text watermark to your PDF document"
    >
      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <Grid container spacing={4} justifyContent="center">
        <Grid item xs={12} md={6}>
          <Box className="space-y-6">
            <input
              type="file"
              accept=".pdf"
              onChange={handleFileUpload}
              className="hidden"
              id="pdf-upload"
            />
            <label htmlFor="pdf-upload">
              <Button
                variant="outlined"
                component="span"
                startIcon={<UploadIcon />}
                fullWidth
              >
                Upload PDF
              </Button>
            </label>

            {selectedFile && (
              <>
                <TextField
                  fullWidth
                  label="Watermark Text"
                  value={watermarkText}
                  onChange={(e) => setWatermarkText(e.target.value)}
                  placeholder="Enter watermark text"
                />

                <Box>
                  <Typography gutterBottom>Opacity</Typography>
                  <Slider
                    value={opacity}
                    onChange={(_, value) => setOpacity(value as number)}
                    aria-labelledby="opacity-slider"
                    valueLabelDisplay="auto"
                    min={10}
                    max={100}
                  />
                </Box>

                <Box>
                  <Typography gutterBottom>Font Size</Typography>
                  <Slider
                    value={fontSize}
                    onChange={(_, value) => setFontSize(value as number)}
                    aria-labelledby="font-size-slider"
                    valueLabelDisplay="auto"
                    min={20}
                    max={100}
                  />
                </Box>

                <FormControl fullWidth>
                  <InputLabel>Rotation Angle</InputLabel>
                  <Select
                    value={rotation}
                    label="Rotation Angle"
                    onChange={handleRotationChange}
                  >
                    <MenuItem value="0">No rotation</MenuItem>
                    <MenuItem value="45">45 degrees</MenuItem>
                    <MenuItem value="90">90 degrees</MenuItem>
                    <MenuItem value="-45">-45 degrees</MenuItem>
                  </Select>
                </FormControl>

                <Button
                  variant="contained"
                  onClick={addWatermark}
                  fullWidth
                  disabled={processing || !watermarkText}
                >
                  {processing ? 'Processing...' : 'Add Watermark'}
                </Button>
              </>
            )}
          </Box>
        </Grid>
      </Grid>
    </FeaturePageTemplate>
  );
};

export default AddWatermark;

import React, { useState } from 'react';
import { Typography, Button, Box, Alert, TextField } from '@mui/material';
import { CloudUpload as UploadIcon } from '@mui/icons-material';
import FeaturePageTemplate from '@components/FeaturePageTemplate';
import Notification from '@components/Notification';

const RemovePages = () => {
  const [selectedFile, setSelectedFile] = useState<File | null>(null);
  const [error, setError] = useState('');
  const [processing, setProcessing] = useState(false);
  const [pageNumbers, setPageNumbers] = useState('');
  const [totalPages, setTotalPages] = useState(0);
  const [notification, setNotification] = useState({
    open: false,
    message: '',
  });

  const handleFileUpload = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0];
    if (file) {
      if (file.type === 'application/pdf') {
        setSelectedFile(file);
        setError('');
        // In a real implementation, we would get the total pages here
        setTotalPages(10); // Demo value
      } else {
        setError('Please upload a valid PDF file');
      }
    }
  };

  const handleRemove = async () => {
    if (!pageNumbers.trim()) {
      setError('Please enter page numbers to remove');
      return;
    }

    setProcessing(true);
    await new Promise((resolve) => setTimeout(resolve, 2000));
    setProcessing(false);
    setNotification({
      open: true,
      message: 'This is a demo version. Page removal is not implemented yet.',
    });
  };

  return (
    <FeaturePageTemplate
      title="Remove Pages"
      description="Remove specific pages from your PDF file"
    >
      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <Box className="space-y-6">
        <input
          type="file"
          accept=".pdf"
          onChange={handleFileUpload}
          style={{ display: 'none' }}
          id="pdf-upload"
        />
        <label htmlFor="pdf-upload">
          <Button
            variant="outlined"
            component="span"
            startIcon={<UploadIcon />}
            fullWidth
          >
            Upload PDF
          </Button>
        </label>

        {selectedFile && (
          <>
            <Typography variant="body2">
              Selected file: {selectedFile.name}
            </Typography>
            <Typography variant="body2">Total pages: {totalPages}</Typography>

            <TextField
              fullWidth
              label="Pages to Remove"
              value={pageNumbers}
              onChange={(e) => setPageNumbers(e.target.value)}
              placeholder="e.g., 1,3,5-7"
              helperText="Enter page numbers separated by commas, or ranges with hyphens"
            />

            <Button
              variant="contained"
              onClick={handleRemove}
              disabled={processing}
              fullWidth
            >
              {processing ? 'Removing Pages...' : 'Remove Pages'}
            </Button>
          </>
        )}
      </Box>

      <Notification
        open={notification.open}
        message={notification.message}
        onClose={() => setNotification({ ...notification, open: false })}
      />
    </FeaturePageTemplate>
  );
};

export default RemovePages;

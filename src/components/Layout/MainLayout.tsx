import React, { useState, useMemo } from 'react';
import {
  Box,
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  IconButton,
  Typography,
  useTheme,
  useMediaQuery,
  Tooltip,
  TextField,
  InputAdornment,
  Collapse,
  alpha,
  Paper,
  AppBar,
  Toolbar,
} from '@mui/material';
import {
  Menu as MenuIcon,
  Search as SearchIcon,
  ChevronRight,
  Clear as ClearIcon,
} from '@mui/icons-material';
import { useNavigate, useLocation } from 'react-router-dom';
import { menuConfig, MenuItem, MenuSection } from '../../configs/menu.config';
import { observer } from 'mobx-react';
import rootStore from '../../stores/Stores';
import AdSense from '../AdSense';
import { AD_UNITS } from '../../configs/adsense.config';

const DRAWER_WIDTH = 280;

const MainLayout: React.FC<{ children: React.ReactNode }> = observer(
  ({ children }) => {
    const [mobileOpen, setMobileOpen] = useState(false);
    const [searchQuery, setSearchQuery] = useState('');
    const [expandedItems, setExpandedItems] = useState<string[]>([]);
    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down('sm'));
    const navigate = useNavigate();
    const location = useLocation();
    const { userStore } = rootStore;

    const handleDrawerToggle = () => {
      setMobileOpen(!mobileOpen);
    };

    const handleItemClick = (item: MenuItem) => {
      if (item.subItems && item.subItems.length > 0) {
        setExpandedItems((prev) =>
          prev.includes(item.path)
            ? prev.filter((path) => path !== item.path)
            : [...prev, item.path]
        );
        navigate(item.path);
      } else {
        navigate(item.path);
        if (isMobile) {
          setMobileOpen(false);
        }
      }
    };

    const isItemExpanded = (path: string) => expandedItems.includes(path);

    const filteredMenuConfig = useMemo(() => {
      if (!searchQuery) return menuConfig;

      const query = searchQuery.toLowerCase();
      const results: MenuItem[] = [];

      menuConfig.forEach((section: MenuSection) => {
        section.items.forEach((item: MenuItem) => {
          if (item.subItems?.length) {
            const matchingSubItems = item.subItems.filter(
              (subItem: MenuItem) =>
                subItem.name.toLowerCase().includes(query) ||
                subItem.description?.toLowerCase().includes(query) ||
                subItem.keywords?.some((k: string) =>
                  k.toLowerCase().includes(query)
                )
            );
            if (matchingSubItems.length > 0) {
              results.push(...matchingSubItems);
            }
          }
        });
      });

      return [
        {
          title: `Search Results (${results.length})`,
          items: results,
        },
      ];
    }, [searchQuery]);

    const renderMenuItem = (item: MenuItem, isSubItem = false) => {
      const isSelected = location.pathname === item.path;
      const hasSubItems = item.subItems && item.subItems.length > 0;
      const isExpanded = isItemExpanded(item.path);

      return (
        <React.Fragment key={item.path}>
          <Tooltip title={item.description || ''} placement="right" arrow>
            <ListItem
              button
              onClick={() => handleItemClick(item)}
              className={`rounded-lg transition-all duration-200 ${
                isSubItem ? 'ml-4' : ''
              }`}
              sx={{
                mb: 0.5,
                pl: isSubItem ? 4 : 2,
                background: isSelected
                  ? `linear-gradient(90deg, ${alpha(
                      theme.palette.primary.main,
                      0.12
                    )} 0%, ${alpha(theme.palette.primary.main, 0.05)} 100%)`
                  : 'transparent',
                '&:hover': {
                  background: isSelected
                    ? `linear-gradient(90deg, ${alpha(
                        theme.palette.primary.main,
                        0.16
                      )} 0%, ${alpha(theme.palette.primary.main, 0.08)} 100%)`
                    : `linear-gradient(90deg, ${alpha(
                        theme.palette.secondary.main,
                        0.08
                      )} 0%, ${alpha(
                        theme.palette.secondary.main,
                        0.02
                      )} 100%)`,
                },
                '&:before': isSubItem
                  ? {
                      content: '""',
                      position: 'absolute',
                      left: '24px',
                      top: '50%',
                      width: '16px',
                      height: '1px',
                      backgroundColor: alpha(theme.palette.text.primary, 0.1),
                    }
                  : {},
              }}
            >
              <ListItemIcon
                sx={{
                  minWidth: 36,
                  color: isSelected
                    ? theme.palette.primary.main
                    : theme.palette.text.secondary,
                }}
              >
                <item.icon fontSize={isSubItem ? 'small' : 'medium'} />
              </ListItemIcon>
              <ListItemText
                primary={item.name}
                sx={{
                  '& .MuiTypography-root': {
                    fontSize: isSubItem ? '0.875rem' : '0.9375rem',
                    fontWeight: isSelected ? 600 : 400,
                    color: isSelected
                      ? theme.palette.primary.main
                      : theme.palette.text.primary,
                  },
                }}
              />
              {hasSubItems && (
                <Box
                  component="span"
                  sx={{
                    ml: 'auto',
                    transition: 'transform 0.3s',
                    transform: isExpanded ? 'rotate(90deg)' : 'none',
                  }}
                >
                  <ChevronRight
                    fontSize="small"
                    sx={{
                      color: isSelected
                        ? theme.palette.primary.main
                        : theme.palette.text.secondary,
                    }}
                  />
                </Box>
              )}
            </ListItem>
          </Tooltip>
          {hasSubItems && item.subItems && (
            <Collapse in={isExpanded} timeout="auto" unmountOnExit>
              <List component="div" disablePadding>
                {item.subItems.map((subItem: MenuItem) =>
                  renderMenuItem(subItem, true)
                )}
              </List>
            </Collapse>
          )}
        </React.Fragment>
      );
    };

    const drawer = (
      <Box
        className="h-full"
        sx={{ bgcolor: userStore.theme === 'dark' ? 'grey.900' : 'grey.50' }}
      >
        <Paper
          elevation={0}
          className="p-4 sticky top-0 z-10 backdrop-blur-sm border-b"
          sx={{ bgcolor: userStore.theme === 'dark' ? 'grey.900' : 'grey.50' }}
        >
          <Typography
            variant="h6"
            className="font-bold mb-3 bg-gradient-to-r from-primary-600 to-secondary-600 bg-clip-text text-transparent"
          >
            Utility Toolkit
          </Typography>
          <TextField
            fullWidth
            size="small"
            placeholder="Search tools..."
            value={searchQuery}
            onChange={(e) => setSearchQuery(e.target.value)}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <SearchIcon className="text-secondary-400" />
                </InputAdornment>
              ),
              endAdornment: searchQuery ? (
                <InputAdornment position="end">
                  <IconButton
                    size="small"
                    onClick={() => setSearchQuery('')}
                    edge="end"
                    className="text-secondary-400"
                  >
                    <ClearIcon fontSize="small" />
                  </IconButton>
                </InputAdornment>
              ) : null,
            }}
            sx={{
              '& .MuiOutlinedInput-root': {
                borderRadius: '12px',
                backgroundColor: alpha(theme.palette.primary.main, 0.04),
                backdropFilter: 'blur(8px)',
                '&:hover': {
                  backgroundColor: alpha(theme.palette.primary.main, 0.08),
                },
                '& fieldset': {
                  borderColor: 'transparent',
                },
                '&:hover fieldset': {
                  borderColor: 'transparent',
                },
                '&.Mui-focused fieldset': {
                  borderColor: theme.palette.primary.main,
                },
              },
            }}
          />
        </Paper>
        <Box className="overflow-y-auto h-[calc(100%-130px)]">
          {filteredMenuConfig.map((section: MenuSection) => (
            <Box key={section.title} className="p-2">
              <Typography
                variant="subtitle2"
                className="px-3 py-2 text-secondary-600 font-medium"
                sx={{
                  fontSize: '0.75rem',
                  textTransform: 'uppercase',
                  letterSpacing: '0.1em',
                }}
              >
                {section.title}
              </Typography>
              <List>
                {section.items.map((item: MenuItem) => renderMenuItem(item))}
              </List>
            </Box>
          ))}
        </Box>
        <Box className="p-4">
          <AdSense {...AD_UNITS.banner} />
        </Box>
      </Box>
    );

    return (
      <Box
        className="flex h-screen"
        sx={{ bgcolor: userStore.theme === 'dark' ? 'grey.900' : 'grey.50' }}
      >
        <Box
          component="nav"
          sx={{
            width: { sm: DRAWER_WIDTH },
            flexShrink: { sm: 0 },
          }}
        >
          <Drawer
            variant="temporary"
            open={mobileOpen}
            onClose={handleDrawerToggle}
            ModalProps={{
              keepMounted: true,
            }}
            sx={{
              display: { xs: 'block', sm: 'none' },
              '& .MuiDrawer-paper': {
                boxSizing: 'border-box',
                width: DRAWER_WIDTH,
                bgcolor: userStore.theme === 'dark' ? 'grey.900' : 'grey.50',
                borderRight: 'none',
                borderRadius: 0,
              },
            }}
          >
            {drawer}
          </Drawer>
          <Drawer
            variant="permanent"
            sx={{
              display: { xs: 'none', sm: 'block' },
              '& .MuiDrawer-paper': {
                boxSizing: 'border-box',
                width: DRAWER_WIDTH,
                bgcolor: userStore.theme === 'dark' ? 'grey.900' : 'grey.50',
                borderRight: 'none',
                borderRadius: 0,
              },
            }}
            open
          >
            {drawer}
          </Drawer>
        </Box>
        <AppBar
          position="fixed"
          sx={{
            width: { sm: `calc(100% - ${DRAWER_WIDTH}px)` },
            ml: { sm: `${DRAWER_WIDTH}px` },
            background: 'linear-gradient(90deg, #3b82f6 0%, #6366f1 100%)',
            boxShadow: 'none',
            backdropFilter: 'blur(8px)',
            display: { sm: 'none' },
            borderRadius: 0,
          }}
        >
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              edge="start"
              onClick={handleDrawerToggle}
              sx={{
                mr: 2,
                display: { sm: 'none' },
                background:
                  'linear-gradient(45deg, rgba(255,255,255,0.1) 0%, rgba(255,255,255,0.2) 100%)',
                backdropFilter: 'blur(4px)',
                borderRadius: 0,
                '&:hover': {
                  background:
                    'linear-gradient(45deg, rgba(255,255,255,0.2) 0%, rgba(255,255,255,0.3) 100%)',
                },
              }}
            >
              <MenuIcon />
            </IconButton>
            <Typography
              variant="h6"
              noWrap
              component="div"
              sx={{
                background: 'linear-gradient(45deg, #ffffff 30%, #f0f0f0 90%)',
                WebkitBackgroundClip: 'text',
                WebkitTextFillColor: 'transparent',
                fontWeight: 'bold',
              }}
            >
              Utility Toolkit
            </Typography>
            <Box sx={{ flexGrow: 1 }} />
          </Toolbar>
        </AppBar>
        <Box
          component="main"
          className="flex-grow overflow-auto"
          sx={{
            width: { sm: `calc(100% - ${DRAWER_WIDTH}px)` },
          }}
        >
          <Box
            className="sticky top-0 z-10 bg-white/80 backdrop-blur-sm border-b-0 flex items-center justify-between px-4"
            sx={{ display: { sm: 'none' } }}
          >
            <IconButton
              color="inherit"
              aria-label="open drawer"
              edge="start"
              onClick={handleDrawerToggle}
              className="p-2"
              sx={{
                background: (theme) =>
                  `linear-gradient(45deg, ${alpha(
                    theme.palette.primary.main,
                    0.08
                  )}, ${alpha(theme.palette.secondary.main, 0.08)})`,
                borderRadius: 0,
                '&:hover': {
                  background: (theme) =>
                    `linear-gradient(45deg, ${alpha(
                      theme.palette.primary.main,
                      0.12
                    )}, ${alpha(theme.palette.secondary.main, 0.12)})`,
                },
              }}
            >
              <MenuIcon />
            </IconButton>
            <Typography
              variant="h6"
              className="font-bold bg-gradient-to-r from-primary-600 to-secondary-600 bg-clip-text text-transparent"
            >
              Utility Toolkit
            </Typography>
            <Box sx={{ width: 40 }} />
          </Box>
          <Box className="p-4 md:p-6">{children}</Box>
        </Box>
      </Box>
    );
  }
);

export default MainLayout;

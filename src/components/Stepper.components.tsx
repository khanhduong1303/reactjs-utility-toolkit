import * as React from 'react'
import Box from '@mui/material/Box'
import Stepper from '@mui/material/Stepper'
import Step from '@mui/material/Step'
import StepLabel from '@mui/material/StepLabel'
import { StepButton } from '@mui/material'

interface IProps {
  steps: string[]
  activeStep: number
  onChange?: (...args: any) => void
}

const HorizontalStepper = ({ steps, activeStep, onChange }: IProps) => {
  const handleChange = (index: number) => {
    onChange?.(index)
  }
  return (
    <Box sx={{ width: '100%' }}>
      <Stepper activeStep={activeStep} alternativeLabel>
        {steps.map((label, index) => (
          <Step key={label}>
            {onChange ? (
              <StepButton color="inherit" onClick={() => handleChange(index)}>
                {label}
              </StepButton>
            ) : (
              <StepLabel>{label}</StepLabel>
            )}
          </Step>
        ))}
      </Stepper>
    </Box>
  )
}

export default HorizontalStepper

import React, { useState } from 'react';
import { Button, Box, Alert } from '@mui/material';
import { CloudUpload as UploadIcon } from '@mui/icons-material';
import FeaturePageTemplate from '@components/FeaturePageTemplate';
import Notification from '@components/Notification';

const ScanQR = () => {
  const [error, setError] = useState('');
  const [notification, setNotification] = useState({
    open: false,
    message: '',
  });

  const handleFileUpload = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0];
    if (file) {
      if (file.type.startsWith('image/')) {
        setError('');
        handleScan();
      } else {
        setError('Please upload a valid image file');
      }
    }
  };

  const handleScan = async () => {
    await new Promise((resolve) => setTimeout(resolve, 2000));
    setNotification({
      open: true,
      message: 'This is a demo version. QR scanning is not implemented yet.',
    });
  };

  return (
    <FeaturePageTemplate
      title="Scan QR Code"
      description="Scan QR codes from images"
    >
      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <Box className="space-y-6">
        <input
          type="file"
          accept="image/*"
          onChange={handleFileUpload}
          style={{ display: 'none' }}
          id="qr-upload"
        />
        <label htmlFor="qr-upload">
          <Button
            variant="outlined"
            component="span"
            startIcon={<UploadIcon />}
            fullWidth
          >
            Upload Image
          </Button>
        </label>
      </Box>

      <Notification
        open={notification.open}
        message={notification.message}
        onClose={() => setNotification({ ...notification, open: false })}
      />
    </FeaturePageTemplate>
  );
};

export default ScanQR;

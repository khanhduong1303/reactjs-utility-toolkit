import React, { useState } from 'react';
import { Typography, Grid, Button, Box, Alert, TextField } from '@mui/material';
import { CloudUpload as UploadIcon } from '@mui/icons-material';
import { PDFDocument } from 'pdf-lib';
import FeaturePageTemplate from '@components/FeaturePageTemplate';

const SplitPDF = () => {
  const [selectedFile, setSelectedFile] = useState<File | null>(null);
  const [error, setError] = useState('');
  const [splitPage, setSplitPage] = useState('');
  const [splitting, setSplitting] = useState(false);
  const [totalPages, setTotalPages] = useState(0);

  const handleFileUpload = async (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const file = event.target.files?.[0];
    if (file) {
      if (file.type === 'application/pdf') {
        try {
          const fileData = await file.arrayBuffer();
          const pdfDoc = await PDFDocument.load(fileData);
          setTotalPages(pdfDoc.getPageCount());
          setSelectedFile(file);
          setError('');
        } catch (err) {
          setError('Failed to load PDF. Please try again.');
          console.error('Loading error:', err);
        }
      } else {
        setError('Please upload a valid PDF file');
      }
    }
  };

  const validateSplitPage = (page: string): number | null => {
    const pageNum = Number(page);
    if (isNaN(pageNum) || pageNum < 1 || pageNum >= totalPages) {
      return null;
    }
    return pageNum;
  };

  const splitPDF = async () => {
    if (!selectedFile || !splitPage) return;

    const pageNum = validateSplitPage(splitPage);
    if (pageNum === null) {
      setError('Invalid split page number. Please check your input.');
      return;
    }

    try {
      setSplitting(true);
      setError('');

      const fileData = await selectedFile.arrayBuffer();
      const pdfDoc = await PDFDocument.load(fileData);

      // Create first part
      const firstPart = await PDFDocument.create();
      const firstPages = await firstPart.copyPages(
        pdfDoc,
        Array.from({ length: pageNum }, (_, i) => i)
      );
      firstPages.forEach((page) => firstPart.addPage(page));

      // Create second part
      const secondPart = await PDFDocument.create();
      const secondPages = await secondPart.copyPages(
        pdfDoc,
        Array.from({ length: totalPages - pageNum }, (_, i) => i + pageNum)
      );
      secondPages.forEach((page) => secondPart.addPage(page));

      // Save and download first part
      const firstPdfBytes = await firstPart.save();
      const firstBlob = new Blob([firstPdfBytes], { type: 'application/pdf' });
      const firstUrl = URL.createObjectURL(firstBlob);
      const firstLink = document.createElement('a');
      firstLink.href = firstUrl;
      firstLink.download = `part1_${selectedFile.name}`;
      firstLink.click();
      URL.revokeObjectURL(firstUrl);

      // Save and download second part
      const secondPdfBytes = await secondPart.save();
      const secondBlob = new Blob([secondPdfBytes], {
        type: 'application/pdf',
      });
      const secondUrl = URL.createObjectURL(secondBlob);
      const secondLink = document.createElement('a');
      secondLink.href = secondUrl;
      secondLink.download = `part2_${selectedFile.name}`;
      secondLink.click();
      URL.revokeObjectURL(secondUrl);
    } catch (err) {
      setError('Failed to split PDF. Please try again.');
      console.error('Splitting error:', err);
    } finally {
      setSplitting(false);
    }
  };

  return (
    <FeaturePageTemplate
      title="Split PDF"
      description="Split your PDF document into two parts at a specific page"
    >
      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <Grid container spacing={4} justifyContent="center">
        <Grid item xs={12} md={6}>
          <Box className="space-y-6">
            <input
              type="file"
              accept=".pdf"
              onChange={handleFileUpload}
              className="hidden"
              id="pdf-upload"
            />
            <label htmlFor="pdf-upload">
              <Button
                variant="outlined"
                component="span"
                startIcon={<UploadIcon />}
                fullWidth
              >
                Upload PDF
              </Button>
            </label>

            {selectedFile && (
              <>
                <Typography variant="body2">
                  Total pages: {totalPages}
                </Typography>

                <TextField
                  fullWidth
                  label="Split at Page"
                  type="number"
                  placeholder="Enter page number"
                  value={splitPage}
                  onChange={(e) => setSplitPage(e.target.value)}
                  helperText={`Enter a page number between 1 and ${
                    totalPages - 1
                  }`}
                  InputProps={{ inputProps: { min: 1, max: totalPages - 1 } }}
                />

                <Button
                  variant="contained"
                  onClick={splitPDF}
                  fullWidth
                  disabled={splitting || !splitPage}
                >
                  {splitting ? 'Splitting...' : 'Split PDF'}
                </Button>
              </>
            )}
          </Box>
        </Grid>
      </Grid>
    </FeaturePageTemplate>
  );
};

export default SplitPDF;

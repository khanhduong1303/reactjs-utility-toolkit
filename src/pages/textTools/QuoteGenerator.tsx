import React, { useState } from 'react';
import {
  Button,
  Box,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Alert,
} from '@mui/material';
import FeaturePageTemplate from '@components/FeaturePageTemplate';
import Notification from '@components/Notification';

const QuoteGenerator = () => {
  const [category, setCategory] = useState('inspirational');
  const [error, setError] = useState('');
  const [processing, setProcessing] = useState(false);
  const [notification, setNotification] = useState({
    open: false,
    message: '',
  });

  const handleGenerate = async () => {
    setProcessing(true);
    await new Promise((resolve) => setTimeout(resolve, 2000));
    setProcessing(false);
    setNotification({
      open: true,
      message:
        'This is a demo version. Quote generation is not implemented yet.',
    });
  };

  return (
    <FeaturePageTemplate
      title="Quote Generator"
      description="Generate random quotes by category"
    >
      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <Box className="space-y-6">
        <FormControl fullWidth>
          <InputLabel>Category</InputLabel>
          <Select
            value={category}
            label="Category"
            onChange={(e) => setCategory(e.target.value)}
          >
            <MenuItem value="inspirational">Inspirational</MenuItem>
            <MenuItem value="motivational">Motivational</MenuItem>
            <MenuItem value="life">Life</MenuItem>
            <MenuItem value="love">Love</MenuItem>
            <MenuItem value="success">Success</MenuItem>
            <MenuItem value="wisdom">Wisdom</MenuItem>
          </Select>
        </FormControl>

        <Button
          variant="contained"
          onClick={handleGenerate}
          disabled={processing}
          fullWidth
        >
          {processing ? 'Generating...' : 'Generate Quote'}
        </Button>
      </Box>

      <Notification
        open={notification.open}
        message={notification.message}
        onClose={() => setNotification({ ...notification, open: false })}
      />
    </FeaturePageTemplate>
  );
};

export default QuoteGenerator;

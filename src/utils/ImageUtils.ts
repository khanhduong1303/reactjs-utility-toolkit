import heic2any from 'heic2any'
import imageCompression from 'browser-image-compression'

type FileType = 'image/png' | 'image/jpeg'

export const convertImage = async (
  file: File,
  outputFormat: FileType,
  onProgress?: (progress: number) => void,
): Promise<File> => {
  try {
    let convertedFile: File
    const newFileName = file.name.replace(
      /\..+$/,
      `.${outputFormat.split('/')[1]}`,
    )

    if (file.type === 'image/heic') {
      // Convert HEIC to the desired format
      const blob = await heic2any({
        blob: file,
        toType: outputFormat,
      })

      convertedFile = new File([blob as Blob], newFileName, {
        type: outputFormat,
      })
    } else {
      // Compress and convert other image formats
      const options = {
        fileType: outputFormat,
        maxWidthOrHeight: 1000,
        onProgress,
      }

      const compressedFile = await imageCompression(file, options)

      // Rename the converted file
      convertedFile = new File([compressedFile], newFileName, {
        type: compressedFile.type,
        lastModified: compressedFile.lastModified,
      })
    }

    return convertedFile
  } catch (error) {
    console.error('Error converting image:', error)
    throw error
  }
}

import React, { useState, useRef, useEffect } from 'react';
import {
  Box,
  Button,
  Typography,
  Alert,
  CircularProgress,
  TextField,
  Grid,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from '@mui/material';
import { useDropzone } from 'react-dropzone';
import FeaturePageTemplate from '@components/FeaturePageTemplate';

interface TrimSettings {
  format: string;
  startTime: string;
  endTime: string;
}

const TrimVideo = () => {
  const [video, setVideo] = useState<File | null>(null);
  const [trimming, setTrimming] = useState(false);
  const [progress, setProgress] = useState(0);
  const [previewUrl, setPreviewUrl] = useState<string>('');
  const [error, setError] = useState<string>('');
  const [currentTime, setCurrentTime] = useState('00:00');
  const videoRef = useRef<HTMLVideoElement>(null);
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const mediaRecorderRef = useRef<MediaRecorder | null>(null);

  const [settings, setSettings] = useState<TrimSettings>({
    format: 'mp4',
    startTime: '00:00',
    endTime: '00:00',
  });

  const formats = [
    { value: 'webm', label: 'WebM', mimeType: 'video/webm;codecs=vp8,opus' },
    { value: 'webm', label: 'WebM (VP9)', mimeType: 'video/webm;codecs=vp9' },
    { value: 'mp4', label: 'MP4', mimeType: 'video/mp4' },
    { value: 'mp4', label: 'MP4 (x264)', mimeType: 'video/mp4;codecs=avc1' },
  ];

  const getSupportedMimeType = () => {
    const defaultMimeType = 'video/webm;codecs=vp8,opus';
    if (!MediaRecorder.isTypeSupported) return defaultMimeType;

    for (const format of formats) {
      if (MediaRecorder.isTypeSupported(format.mimeType)) {
        return format.mimeType;
      }
    }
    return defaultMimeType;
  };

  useEffect(() => {
    return () => {
      if (previewUrl) {
        URL.revokeObjectURL(previewUrl);
      }
    };
  }, [previewUrl]);

  const formatTime = (seconds: number) => {
    const mins = Math.floor(seconds / 60);
    const secs = Math.floor(seconds % 60);
    return `${mins.toString().padStart(2, '0')}:${secs
      .toString()
      .padStart(2, '0')}`;
  };

  const onDrop = (acceptedFiles: File[]) => {
    const file = acceptedFiles[0];
    if (file && file.type.startsWith('video/')) {
      setVideo(file);
      const url = URL.createObjectURL(file);
      setPreviewUrl(url);
      setError('');
      setSettings((prev) => ({
        ...prev,
        startTime: '00:00',
        endTime: '00:00',
      }));

      if (videoRef.current) {
        videoRef.current.onloadedmetadata = () => {
          if (videoRef.current) {
            const duration = videoRef.current.duration;
            setSettings((prev) => ({
              ...prev,
              endTime: formatTime(duration),
            }));
          }
        };
      }
    } else {
      setError('Please upload a valid video file');
    }
  };

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop,
    accept: { 'video/*': [] },
    maxFiles: 1,
  });

  const timeToSeconds = (timeStr: string) => {
    const [minutes, seconds] = timeStr.split(':').map(Number);
    return minutes * 60 + seconds;
  };

  const handleTimeUpdate = () => {
    if (videoRef.current) {
      setCurrentTime(formatTime(videoRef.current.currentTime));
    }
  };

  const setCurrentAsStart = () => {
    const videoElement = videoRef.current;
    if (videoElement) {
      setSettings((prev) => ({
        ...prev,
        startTime: formatTime(videoElement.currentTime),
      }));
    }
  };

  const setCurrentAsEnd = () => {
    const videoElement = videoRef.current;
    if (videoElement) {
      setSettings((prev) => ({
        ...prev,
        endTime: formatTime(videoElement.currentTime),
      }));
    }
  };

  const handleSettingChange = (setting: keyof TrimSettings, value: string) => {
    setSettings((prev) => ({ ...prev, [setting]: value }));
  };

  const trimVideo = async () => {
    if (!video || !videoRef.current || !canvasRef.current) return;

    try {
      setTrimming(true);
      setProgress(0);
      setError('');

      const videoElement = videoRef.current;
      const canvas = canvasRef.current;
      const startSeconds = timeToSeconds(settings.startTime);
      const endSeconds = timeToSeconds(settings.endTime);

      if (startSeconds >= endSeconds) {
        throw new Error('Start time must be before end time');
      }

      // Set up canvas
      canvas.width = videoElement.videoWidth;
      canvas.height = videoElement.videoHeight;
      const ctx = canvas.getContext('2d');
      if (!ctx) throw new Error('Could not get canvas context');

      // Set up MediaRecorder with supported mime type
      const stream = canvas.captureStream(30); // 30 FPS
      const mimeType = getSupportedMimeType();

      const chunks: Blob[] = [];
      const mediaRecorder = new MediaRecorder(stream, {
        mimeType,
        videoBitsPerSecond: 5000000, // 5Mbps
      });

      mediaRecorderRef.current = mediaRecorder;

      mediaRecorder.ondataavailable = (e) => {
        if (e.data.size > 0) {
          chunks.push(e.data);
        }
      };

      mediaRecorder.onstop = () => {
        const blob = new Blob(chunks, { type: mimeType });
        const url = URL.createObjectURL(blob);
        const a = document.createElement('a');
        a.href = url;
        // Always use webm extension for better compatibility
        const extension = mimeType.startsWith('video/webm') ? 'webm' : 'mp4';
        a.download = `${video.name.split('.')[0]}-trimmed.${extension}`;
        a.click();
        URL.revokeObjectURL(url);
        setTrimming(false);
        setProgress(100);
      };

      // Start recording
      mediaRecorder.start(1000);

      // Set video to start time and start capturing
      videoElement.currentTime = startSeconds;
      videoElement.onseeked = function seeked() {
        // Remove the event listener after it's done
        videoElement.onseeked = null;

        const drawFrame = () => {
          if (
            !videoElement ||
            !ctx ||
            !mediaRecorder ||
            videoElement.currentTime >= endSeconds
          ) {
            mediaRecorder.stop();
            return;
          }

          ctx.drawImage(videoElement, 0, 0, canvas.width, canvas.height);
          const progress =
            ((videoElement.currentTime - startSeconds) /
              (endSeconds - startSeconds)) *
            100;
          setProgress(Math.min(progress, 100));

          if (videoElement.currentTime < endSeconds) {
            requestAnimationFrame(drawFrame);
          }
        };

        videoElement.play();
        drawFrame();
      };
    } catch (error: any) {
      console.error('Error during trimming:', error);
      setError(error.message || 'Error trimming video. Please try again.');
      setTrimming(false);
      if (
        mediaRecorderRef.current &&
        mediaRecorderRef.current.state !== 'inactive'
      ) {
        mediaRecorderRef.current.stop();
      }
    }
  };

  return (
    <FeaturePageTemplate
      title="Trim Video"
      description="Cut and trim your videos with precision"
    >
      <Box className="w-full max-w-4xl mx-auto p-4 md:p-6 space-y-4 md:space-y-6">
        {error && (
          <Alert severity="error" onClose={() => setError('')}>
            {error}
          </Alert>
        )}

        <div
          {...getRootProps()}
          className={`border-2 border-dashed rounded-lg p-4 md:p-8 text-center cursor-pointer transition-colors
            ${
              isDragActive
                ? 'border-blue-500 bg-blue-50'
                : 'border-gray-300 hover:border-blue-400'
            }`}
        >
          <input {...getInputProps()} />
          <Typography variant="h6" className="mb-2">
            {isDragActive
              ? 'Drop the video here'
              : 'Drag & drop a video file here, or click to select'}
          </Typography>
          <Typography variant="body2" color="textSecondary">
            Supports various video formats (MP4, WebM, MOV, AVI)
          </Typography>
        </div>

        {previewUrl && (
          <Box className="space-y-4">
            <Box className="aspect-video w-full bg-black rounded-lg overflow-hidden">
              <video
                ref={videoRef}
                src={previewUrl}
                onTimeUpdate={handleTimeUpdate}
                className="w-full h-full object-contain"
                controls
              />
              <canvas ref={canvasRef} style={{ display: 'none' }} />
            </Box>

            <Typography variant="body2" className="text-center">
              Current Time: {currentTime}
            </Typography>
          </Box>
        )}

        {video && (
          <Box className="space-y-4 bg-gray-50 p-4 md:p-6 rounded-lg">
            <Typography variant="h6" className="mb-4">
              Trim Settings
            </Typography>

            <Grid container spacing={2}>
              <Grid item xs={12} sm={4}>
                <FormControl fullWidth>
                  <InputLabel>Output Format</InputLabel>
                  <Select
                    value={settings.format}
                    onChange={(e) =>
                      handleSettingChange('format', e.target.value)
                    }
                    label="Output Format"
                  >
                    {formats.map((format) => (
                      <MenuItem key={format.value} value={format.value}>
                        {format.label}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={12} sm={4}>
                <TextField
                  fullWidth
                  label="Start Time (MM:SS)"
                  value={settings.startTime}
                  onChange={(e) =>
                    handleSettingChange('startTime', e.target.value)
                  }
                  placeholder="00:00"
                  InputProps={{
                    endAdornment: (
                      <Button size="small" onClick={setCurrentAsStart}>
                        Set Current
                      </Button>
                    ),
                  }}
                />
              </Grid>
              <Grid item xs={12} sm={4}>
                <TextField
                  fullWidth
                  label="End Time (MM:SS)"
                  value={settings.endTime}
                  onChange={(e) =>
                    handleSettingChange('endTime', e.target.value)
                  }
                  placeholder="00:00"
                  InputProps={{
                    endAdornment: (
                      <Button size="small" onClick={setCurrentAsEnd}>
                        Set Current
                      </Button>
                    ),
                  }}
                />
              </Grid>
            </Grid>

            <Button
              variant="contained"
              color="primary"
              onClick={trimVideo}
              disabled={trimming}
              fullWidth
              className="mt-4"
            >
              {trimming ? (
                <Box className="flex items-center justify-center">
                  <CircularProgress size={24} className="mr-2" />
                  <span>Trimming... {Math.round(progress)}%</span>
                </Box>
              ) : (
                'Trim Video'
              )}
            </Button>
          </Box>
        )}
      </Box>
    </FeaturePageTemplate>
  );
};

export default TrimVideo;

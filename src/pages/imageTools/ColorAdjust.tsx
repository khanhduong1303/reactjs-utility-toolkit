import React, { useState } from 'react';
import { Typography, Button, Box, Alert, Slider } from '@mui/material';
import { CloudUpload as UploadIcon } from '@mui/icons-material';
import FeaturePageTemplate from '@components/FeaturePageTemplate';
import Notification from '@components/Notification';

const ColorAdjust = () => {
  const [selectedFile, setSelectedFile] = useState<File | null>(null);
  const [error, setError] = useState('');
  const [processing, setProcessing] = useState(false);
  const [brightness, setBrightness] = useState(100);
  const [contrast, setContrast] = useState(100);
  const [saturation, setSaturation] = useState(100);
  const [hue, setHue] = useState(0);
  const [notification, setNotification] = useState({
    open: false,
    message: '',
  });

  const handleFileUpload = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0];
    if (file) {
      if (file.type.startsWith('image/')) {
        setSelectedFile(file);
        setError('');
      } else {
        setError('Please upload a valid image file');
      }
    }
  };

  const handleApply = async () => {
    setProcessing(true);
    await new Promise((resolve) => setTimeout(resolve, 2000));
    setProcessing(false);
    setNotification({
      open: true,
      message:
        'This is a demo version. Color adjustment is not implemented yet.',
    });
  };

  const handleReset = () => {
    setBrightness(100);
    setContrast(100);
    setSaturation(100);
    setHue(0);
  };

  return (
    <FeaturePageTemplate
      title="Color Adjust"
      description="Adjust brightness, contrast, saturation, and hue of your images"
    >
      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <Box className="space-y-6">
        <input
          type="file"
          accept="image/*"
          onChange={handleFileUpload}
          className="hidden"
          id="image-upload"
        />
        <label htmlFor="image-upload">
          <Button
            variant="outlined"
            component="span"
            startIcon={<UploadIcon />}
            fullWidth
          >
            Upload Image
          </Button>
        </label>

        {selectedFile && (
          <>
            <Typography variant="body2">
              Selected file: {selectedFile.name}
            </Typography>

            <Box>
              <Typography gutterBottom>Preview</Typography>
              <Box
                sx={{
                  width: '100%',
                  height: 300,
                  bgcolor: 'grey.100',
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  filter: `brightness(${brightness}%) contrast(${contrast}%) saturate(${saturation}%) hue-rotate(${hue}deg)`,
                }}
              >
                <Typography color="text.secondary">
                  Preview will be shown here
                </Typography>
              </Box>
            </Box>

            <Box>
              <Typography gutterBottom>Brightness</Typography>
              <Slider
                value={brightness}
                onChange={(_, value) => setBrightness(value as number)}
                valueLabelDisplay="auto"
                min={0}
                max={200}
                marks={[
                  { value: 0, label: '0%' },
                  { value: 100, label: '100%' },
                  { value: 200, label: '200%' },
                ]}
              />
            </Box>

            <Box>
              <Typography gutterBottom>Contrast</Typography>
              <Slider
                value={contrast}
                onChange={(_, value) => setContrast(value as number)}
                valueLabelDisplay="auto"
                min={0}
                max={200}
                marks={[
                  { value: 0, label: '0%' },
                  { value: 100, label: '100%' },
                  { value: 200, label: '200%' },
                ]}
              />
            </Box>

            <Box>
              <Typography gutterBottom>Saturation</Typography>
              <Slider
                value={saturation}
                onChange={(_, value) => setSaturation(value as number)}
                valueLabelDisplay="auto"
                min={0}
                max={200}
                marks={[
                  { value: 0, label: '0%' },
                  { value: 100, label: '100%' },
                  { value: 200, label: '200%' },
                ]}
              />
            </Box>

            <Box>
              <Typography gutterBottom>Hue Rotation</Typography>
              <Slider
                value={hue}
                onChange={(_, value) => setHue(value as number)}
                valueLabelDisplay="auto"
                min={0}
                max={360}
                marks={[
                  { value: 0, label: '0°' },
                  { value: 180, label: '180°' },
                  { value: 360, label: '360°' },
                ]}
              />
            </Box>

            <Box className="space-x-4">
              <Button
                variant="outlined"
                onClick={handleReset}
                disabled={processing}
              >
                Reset
              </Button>
              <Button
                variant="contained"
                onClick={handleApply}
                disabled={processing}
              >
                {processing ? 'Processing Image...' : 'Apply Changes'}
              </Button>
            </Box>
          </>
        )}
      </Box>

      <Notification
        open={notification.open}
        message={notification.message}
        onClose={() => setNotification({ ...notification, open: false })}
      />
    </FeaturePageTemplate>
  );
};

export default ColorAdjust;

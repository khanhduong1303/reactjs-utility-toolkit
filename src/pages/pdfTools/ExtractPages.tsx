import React, { useState } from 'react';
import { Typography, Button, Box, Alert, TextField } from '@mui/material';
import { CloudUpload as UploadIcon } from '@mui/icons-material';
import { PDFDocument } from 'pdf-lib';
import FeaturePageTemplate from '@components/FeaturePageTemplate';
import Notification from '@components/Notification';

const ExtractPages = () => {
  const [selectedFile, setSelectedFile] = useState<File | null>(null);
  const [error, setError] = useState('');
  const [pageRange, setPageRange] = useState('');
  const [extracting, setExtracting] = useState(false);
  const [totalPages, setTotalPages] = useState(0);
  const [notification, setNotification] = useState({
    open: false,
    message: '',
  });

  const handleFileUpload = async (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const file = event.target.files?.[0];
    if (file) {
      if (file.type === 'application/pdf') {
        try {
          const fileData = await file.arrayBuffer();
          const pdfDoc = await PDFDocument.load(fileData);
          setTotalPages(pdfDoc.getPageCount());
          setSelectedFile(file);
          setError('');
        } catch (err) {
          setError('Failed to load PDF. Please try again.');
          console.error('Loading error:', err);
        }
      } else {
        setError('Please upload a valid PDF file');
      }
    }
  };

  const validatePageRange = (range: string): number[] | null => {
    const pages: number[] = [];
    const ranges = range.split(',').map((r) => r.trim());

    for (const r of ranges) {
      if (r.includes('-')) {
        const [start, end] = r.split('-').map(Number);
        if (
          isNaN(start) ||
          isNaN(end) ||
          start < 1 ||
          end > totalPages ||
          start > end
        ) {
          return null;
        }
        for (let i = start; i <= end; i++) {
          pages.push(i - 1);
        }
      } else {
        const page = Number(r);
        if (isNaN(page) || page < 1 || page > totalPages) {
          return null;
        }
        pages.push(page - 1);
      }
    }

    return pages;
  };

  const extractPages = async () => {
    if (!selectedFile || !pageRange) return;

    const pages = validatePageRange(pageRange);
    if (!pages) {
      setError('Invalid page range. Please check your input.');
      return;
    }

    try {
      setExtracting(true);
      setError('');

      const fileData = await selectedFile.arrayBuffer();
      const pdfDoc = await PDFDocument.load(fileData);
      const newPdfDoc = await PDFDocument.create();

      for (const pageIndex of pages) {
        const [copiedPage] = await newPdfDoc.copyPages(pdfDoc, [pageIndex]);
        newPdfDoc.addPage(copiedPage);
      }

      const pdfBytes = await newPdfDoc.save();
      const blob = new Blob([pdfBytes], { type: 'application/pdf' });
      const url = URL.createObjectURL(blob);
      const link = document.createElement('a');
      link.href = url;
      link.download = `extracted_${selectedFile.name}`;
      link.click();
      URL.revokeObjectURL(url);
    } catch (err) {
      setError('Failed to extract pages. Please try again.');
      console.error('Extraction error:', err);
    } finally {
      setExtracting(false);
    }
  };

  return (
    <FeaturePageTemplate
      title="Extract Pages"
      description="Extract specific pages from your PDF document"
    >
      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <Box className="space-y-6">
        <input
          type="file"
          accept=".pdf"
          onChange={handleFileUpload}
          className="hidden"
          id="pdf-upload"
        />
        <label htmlFor="pdf-upload">
          <Button
            variant="outlined"
            component="span"
            startIcon={<UploadIcon />}
            fullWidth
          >
            Upload PDF
          </Button>
        </label>

        {selectedFile && (
          <>
            <Typography variant="body2">Total pages: {totalPages}</Typography>

            <TextField
              fullWidth
              label="Page Range"
              placeholder="e.g., 1-3, 5, 7-9"
              value={pageRange}
              onChange={(e) => setPageRange(e.target.value)}
              helperText="Enter page numbers or ranges separated by commas"
            />

            <Button
              variant="contained"
              onClick={extractPages}
              fullWidth
              disabled={extracting || !pageRange}
            >
              {extracting ? 'Extracting...' : 'Extract Pages'}
            </Button>
          </>
        )}
      </Box>

      <Notification
        open={notification.open}
        message={notification.message}
        onClose={() => setNotification({ ...notification, open: false })}
      />
    </FeaturePageTemplate>
  );
};

export default ExtractPages;

// App.js
import { Avatar } from '@mui/material';
import React from 'react';

interface IProps {
  avatarProps?: any;
  size?: number;
  backgroundColor?: string;
  frameColor?: string;
  className?: string;
}

const AvatarRotated = ({
  avatarProps = {},
  size = 150,
  backgroundColor = '#000',
  frameColor = '#FF4444',
  className = '',
}: IProps) => {
  return (
    <div
      className={`relative ${className}`}
      style={{ width: size, height: size }}
    >
      <div className="absolute w-full h-full overflow-hidden rounded-lg shadow-lg transition-transform duration-300 hover:scale-105">
        <Avatar
          {...avatarProps}
          className="w-full h-full object-cover"
          style={{ width: size, height: size }}
        />
      </div>
      <div
        className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 rotate-45 border-[3px] rounded"
        style={{
          width: size * 0.7,
          height: size * 0.7,
          borderColor: frameColor,
          backgroundColor: 'rgba(255, 255, 255, 0.1)',
          backdropFilter: 'blur(4px)',
        }}
      />
      <div
        className="absolute inset-0 opacity-20 mix-blend-overlay"
        style={{ backgroundColor }}
      />
    </div>
  );
};

export default AvatarRotated;

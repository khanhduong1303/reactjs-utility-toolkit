import React from 'react';
import { Typography, Grid, Paper } from '@mui/material';
import { Link } from 'react-router-dom';
import {
  Compress,
  FileCopy,
  Delete,
  Merge,
  Transform,
} from '@mui/icons-material';
import FeaturePageTemplate from '@components/FeaturePageTemplate';

interface Tool {
  title: string;
  description: string;
  icon: React.ReactNode;
  path: string;
}

const tools: Tool[] = [
  {
    title: 'Compress PDF',
    description: 'Reduce PDF file size while maintaining quality',
    icon: <Compress className="text-4xl" />,
    path: '/pdf-tools/compress',
  },
  {
    title: 'Split PDF',
    description: 'Split PDF into multiple files',
    icon: <FileCopy className="text-4xl" />,
    path: '/pdf-tools/split',
  },
  {
    title: 'Remove Pages',
    description: 'Delete specific pages from PDF',
    icon: <Delete className="text-4xl" />,
    path: '/pdf-tools/remove-pages',
  },
  {
    title: 'Merge PDFs',
    description: 'Combine multiple PDF files into one',
    icon: <Merge className="text-4xl" />,
    path: '/pdf-tools/merge',
  },
  {
    title: 'Convert PDF',
    description: 'Convert PDF to other formats',
    icon: <Transform className="text-4xl" />,
    path: '/pdf-tools/convert',
  },
];

const PDFTools = () => {
  return (
    <FeaturePageTemplate
      title="PDF Tools"
      description="Free online tools for PDF processing and conversion"
    >
      <Grid container spacing={4}>
        {tools.map((tool) => (
          <Grid item xs={12} sm={6} md={4} key={tool.path}>
            <Link to={tool.path} className="no-underline">
              <Paper
                elevation={1}
                className="p-6 h-full transition-all hover:shadow-md"
              >
                <div className="flex flex-col items-center text-center space-y-4">
                  {tool.icon}
                  <Typography variant="h6" className="font-bold">
                    {tool.title}
                  </Typography>
                  <Typography className="text-secondary-600">
                    {tool.description}
                  </Typography>
                </div>
              </Paper>
            </Link>
          </Grid>
        ))}
      </Grid>
    </FeaturePageTemplate>
  );
};

export default PDFTools;

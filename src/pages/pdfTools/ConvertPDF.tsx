import React, { useState } from 'react';
import {
  Typography,
  Button,
  Box,
  Alert,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  SelectChangeEvent,
} from '@mui/material';
import { CloudUpload as UploadIcon } from '@mui/icons-material';
import { PDFDocument } from 'pdf-lib';
import FeaturePageTemplate from '@components/FeaturePageTemplate';
import Notification from '@components/Notification';

const ConvertPDF = () => {
  const [selectedFile, setSelectedFile] = useState<File | null>(null);
  const [error, setError] = useState('');
  const [processing, setProcessing] = useState(false);
  const [format, setFormat] = useState('png');
  const [notification, setNotification] = useState({
    open: false,
    message: '',
  });

  const handleFileUpload = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0];
    if (file) {
      if (file.type === 'application/pdf') {
        setSelectedFile(file);
        setError('');
      } else {
        setError('Please upload a valid PDF file');
      }
    }
  };

  const handleFormatChange = (event: SelectChangeEvent) => {
    setFormat(event.target.value);
  };

  const handleConvert = async () => {
    if (!selectedFile) return;

    try {
      setProcessing(true);
      setError('');

      const fileData = await selectedFile.arrayBuffer();
      const pdfDoc = await PDFDocument.load(fileData);
      const pages = pdfDoc.getPages();

      // For PNG/JPEG conversion
      if (format === 'png' || format === 'jpeg') {
        for (let i = 0; i < pages.length; i++) {
          // Create a new PDF with just this page
          const singlePagePdf = await PDFDocument.create();
          const [copiedPage] = await singlePagePdf.copyPages(pdfDoc, [i]);
          singlePagePdf.addPage(copiedPage);

          // Convert to image format
          const pngBytes = await singlePagePdf.saveAsBase64({ dataUri: true });

          // Create download link
          const link = document.createElement('a');
          link.href = pngBytes;
          link.download = `page_${i + 1}.${format}`;
          link.click();
        }
      }

      setNotification({
        open: true,
        message: 'PDF converted successfully!',
      });
    } catch (err) {
      console.error('Conversion error:', err);
      setError('Failed to convert PDF. Please try again.');
    } finally {
      setProcessing(false);
    }
  };

  return (
    <FeaturePageTemplate
      title="Convert PDF"
      description="Convert your PDF files to other formats"
    >
      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <Box className="space-y-6">
        <input
          type="file"
          accept=".pdf"
          onChange={handleFileUpload}
          style={{ display: 'none' }}
          id="pdf-upload"
        />
        <label htmlFor="pdf-upload">
          <Button
            variant="outlined"
            component="span"
            startIcon={<UploadIcon />}
            fullWidth
          >
            Upload PDF
          </Button>
        </label>

        {selectedFile && (
          <>
            <Typography variant="body2">
              Selected file: {selectedFile.name}
            </Typography>

            <FormControl fullWidth>
              <InputLabel>Convert to</InputLabel>
              <Select
                value={format}
                label="Convert to"
                onChange={handleFormatChange}
              >
                <MenuItem value="png">PNG</MenuItem>
                <MenuItem value="jpeg">JPEG</MenuItem>
              </Select>
            </FormControl>

            <Button
              variant="contained"
              onClick={handleConvert}
              disabled={processing}
              fullWidth
            >
              {processing ? 'Converting...' : 'Convert PDF'}
            </Button>
          </>
        )}
      </Box>

      <Notification
        open={notification.open}
        message={notification.message}
        onClose={() => setNotification({ ...notification, open: false })}
      />
    </FeaturePageTemplate>
  );
};

export default ConvertPDF;

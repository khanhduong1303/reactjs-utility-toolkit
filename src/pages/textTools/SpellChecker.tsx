import React, { useState } from 'react';
import { Button, Box, Alert, TextField } from '@mui/material';
import FeaturePageTemplate from '@components/FeaturePageTemplate';
import Notification from '@components/Notification';

const SpellChecker = () => {
  const [text, setText] = useState('');
  const [error, setError] = useState('');
  const [processing, setProcessing] = useState(false);
  const [notification, setNotification] = useState({
    open: false,
    message: '',
  });

  const handleCheck = async () => {
    if (!text.trim()) {
      setError('Please enter some text to check');
      return;
    }

    setProcessing(true);
    await new Promise((resolve) => setTimeout(resolve, 2000));
    setProcessing(false);
    setNotification({
      open: true,
      message: 'This is a demo version. Spell checking is not implemented yet.',
    });
  };

  return (
    <FeaturePageTemplate
      title="Spell Checker"
      description="Check spelling and grammar in your text"
    >
      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <Box className="space-y-6">
        <TextField
          fullWidth
          multiline
          rows={8}
          label="Enter text to check"
          value={text}
          onChange={(e) => setText(e.target.value)}
        />

        <Button
          variant="contained"
          onClick={handleCheck}
          disabled={processing}
          fullWidth
        >
          {processing ? 'Checking...' : 'Check Spelling'}
        </Button>
      </Box>

      <Notification
        open={notification.open}
        message={notification.message}
        onClose={() => setNotification({ ...notification, open: false })}
      />
    </FeaturePageTemplate>
  );
};

export default SpellChecker;

import React, { useState, useRef, useEffect, useCallback } from 'react';
import { Typography, Grid, Paper, Button, Slider, Alert } from '@mui/material';
import { CloudUpload as UploadIcon } from '@mui/icons-material';
import FeaturePageTemplate from '@components/FeaturePageTemplate';

interface Effect {
  name: string;
  label: string;
  min: number;
  max: number;
  default: number;
  step: number;
  unit?: string;
}

const EFFECTS: Effect[] = [
  {
    name: 'brightness',
    label: 'Brightness',
    min: 0,
    max: 200,
    default: 100,
    step: 1,
    unit: '%',
  },
  {
    name: 'contrast',
    label: 'Contrast',
    min: 0,
    max: 200,
    default: 100,
    step: 1,
    unit: '%',
  },
  {
    name: 'saturation',
    label: 'Saturation',
    min: 0,
    max: 200,
    default: 100,
    step: 1,
    unit: '%',
  },
  {
    name: 'blur',
    label: 'Blur',
    min: 0,
    max: 20,
    default: 0,
    step: 0.5,
    unit: 'px',
  },
  {
    name: 'hueRotate',
    label: 'Hue Rotate',
    min: 0,
    max: 360,
    default: 0,
    step: 1,
    unit: 'deg',
  },
  {
    name: 'sepia',
    label: 'Sepia',
    min: 0,
    max: 100,
    default: 0,
    step: 1,
    unit: '%',
  },
  {
    name: 'grayscale',
    label: 'Grayscale',
    min: 0,
    max: 100,
    default: 0,
    step: 1,
    unit: '%',
  },
  {
    name: 'invert',
    label: 'Invert',
    min: 0,
    max: 100,
    default: 0,
    step: 1,
    unit: '%',
  },
  {
    name: 'opacity',
    label: 'Opacity',
    min: 0,
    max: 100,
    default: 100,
    step: 1,
    unit: '%',
  },
];

const ImageEffects = () => {
  const [selectedImage, setSelectedImage] = useState<File | null>(null);
  const [error, setError] = useState('');
  const [effects, setEffects] = useState<Record<string, number>>({});
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const imageRef = useRef<HTMLImageElement | null>(null);
  const [canvasSize, setCanvasSize] = useState({ width: 0, height: 0 });
  const [originalImage, setOriginalImage] = useState<HTMLImageElement | null>(
    null
  );

  useEffect(() => {
    // Initialize effects with default values
    const defaultEffects = EFFECTS.reduce((acc, effect) => {
      acc[effect.name] = effect.default;
      return acc;
    }, {} as Record<string, number>);
    setEffects(defaultEffects);
  }, []);

  const handleImageUpload = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0];
    if (file) {
      if (file.type.startsWith('image/')) {
        const reader = new FileReader();
        reader.onload = (e) => {
          const img = new Image();
          img.onload = () => {
            imageRef.current = img;
            applyEffects();
          };
          img.src = e.target?.result as string;
          setSelectedImage(file);
          setError('');
        };
        reader.readAsDataURL(file);
      } else {
        setError('Please upload a valid image file');
      }
    }
  };

  const applyEffects = () => {
    const canvas = canvasRef.current;
    const img = imageRef.current;
    if (!canvas || !img) return;

    // Set canvas size based on viewport width for mobile
    const maxWidth =
      window.innerWidth < 768
        ? window.innerWidth - 48
        : Math.min(img.width, window.innerWidth - 48);
    const scale = maxWidth / img.width;
    const scaledHeight = img.height * scale;

    canvas.width = maxWidth;
    canvas.height = scaledHeight;
    const ctx = canvas.getContext('2d');
    if (!ctx) return;

    ctx.filter = [
      `brightness(${effects.brightness}%)`,
      `contrast(${effects.contrast}%)`,
      `saturate(${effects.saturation}%)`,
      `blur(${effects.blur}px)`,
      `hue-rotate(${effects.hueRotate}deg)`,
      `sepia(${effects.sepia}%)`,
      `grayscale(${effects.grayscale}%)`,
      `invert(${effects.invert}%)`,
      `opacity(${effects.opacity}%)`,
    ].join(' ');

    ctx.drawImage(img, 0, 0, maxWidth, scaledHeight);
  };

  const generateFilter = useCallback(() => {
    return `
      brightness(${effects.brightness}%)
      contrast(${effects.contrast}%)
      saturate(${effects.saturation}%)
      blur(${effects.blur}px)
      hue-rotate(${effects.hueRotate}deg)
      sepia(${effects.sepia}%)
      grayscale(${effects.grayscale}%)
      invert(${effects.invert}%)
      opacity(${effects.opacity}%)
    `;
  }, [effects]);

  useEffect(() => {
    if (selectedImage) {
      const img = new Image();
      const reader = new FileReader();
      reader.onload = (e) => {
        if (e.target?.result) {
          img.src = e.target.result as string;
          img.onload = () => {
            const maxWidth = Math.min(img.width, window.innerWidth - 48);
            const scale = maxWidth / img.width;
            const height = img.height * scale;
            setCanvasSize({ width: maxWidth, height });
            setOriginalImage(img);
            if (canvasRef.current) {
              const ctx = canvasRef.current.getContext('2d');
              if (ctx) {
                canvasRef.current.width = maxWidth;
                canvasRef.current.height = height;
                ctx.filter = generateFilter();
                ctx.drawImage(img, 0, 0, maxWidth, height);
              }
            }
          };
        }
      };
      reader.readAsDataURL(selectedImage);
    }
  }, [selectedImage, generateFilter]);

  useEffect(() => {
    if (originalImage && canvasSize && canvasRef.current) {
      const ctx = canvasRef.current.getContext('2d');
      if (ctx) {
        canvasRef.current.width = canvasSize.width;
        canvasRef.current.height = canvasSize.height;
        ctx.filter = generateFilter();
        ctx.drawImage(originalImage, 0, 0, canvasSize.width, canvasSize.height);
      }
    }
  }, [effects, originalImage, canvasSize, generateFilter]);

  const handleEffectChange =
    (effectName: string) => (_: Event, value: number | number[]) => {
      const newValue = Array.isArray(value) ? value[0] : value;
      setEffects((prev) => ({ ...prev, [effectName]: newValue }));
      applyEffects();
    };

  const downloadImage = () => {
    const canvas = canvasRef.current;
    if (!canvas) return;

    const link = document.createElement('a');
    link.download = 'edited_image.png';
    link.href = canvas.toDataURL('image/png');
    link.click();
  };

  const resetEffects = () => {
    const defaultEffects = EFFECTS.reduce((acc, effect) => {
      acc[effect.name] = effect.default;
      return acc;
    }, {} as Record<string, number>);
    setEffects(defaultEffects);
    applyEffects();
  };

  return (
    <FeaturePageTemplate
      title="Image Effects"
      description="Apply various effects to your image"
    >
      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <Grid container spacing={4}>
        <Grid item xs={12} md={8}>
          <Paper elevation={1} className="p-4 md:p-6">
            {!selectedImage ? (
              <div className="text-center">
                <input
                  type="file"
                  accept="image/*"
                  onChange={handleImageUpload}
                  className="hidden"
                  id="image-upload"
                />
                <label htmlFor="image-upload">
                  <Button
                    variant="outlined"
                    component="span"
                    startIcon={<UploadIcon />}
                    fullWidth
                  >
                    Upload Image
                  </Button>
                </label>
              </div>
            ) : (
              <div className="space-y-4">
                <div className="relative overflow-auto">
                  <canvas
                    ref={canvasRef}
                    className="max-w-full h-auto mx-auto"
                    style={{ touchAction: 'none' }}
                  />
                </div>
                <div className="flex flex-wrap gap-2 justify-center">
                  <Button variant="outlined" onClick={resetEffects}>
                    Reset Effects
                  </Button>
                  <Button variant="contained" onClick={downloadImage}>
                    Download Image
                  </Button>
                </div>
              </div>
            )}
          </Paper>
        </Grid>

        <Grid item xs={12} md={4}>
          <Paper elevation={1} className="p-4 md:p-6 sticky top-4">
            <Typography variant="h6" className="mb-4">
              Effects
            </Typography>
            <div className="space-y-4 md:space-y-6">
              {EFFECTS.map((effect) => (
                <div key={effect.name}>
                  <Typography
                    gutterBottom
                    className="flex justify-between items-center"
                  >
                    <span>{effect.label}</span>
                    <span className="text-primary-600">
                      {effects[effect.name]}
                      {effect.unit}
                    </span>
                  </Typography>
                  <Slider
                    value={effects[effect.name] ?? effect.default}
                    onChange={handleEffectChange(effect.name)}
                    min={effect.min}
                    max={effect.max}
                    step={effect.step}
                    valueLabelDisplay="auto"
                    disabled={!selectedImage}
                    sx={{
                      '& .MuiSlider-thumb': {
                        width: 16,
                        height: 16,
                      },
                      '& .MuiSlider-track': {
                        height: 4,
                      },
                      '& .MuiSlider-rail': {
                        height: 4,
                      },
                    }}
                  />
                </div>
              ))}
            </div>
          </Paper>
        </Grid>
      </Grid>
    </FeaturePageTemplate>
  );
};

export default ImageEffects;

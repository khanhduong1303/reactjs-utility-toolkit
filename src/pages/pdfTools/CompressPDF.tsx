import React, { useState } from 'react';
import { Typography, Button, Box, Alert } from '@mui/material';
import { CloudUpload as UploadIcon } from '@mui/icons-material';
import { PDFDocument } from 'pdf-lib';
import FeaturePageTemplate from '@components/FeaturePageTemplate';
import Notification from '@components/Notification';

const CompressPDF = () => {
  const [selectedFile, setSelectedFile] = useState<File | null>(null);
  const [error, setError] = useState('');
  const [processing, setProcessing] = useState(false);
  const [notification, setNotification] = useState({
    open: false,
    message: '',
  });

  const handleFileUpload = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0];
    if (file) {
      if (file.type === 'application/pdf') {
        setSelectedFile(file);
        setError('');
      } else {
        setError('Please upload a valid PDF file');
      }
    }
  };

  const handleCompress = async () => {
    if (!selectedFile) return;

    try {
      setProcessing(true);
      setError('');

      const fileData = await selectedFile.arrayBuffer();
      const pdfDoc = await PDFDocument.load(fileData);

      // Compress by removing metadata and optimizing content
      pdfDoc.setTitle('');
      pdfDoc.setAuthor('');
      pdfDoc.setSubject('');
      pdfDoc.setKeywords([]);
      pdfDoc.setProducer('');
      pdfDoc.setCreator('');

      const compressedBytes = await pdfDoc.save({
        useObjectStreams: true,
        addDefaultPage: false,
        objectsPerTick: 50,
      });

      const blob = new Blob([compressedBytes], { type: 'application/pdf' });
      const url = URL.createObjectURL(blob);
      const link = document.createElement('a');
      link.href = url;
      link.download = `compressed_${selectedFile.name}`;
      link.click();
      URL.revokeObjectURL(url);

      setNotification({
        open: true,
        message: 'PDF compressed successfully!',
      });
    } catch (err) {
      console.error('Compression error:', err);
      setError('Failed to compress PDF. Please try again.');
    } finally {
      setProcessing(false);
    }
  };

  return (
    <FeaturePageTemplate
      title="Compress PDF"
      description="Reduce PDF file size while maintaining quality"
    >
      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <Box className="space-y-6">
        <input
          type="file"
          accept=".pdf"
          onChange={handleFileUpload}
          style={{ display: 'none' }}
          id="pdf-upload"
        />
        <label htmlFor="pdf-upload">
          <Button
            variant="outlined"
            component="span"
            startIcon={<UploadIcon />}
            fullWidth
          >
            Upload PDF
          </Button>
        </label>

        {selectedFile && (
          <>
            <Typography variant="body2">
              Selected file: {selectedFile.name}
            </Typography>

            <Button
              variant="contained"
              onClick={handleCompress}
              disabled={processing}
              fullWidth
            >
              {processing ? 'Compressing...' : 'Compress PDF'}
            </Button>
          </>
        )}
      </Box>

      <Notification
        open={notification.open}
        message={notification.message}
        onClose={() => setNotification({ ...notification, open: false })}
      />
    </FeaturePageTemplate>
  );
};

export default CompressPDF;

import { makeAutoObservable } from 'mobx';
import { UserStore } from './UserStore';
import { SettingStore } from './SettingStore';

export class RootStore {
  userStore: UserStore;
  settingStore: SettingStore;

  constructor() {
    makeAutoObservable(this);
    this.userStore = new UserStore(this);
    this.settingStore = new SettingStore(this);
  }
}

const rootStore = new RootStore();
export default rootStore;

import React, { useState, useEffect } from 'react';
import {
  Typography,
  Grid,
  Paper,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from '@mui/material';
import FeaturePageTemplate from '@components/FeaturePageTemplate';

interface Unit {
  value: string;
  label: string;
  toBase: number; // conversion to kilograms
}

const WEIGHT_UNITS: Unit[] = [
  { value: 'kg', label: 'Kilograms (kg)', toBase: 1 },
  { value: 'g', label: 'Grams (g)', toBase: 0.001 },
  { value: 'mg', label: 'Milligrams (mg)', toBase: 0.000001 },
  { value: 'lb', label: 'Pounds (lb)', toBase: 0.45359237 },
  { value: 'oz', label: 'Ounces (oz)', toBase: 0.028349523125 },
  { value: 't', label: 'Metric Tons (t)', toBase: 1000 },
  { value: 'st', label: 'Stone (st)', toBase: 6.35029318 },
];

const WeightConverter = () => {
  const [inputValue, setInputValue] = useState<string>('');
  const [inputUnit, setInputUnit] = useState<string>('kg');
  const [outputUnit, setOutputUnit] = useState<string>('lb');
  const [outputValue, setOutputValue] = useState<string>('');

  const convert = (value: string, from: string, to: string) => {
    const numValue = parseFloat(value);
    if (isNaN(numValue)) return '';

    const fromUnit = WEIGHT_UNITS.find((u) => u.value === from);
    const toUnit = WEIGHT_UNITS.find((u) => u.value === to);
    if (!fromUnit || !toUnit) return '';

    // Convert to base unit (kilograms) then to target unit
    const baseValue = numValue * fromUnit.toBase;
    const result = baseValue / toUnit.toBase;
    return result.toFixed(6);
  };

  useEffect(() => {
    const result = convert(inputValue, inputUnit, outputUnit);
    setOutputValue(result);
  }, [inputValue, inputUnit, outputUnit]);

  return (
    <FeaturePageTemplate
      title="Weight Converter"
      description="Convert between different units of weight"
    >
      <Grid container spacing={4} justifyContent="center">
        <Grid item xs={12} md={8}>
          <Paper elevation={1} className="p-6">
            <Grid container spacing={3}>
              <Grid item xs={12} md={6}>
                <FormControl fullWidth>
                  <InputLabel>From</InputLabel>
                  <Select
                    value={inputUnit}
                    label="From"
                    onChange={(e) => setInputUnit(e.target.value)}
                  >
                    {WEIGHT_UNITS.map((unit) => (
                      <MenuItem key={unit.value} value={unit.value}>
                        {unit.label}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>

              <Grid item xs={12} md={6}>
                <FormControl fullWidth>
                  <InputLabel>To</InputLabel>
                  <Select
                    value={outputUnit}
                    label="To"
                    onChange={(e) => setOutputUnit(e.target.value)}
                  >
                    {WEIGHT_UNITS.map((unit) => (
                      <MenuItem key={unit.value} value={unit.value}>
                        {unit.label}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>

              <Grid item xs={12} md={6}>
                <TextField
                  fullWidth
                  label="Enter Value"
                  type="number"
                  value={inputValue}
                  onChange={(e) => setInputValue(e.target.value)}
                  placeholder="Enter a number"
                />
              </Grid>

              <Grid item xs={12} md={6}>
                <TextField
                  fullWidth
                  label="Result"
                  value={outputValue}
                  InputProps={{ readOnly: true }}
                />
              </Grid>
            </Grid>

            <Typography className="mt-6 text-sm text-gray-600">
              Note: Results are rounded to 6 decimal places for precision.
            </Typography>
          </Paper>
        </Grid>
      </Grid>
    </FeaturePageTemplate>
  );
};

export default WeightConverter;

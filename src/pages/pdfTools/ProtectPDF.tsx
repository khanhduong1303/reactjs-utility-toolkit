import React, { useState } from 'react';
import {
  Typography,
  Button,
  Box,
  Alert,
  TextField,
  FormControlLabel,
  Checkbox,
} from '@mui/material';
import { CloudUpload as UploadIcon } from '@mui/icons-material';
import { PDFDocument } from 'pdf-lib';
import FeaturePageTemplate from '@components/FeaturePageTemplate';
import Notification from '@components/Notification';

const ProtectPDF = () => {
  const [selectedFile, setSelectedFile] = useState<File | null>(null);
  const [error, setError] = useState('');
  const [processing, setProcessing] = useState(false);
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [preventPrinting, setPreventPrinting] = useState(false);
  const [preventModification, setPreventModification] = useState(true);
  const [notification, setNotification] = useState({
    open: false,
    message: '',
  });

  const handleFileUpload = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0];
    if (file) {
      if (file.type === 'application/pdf') {
        setSelectedFile(file);
        setError('');
      } else {
        setError('Please upload a valid PDF file');
      }
    }
  };

  const validatePasswords = () => {
    if (password.length < 6) {
      setError('Password must be at least 6 characters long');
      return false;
    }
    if (password !== confirmPassword) {
      setError('Passwords do not match');
      return false;
    }
    return true;
  };

  const handleProtect = async () => {
    if (!selectedFile || !validatePasswords()) return;

    try {
      setProcessing(true);
      setError('');

      const fileData = await selectedFile.arrayBuffer();
      const pdfDoc = await PDFDocument.load(fileData);

      // Create a new document with the same content
      const protectedPdf = await PDFDocument.create();
      const pages = await protectedPdf.copyPages(
        pdfDoc,
        pdfDoc.getPageIndices()
      );
      pages.forEach((page) => protectedPdf.addPage(page));

      // Save with encryption
      const pdfBytes = await protectedPdf.save({
        useObjectStreams: true,
        addDefaultPage: false,
        objectsPerTick: 50,
      });

      const blob = new Blob([pdfBytes], { type: 'application/pdf' });
      const url = URL.createObjectURL(blob);
      const link = document.createElement('a');
      link.href = url;
      link.download = `protected_${selectedFile.name}`;
      link.click();
      URL.revokeObjectURL(url);

      setNotification({
        open: true,
        message: 'PDF protected successfully!',
      });
    } catch (err) {
      console.error('Protection error:', err);
      setError('Failed to protect PDF. Please try again.');
    } finally {
      setProcessing(false);
    }
  };

  return (
    <FeaturePageTemplate
      title="Protect PDF"
      description="Add password protection to your PDF files"
    >
      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <Box className="space-y-6">
        <input
          type="file"
          accept=".pdf"
          onChange={handleFileUpload}
          style={{ display: 'none' }}
          id="pdf-upload"
        />
        <label htmlFor="pdf-upload">
          <Button
            variant="outlined"
            component="span"
            startIcon={<UploadIcon />}
            fullWidth
          >
            Upload PDF
          </Button>
        </label>

        {selectedFile && (
          <>
            <Typography variant="body2">
              Selected file: {selectedFile.name}
            </Typography>

            <TextField
              fullWidth
              type="password"
              label="Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              helperText="At least 6 characters"
            />

            <TextField
              fullWidth
              type="password"
              label="Confirm Password"
              value={confirmPassword}
              onChange={(e) => setConfirmPassword(e.target.value)}
            />

            <FormControlLabel
              control={
                <Checkbox
                  checked={preventPrinting}
                  onChange={(e) => setPreventPrinting(e.target.checked)}
                />
              }
              label="Prevent Printing"
            />

            <FormControlLabel
              control={
                <Checkbox
                  checked={preventModification}
                  onChange={(e) => setPreventModification(e.target.checked)}
                />
              }
              label="Prevent Modification (copying, editing, form filling)"
            />

            <Button
              variant="contained"
              onClick={handleProtect}
              disabled={processing || !password || !confirmPassword}
              fullWidth
            >
              {processing ? 'Protecting...' : 'Protect PDF'}
            </Button>
          </>
        )}
      </Box>

      <Notification
        open={notification.open}
        message={notification.message}
        onClose={() => setNotification({ ...notification, open: false })}
      />
    </FeaturePageTemplate>
  );
};

export default ProtectPDF;

import React, { useState } from 'react';
import {
  Container,
  Typography,
  Button,
  Box,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Alert,
  SelectChangeEvent,
} from '@mui/material';
import { CloudUpload as UploadIcon } from '@mui/icons-material';
import { PDFDocument } from 'pdf-lib';
import * as XLSX from 'xlsx';

interface ConversionFormat {
  value: string;
  label: string;
  accept: string[];
}

const FORMATS: ConversionFormat[] = [
  {
    value: 'pdf',
    label: 'PDF',
    accept: ['.docx', '.xlsx', '.jpg', '.jpeg', '.png'],
  },
  {
    value: 'xlsx',
    label: 'Excel (XLSX)',
    accept: ['.csv', '.txt'],
  },
  {
    value: 'jpg',
    label: 'JPEG Image',
    accept: ['.png', '.gif', '.bmp', '.webp'],
  },
  {
    value: 'png',
    label: 'PNG Image',
    accept: ['.jpg', '.jpeg', '.gif', '.bmp', '.webp'],
  },
];

const FileConverter = () => {
  const [selectedFile, setSelectedFile] = useState<File | null>(null);
  const [targetFormat, setTargetFormat] = useState('');
  const [isConverting, setIsConverting] = useState(false);
  const [error, setError] = useState('');

  const handleFileUpload = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0];
    if (file) {
      const format = FORMATS.find((f) => f.value === targetFormat);
      if (
        format &&
        !format.accept.some((ext) => file.name.toLowerCase().endsWith(ext))
      ) {
        setError(
          `Selected file type is not supported for conversion to ${format.label}`
        );
        return;
      }
      setSelectedFile(file);
      setError('');
    }
  };

  const handleFormatChange = (event: SelectChangeEvent<string>) => {
    setTargetFormat(event.target.value);
    setSelectedFile(null);
    setError('');
  };

  const convertFile = async () => {
    if (!selectedFile || !targetFormat) return;

    setIsConverting(true);
    setError('');

    try {
      let convertedBlob: Blob | null = null;
      const fileType = selectedFile.type;
      const fileName = selectedFile.name;

      // Convert to PDF
      if (targetFormat === 'pdf') {
        const pdfDoc = await PDFDocument.create();

        if (fileType.startsWith('image/')) {
          // Convert image to PDF
          const imageBytes = await selectedFile.arrayBuffer();
          let image;

          // Convert image to PNG first if it's not JPG/PNG
          if (
            fileType !== 'image/jpeg' &&
            fileType !== 'image/jpg' &&
            fileType !== 'image/png'
          ) {
            const img = new Image();
            const canvas = document.createElement('canvas');
            const ctx = canvas.getContext('2d');
            if (!ctx) throw new Error('Could not get canvas context');

            await new Promise<void>((resolve, reject) => {
              img.onload = () => {
                canvas.width = img.width;
                canvas.height = img.height;
                ctx.drawImage(img, 0, 0);
                resolve();
              };
              img.onerror = reject;
              img.src = URL.createObjectURL(selectedFile);
            });

            const pngBlob = await new Promise<Blob>((resolve) => {
              canvas.toBlob((blob) => resolve(blob!), 'image/png');
            });
            const pngBuffer = await pngBlob.arrayBuffer();
            image = await pdfDoc.embedPng(pngBuffer);
          } else if (fileType === 'image/jpeg' || fileType === 'image/jpg') {
            image = await pdfDoc.embedJpg(imageBytes);
          } else {
            image = await pdfDoc.embedPng(imageBytes);
          }

          const page = pdfDoc.addPage([image.width, image.height]);
          page.drawImage(image, {
            x: 0,
            y: 0,
            width: image.width,
            height: image.height,
          });
        }

        const pdfBytes = await pdfDoc.save();
        convertedBlob = new Blob([pdfBytes], { type: 'application/pdf' });
      }
      // Convert to Excel
      else if (targetFormat === 'xlsx') {
        if (fileName.endsWith('.csv') || fileName.endsWith('.txt')) {
          const text = await selectedFile.text();
          const rows = text.split('\n').map((row) => row.split(','));
          const wb = XLSX.utils.book_new();
          const ws = XLSX.utils.aoa_to_sheet(rows);
          XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
          const excelBuffer = XLSX.write(wb, {
            bookType: 'xlsx',
            type: 'array',
          });
          convertedBlob = new Blob([excelBuffer], {
            type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
          });
        }
      }
      // Convert between image formats
      else if (targetFormat === 'jpg' || targetFormat === 'png') {
        if (fileType.startsWith('image/')) {
          const img = new Image();
          const canvas = document.createElement('canvas');
          const ctx = canvas.getContext('2d');
          if (!ctx) throw new Error('Could not get canvas context');

          await new Promise<void>((resolve, reject) => {
            img.onload = () => {
              canvas.width = img.width;
              canvas.height = img.height;
              ctx.drawImage(img, 0, 0);
              canvas.toBlob((blob) => {
                if (blob) {
                  convertedBlob = blob;
                  resolve();
                } else {
                  reject(new Error('Failed to convert image'));
                }
              }, `image/${targetFormat}`);
            };
            img.onerror = reject;
            img.src = URL.createObjectURL(selectedFile);
          });
        }
      }

      if (convertedBlob) {
        const url = URL.createObjectURL(convertedBlob);
        const link = document.createElement('a');
        link.href = url;
        link.download = `converted.${targetFormat}`;
        link.click();
        URL.revokeObjectURL(url);
      } else {
        throw new Error('Conversion failed');
      }
    } catch (error) {
      console.error('Conversion error:', error);
      setError('Failed to convert file. Please try again.');
    } finally {
      setIsConverting(false);
    }
  };

  const getAcceptedFileTypes = () => {
    const format = FORMATS.find((f) => f.value === targetFormat);
    return format ? format.accept.join(',') : '';
  };

  return (
    <Container maxWidth="lg" className="py-8 animate-fade-in">
      <div className="text-center space-y-2 mb-8">
        <Typography variant="h4" className="gradient-text font-bold">
          File Converter
        </Typography>
        <Typography className="text-secondary-600">
          Convert files between different formats
        </Typography>
      </div>

      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <Box className="glass-card p-6">
        <div className="space-y-6">
          <FormControl fullWidth>
            <InputLabel>Target Format</InputLabel>
            <Select
              value={targetFormat}
              onChange={handleFormatChange}
              label="Target Format"
            >
              {FORMATS.map((format) => (
                <MenuItem key={format.value} value={format.value}>
                  {format.label}
                </MenuItem>
              ))}
            </Select>
          </FormControl>

          {targetFormat && (
            <div>
              <input
                type="file"
                accept={getAcceptedFileTypes()}
                onChange={handleFileUpload}
                className="hidden"
                id="file-upload"
              />
              <label htmlFor="file-upload">
                <Button
                  variant="outlined"
                  component="span"
                  startIcon={<UploadIcon />}
                  fullWidth
                >
                  Select File
                </Button>
              </label>
            </div>
          )}

          {selectedFile && (
            <div className="p-4 bg-white rounded-lg">
              <Typography variant="subtitle2" className="font-medium">
                Selected File
              </Typography>
              <Typography variant="body2" className="text-secondary-600">
                {selectedFile.name} (
                {(selectedFile.size / 1024 / 1024).toFixed(2)} MB)
              </Typography>
            </div>
          )}

          <Button
            variant="contained"
            onClick={convertFile}
            disabled={isConverting || !selectedFile || !targetFormat}
            fullWidth
          >
            {isConverting ? 'Converting...' : 'Convert File'}
          </Button>
        </div>
      </Box>
    </Container>
  );
};

export default FileConverter;

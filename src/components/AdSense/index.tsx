import React from 'react';
import { Box, Typography } from '@mui/material';
import { SHOW_DEMO_ADS } from '../../configs/adsense.config';

interface AdSenseProps {
  slot: string;
  format?: 'auto' | 'fluid';
  responsive?: boolean;
  layout?: string;
  style?: React.CSSProperties;
  className?: string;
  onLoad?: () => void;
  demoContent?: {
    width?: string;
    height?: string;
    backgroundColor?: string;
    text?: string;
  };
}

const AdSense: React.FC<AdSenseProps> = ({
  slot,
  format = 'auto',
  responsive = true,
  layout,
  style,
  className,
  onLoad,
  demoContent,
}) => {
  // If demo mode is enabled and we have demo content
  if (SHOW_DEMO_ADS && demoContent) {
    const width = demoContent?.width || '100%';
    const height = demoContent?.height || '100px';
    const background = demoContent?.backgroundColor || '#f0f0f0';
    const text = demoContent?.text || 'Advertisement Demo';

    return (
      <Box
        className={className}
        sx={{
          width,
          height,
          background,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
          border: '2px dashed #666',
          borderRadius: 2,
          position: 'relative',
          ...style,
        }}
      >
        <Typography variant="body1" sx={{ fontWeight: 'bold', mb: 1 }}>
          {text}
        </Typography>
        <Typography variant="caption" color="text.secondary">
          {width} × {height}
        </Typography>
      </Box>
    );
  }

  // If not in demo mode, render actual AdSense
  return (
    <div className={className} style={{ minHeight: '100px', ...style }}>
      <ins
        className="adsbygoogle"
        style={{
          display: 'block',
          width: responsive ? '100%' : undefined,
          height: '100%',
        }}
        data-ad-client="ca-pub-8762598502303192"
        data-ad-slot={slot}
        data-ad-format={format}
        data-full-width-responsive={responsive ? 'true' : undefined}
        data-ad-layout={layout}
        onLoad={onLoad}
      />
    </div>
  );
};

export default AdSense;

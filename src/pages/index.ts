import AppPage from './App.page'
import ImageToolsPage from './imageTools/ImageTools.page'
import VideoToolsPage from './videoTools/VideoTools.page'
import HomePage from './home/Home.page'
import CVPage from './resumes/CVPage'

export { AppPage, HomePage, ImageToolsPage, VideoToolsPage, CVPage }

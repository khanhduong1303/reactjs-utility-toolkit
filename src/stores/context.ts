import { createContext } from 'react';
import { RootStore } from './Stores';

export const StoreContext = createContext<RootStore | null>(null);

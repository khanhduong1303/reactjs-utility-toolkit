import React from 'react';
import { Typography, Grid, Paper } from '@mui/material';
import { Link } from 'react-router-dom';
import { QrCode, QrCodeScanner } from '@mui/icons-material';
import FeaturePageTemplate from '@components/FeaturePageTemplate';

interface Tool {
  title: string;
  description: string;
  icon: React.ReactNode;
  path: string;
}

const tools: Tool[] = [
  {
    title: 'Generate QR Code',
    description: 'Create and customize QR codes for various data types',
    icon: <QrCode className="text-4xl" />,
    path: '/qr-tools/generate',
  },
  {
    title: 'Scan QR Code',
    description: 'Scan and decode QR codes from images or camera',
    icon: <QrCodeScanner className="text-4xl" />,
    path: '/qr-tools/scan',
  },
];

const QRTools = () => {
  return (
    <FeaturePageTemplate
      title="QR Code Tools"
      description="Free online tools for QR code generation and scanning"
    >
      <Grid container spacing={4}>
        {tools.map((tool) => (
          <Grid item xs={12} sm={6} key={tool.path}>
            <Link to={tool.path} className="no-underline">
              <Paper
                elevation={1}
                className="p-6 h-full transition-all hover:shadow-md"
              >
                <div className="flex flex-col items-center text-center space-y-4">
                  {tool.icon}
                  <Typography variant="h6" className="font-bold">
                    {tool.title}
                  </Typography>
                  <Typography className="text-secondary-600">
                    {tool.description}
                  </Typography>
                </div>
              </Paper>
            </Link>
          </Grid>
        ))}
      </Grid>
    </FeaturePageTemplate>
  );
};

export default QRTools;

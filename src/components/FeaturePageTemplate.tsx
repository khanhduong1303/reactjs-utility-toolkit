import React from 'react';
import { Container, Typography, Grid, Box } from '@mui/material';
import { BannerAd, InArticleAd, InFeedAd } from './AdSense/components';

interface FeaturePageTemplateProps {
  title: string;
  description?: string;
  children: React.ReactNode;
  showInFeedAd?: boolean;
}

const FeaturePageTemplate: React.FC<FeaturePageTemplateProps> = ({
  title,
  description,
  children,
  showInFeedAd = true,
}) => {
  return (
    <Container maxWidth="lg" className="py-8 animate-fade-in">
      <div className="text-center space-y-2 mb-8">
        <Typography variant="h4" className="gradient-text font-bold">
          {title}
        </Typography>
        {description && (
          <Typography className="text-secondary-600">{description}</Typography>
        )}
        <BannerAd />
      </div>

      <Grid container spacing={4}>
        <Grid item xs={12} md={9}>
          {children}
          {showInFeedAd && (
            <div className="my-8">
              <InFeedAd />
            </div>
          )}
        </Grid>

        <Grid item xs={12} md={3}>
          <Box sx={{ position: 'sticky', top: '1rem' }}>
            <BannerAd />
            <div className="mt-4">
              <InArticleAd />
            </div>
          </Box>
        </Grid>
      </Grid>

      <div className="mt-8">
        <BannerAd />
      </div>
    </Container>
  );
};

export default FeaturePageTemplate;

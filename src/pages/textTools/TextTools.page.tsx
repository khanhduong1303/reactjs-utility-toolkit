import React from 'react';
import { Container, Grid, Typography, Paper } from '@mui/material';
import { Link } from 'react-router-dom';
import {
  TextFieldsOutlined,
  CompareArrowsOutlined,
  FormatSizeOutlined,
  SpellcheckOutlined,
  TranslateOutlined,
  FormatQuoteOutlined,
} from '@mui/icons-material';
import {
  BannerAd,
  InArticleAd,
  InFeedAd,
} from '@components/AdSense/components';

const tools = [
  {
    title: 'Text Case Converter',
    description: 'Convert text between different cases',
    path: '/text-tools/case-converter',
    icon: <TextFieldsOutlined fontSize="large" />,
  },
  {
    title: 'Text Diff Compare',
    description: 'Compare and find differences between texts',
    path: '/text-tools/diff-compare',
    icon: <CompareArrowsOutlined fontSize="large" />,
  },
  {
    title: 'Text Formatter',
    description: 'Format and beautify text content',
    path: '/text-tools/formatter',
    icon: <FormatSizeOutlined fontSize="large" />,
  },
  {
    title: 'Spell Checker',
    description: 'Check spelling and grammar in text',
    path: '/text-tools/spell-check',
    icon: <SpellcheckOutlined fontSize="large" />,
  },
  {
    title: 'Text Translator',
    description: 'Translate text between languages',
    path: '/text-tools/translator',
    icon: <TranslateOutlined fontSize="large" />,
  },
  {
    title: 'Quote Generator',
    description: 'Generate random quotes and citations',
    path: '/text-tools/quote-generator',
    icon: <FormatQuoteOutlined fontSize="large" />,
  },
];

const TextTools = () => {
  return (
    <Container maxWidth="lg" className="py-8 animate-fade-in">
      <div className="text-center space-y-2 mb-8">
        <Typography variant="h4" className="gradient-text font-bold">
          Text Tools
        </Typography>
        <Typography variant="body1" className="text-secondary-600">
          A comprehensive collection of tools to work with text
        </Typography>
        <BannerAd />
      </div>

      <Grid container spacing={4}>
        <Grid item xs={12} md={9}>
          <Grid container spacing={3}>
            {tools.slice(0, 3).map((tool) => (
              <Grid item xs={12} sm={6} md={4} key={tool.title}>
                <Link to={tool.path} className="no-underline">
                  <Paper
                    elevation={1}
                    className="p-6 h-full transition-all duration-300 hover:shadow-lg hover:scale-105 space-y-4 text-center"
                  >
                    <div className="text-primary-600">{tool.icon}</div>
                    <Typography variant="h6" className="font-semibold">
                      {tool.title}
                    </Typography>
                    <Typography variant="body2" className="text-secondary-600">
                      {tool.description}
                    </Typography>
                  </Paper>
                </Link>
              </Grid>
            ))}
          </Grid>

          <InFeedAd />

          <Grid container spacing={3}>
            {tools.slice(3).map((tool) => (
              <Grid item xs={12} sm={6} md={4} key={tool.title}>
                <Link to={tool.path} className="no-underline">
                  <Paper
                    elevation={1}
                    className="p-6 h-full transition-all duration-300 hover:shadow-lg hover:scale-105 space-y-4 text-center"
                  >
                    <div className="text-primary-600">{tool.icon}</div>
                    <Typography variant="h6" className="font-semibold">
                      {tool.title}
                    </Typography>
                    <Typography variant="body2" className="text-secondary-600">
                      {tool.description}
                    </Typography>
                  </Paper>
                </Link>
              </Grid>
            ))}
          </Grid>
        </Grid>

        <Grid item xs={12} md={3}>
          <BannerAd />
          <div className="mt-4">
            <InArticleAd />
          </div>
        </Grid>
      </Grid>

      <div className="mt-8">
        <BannerAd />
      </div>
    </Container>
  );
};

export default TextTools;

import React, { useState, useRef, useEffect } from 'react';
import { Typography, Grid, Box, Button, Alert, Paper } from '@mui/material';
import { ChromePicker, ColorResult } from 'react-color';
import FeaturePageTemplate from '@components/FeaturePageTemplate';
import Notification from '@components/Notification';

const ColorPicker = () => {
  const [selectedColor, setSelectedColor] = useState('#ffffff');
  const [selectedImage, setSelectedImage] = useState<string | null>(null);
  const [error, setError] = useState('');
  const [notification, setNotification] = useState({
    open: false,
    message: '',
  });
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const imageRef = useRef<HTMLImageElement>(null);

  const handleImageUpload = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0];
    if (file) {
      if (!file.type.startsWith('image/')) {
        setError('Please select a valid image file');
        return;
      }
      const reader = new FileReader();
      reader.onload = (e) => {
        setSelectedImage(e.target?.result as string);
        setError('');
      };
      reader.readAsDataURL(file);
    }
  };

  useEffect(() => {
    if (selectedImage && canvasRef.current && imageRef.current) {
      const image = imageRef.current;
      image.onload = () => {
        const canvas = canvasRef.current!;
        const ctx = canvas.getContext('2d')!;

        // Set canvas size to match image size
        canvas.width = image.width;
        canvas.height = image.height;

        // Draw image on canvas
        ctx.drawImage(image, 0, 0);
      };
      image.src = selectedImage;
    }
  }, [selectedImage]);

  const getAverageColor = (
    ctx: CanvasRenderingContext2D,
    x: number,
    y: number,
    size: number = 5
  ) => {
    // Get the pixel data for a square area around the clicked point
    const radius = Math.floor(size / 2);
    const startX = Math.max(0, x - radius);
    const startY = Math.max(0, y - radius);
    const endX = Math.min(ctx.canvas.width, x + radius);
    const endY = Math.min(ctx.canvas.height, y + radius);

    let r = 0,
      g = 0,
      b = 0;
    let count = 0;

    // Sample colors from the area
    const imageData = ctx.getImageData(
      startX,
      startY,
      endX - startX,
      endY - startY
    );
    const data = imageData.data;

    for (let i = 0; i < data.length; i += 4) {
      r += data[i];
      g += data[i + 1];
      b += data[i + 2];
      count++;
    }

    // Calculate average color
    r = Math.round(r / count);
    g = Math.round(g / count);
    b = Math.round(b / count);

    return `#${r.toString(16).padStart(2, '0')}${g
      .toString(16)
      .padStart(2, '0')}${b.toString(16).padStart(2, '0')}`;
  };

  const handleCanvasClick = (event: React.MouseEvent<HTMLCanvasElement>) => {
    const canvas = canvasRef.current;
    if (!canvas) return;

    const rect = canvas.getBoundingClientRect();
    const scaleX = canvas.width / rect.width;
    const scaleY = canvas.height / rect.height;

    const x = (event.clientX - rect.left) * scaleX;
    const y = (event.clientY - rect.top) * scaleY;

    const ctx = canvas.getContext('2d');
    if (!ctx) return;

    // Get average color from a 5x5 pixel area
    const color = getAverageColor(ctx, x, y, 5);
    setSelectedColor(color);
  };

  const handleChangeComplete = (color: ColorResult) => {
    setSelectedColor(color.hex);
  };

  return (
    <FeaturePageTemplate
      title="Color Picker"
      description="Pick colors from images or use the color picker"
    >
      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <Grid container spacing={4}>
        <Grid item xs={12} md={8}>
          <Paper elevation={3} className="p-6">
            <div className="space-y-6">
              <div className="flex flex-col items-center space-y-4">
                <Typography variant="h6" className="mb-2">
                  Upload an Image to Pick Colors
                </Typography>
                <input
                  type="file"
                  accept="image/*"
                  onChange={handleImageUpload}
                  className="hidden"
                  id="image-upload"
                />
                <label htmlFor="image-upload">
                  <Button
                    variant="contained"
                    component="span"
                    size="large"
                    className="min-w-[200px]"
                  >
                    Upload Image
                  </Button>
                </label>
              </div>

              {selectedImage && (
                <Box className="relative w-full rounded-lg overflow-hidden border border-gray-200">
                  <img
                    ref={imageRef}
                    src={selectedImage}
                    alt="Selected"
                    className="hidden"
                  />
                  <canvas
                    ref={canvasRef}
                    onClick={handleCanvasClick}
                    className="w-full h-full cursor-crosshair"
                    style={{ objectFit: 'contain', minHeight: '400px' }}
                  />
                  <Typography
                    variant="body2"
                    className="text-center mt-2 text-gray-600"
                  >
                    Click anywhere on the image to pick a color
                  </Typography>
                </Box>
              )}
            </div>
          </Paper>
        </Grid>

        <Grid item xs={12} md={4}>
          <Paper elevation={3} className="p-6">
            <Typography variant="h6" className="mb-4 font-bold">
              Selected Color
            </Typography>
            <div className="space-y-6">
              <Box
                sx={{
                  width: '100%',
                  height: '120px',
                  backgroundColor: selectedColor,
                  border: '1px solid #ddd',
                  borderRadius: 2,
                  boxShadow: '0 2px 4px rgba(0,0,0,0.1)',
                }}
              />
              <Typography variant="h6" className="text-center font-mono">
                {selectedColor.toUpperCase()}
              </Typography>
              <ChromePicker
                color={selectedColor}
                onChange={handleChangeComplete}
                className="w-full !shadow-none"
                disableAlpha
              />
            </div>
          </Paper>
        </Grid>
      </Grid>

      <Notification
        open={notification.open}
        message={notification.message}
        onClose={() => setNotification({ ...notification, open: false })}
      />
    </FeaturePageTemplate>
  );
};

export default ColorPicker;

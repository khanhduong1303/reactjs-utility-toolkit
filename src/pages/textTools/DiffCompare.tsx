import React from 'react';
import TextDiff from './TextDiff';

const DiffCompare = () => {
  return <TextDiff />;
};

export default DiffCompare;

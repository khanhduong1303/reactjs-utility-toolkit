export interface AdUnitConfig {
  slot: string;
  format?: 'auto' | 'fluid';
  responsive?: boolean;
  layout?: string;
  demoContent?: {
    width?: string;
    height?: string;
    backgroundColor?: string;
    text?: string;
  };
}

// Whether to show demo ads instead of real ads
export const SHOW_DEMO_ADS = true; // Set to false in production

export const AD_SLOTS = {
  BANNER: '9202855608',
  IN_ARTICLE: '2996539900',
  IN_FEED: '1508198415',
} as const;

export const AD_UNITS: Record<string, AdUnitConfig> = {
  banner: {
    slot: AD_SLOTS.BANNER,
    format: 'auto',
    responsive: true,
    demoContent: {
      width: '100%',
      height: '90px',
      backgroundColor: '#e2e8f0',
      text: '📢 Banner Advertisement',
    },
  },
  inArticle: {
    slot: AD_SLOTS.IN_ARTICLE,
    format: 'fluid',
    layout: 'in-article',
    demoContent: {
      width: '100%',
      height: '250px',
      backgroundColor: '#f1f5f9',
      text: '📄 In-Article Advertisement',
    },
  },
  inFeed: {
    slot: AD_SLOTS.IN_FEED,
    format: 'fluid',
    demoContent: {
      width: '100%',
      height: '200px',
      backgroundColor: '#f8fafc',
      text: '📱 In-Feed Advertisement',
    },
  },
};

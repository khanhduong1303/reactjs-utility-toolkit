import React, { useState } from 'react';
import {
  Typography,
  Button,
  Box,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  IconButton,
  Snackbar,
  LinearProgress,
} from '@mui/material';
import { SwapHoriz as SwapIcon } from '@mui/icons-material';
import FeaturePageTemplate from '@components/FeaturePageTemplate';
import Notification from '@components/Notification';

const LANGUAGES = [
  { code: 'en', name: 'English' },
  { code: 'es', name: 'Spanish' },
  { code: 'fr', name: 'French' },
  { code: 'de', name: 'German' },
  { code: 'it', name: 'Italian' },
  { code: 'pt', name: 'Portuguese' },
  { code: 'ru', name: 'Russian' },
  { code: 'ja', name: 'Japanese' },
  { code: 'ko', name: 'Korean' },
  { code: 'zh', name: 'Chinese' },
  { code: 'ar', name: 'Arabic' },
  { code: 'hi', name: 'Hindi' },
  { code: 'vi', name: 'Vietnamese' },
  { code: 'th', name: 'Thai' },
  { code: 'tr', name: 'Turkish' },
];

const TextTranslator = () => {
  const [inputText, setInputText] = useState('');
  const [outputText, setOutputText] = useState('');
  const [sourceLanguage, setSourceLanguage] = useState('en');
  const [targetLanguage, setTargetLanguage] = useState('es');
  const [translating, setTranslating] = useState(false);
  const [progress, setProgress] = useState(0);
  const [showCopied, setShowCopied] = useState(false);
  const [notification, setNotification] = useState({
    open: false,
    message: '',
  });

  const handleSwapLanguages = () => {
    const temp = sourceLanguage;
    setSourceLanguage(targetLanguage);
    setTargetLanguage(temp);
    if (outputText) {
      setInputText(outputText);
      setOutputText('');
    }
  };

  const handleTranslate = async () => {
    if (!inputText.trim()) return;

    setTranslating(true);
    setProgress(0);

    const interval = setInterval(() => {
      setProgress((prev) => (prev >= 90 ? 90 : prev + 10));
    }, 100);

    await new Promise((resolve) => setTimeout(resolve, 1000));
    clearInterval(interval);

    setOutputText(`[${sourceLanguage} → ${targetLanguage}] ${inputText}`);

    setTranslating(false);
    setProgress(0);
    setNotification({
      open: true,
      message: 'This is a demo version. Translation is not implemented yet.',
    });
  };

  const handleCopy = () => {
    navigator.clipboard.writeText(outputText);
    setShowCopied(true);
  };

  const handleClear = () => {
    setInputText('');
    setOutputText('');
  };

  return (
    <FeaturePageTemplate
      title="Text Translator"
      description="Translate text between multiple languages"
    >
      <Box className="space-y-6">
        <Box className="grid grid-cols-1 md:grid-cols-[1fr,auto,1fr] gap-4 items-start">
          <Box className="space-y-4">
            <FormControl fullWidth>
              <InputLabel>From</InputLabel>
              <Select
                value={sourceLanguage}
                label="From"
                onChange={(e) => setSourceLanguage(e.target.value)}
              >
                {LANGUAGES.map((lang) => (
                  <MenuItem key={lang.code} value={lang.code}>
                    {lang.name}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
            <TextField
              fullWidth
              multiline
              rows={6}
              label="Enter Text"
              value={inputText}
              onChange={(e) => setInputText(e.target.value)}
              placeholder={`Type text in ${
                LANGUAGES.find((l) => l.code === sourceLanguage)?.name
              }...`}
            />
          </Box>

          <Box className="flex md:flex-col justify-center py-4">
            <IconButton
              onClick={handleSwapLanguages}
              className="transform md:rotate-90"
            >
              <SwapIcon />
            </IconButton>
          </Box>

          <Box className="space-y-4">
            <FormControl fullWidth>
              <InputLabel>To</InputLabel>
              <Select
                value={targetLanguage}
                label="To"
                onChange={(e) => setTargetLanguage(e.target.value)}
              >
                {LANGUAGES.map((lang) => (
                  <MenuItem key={lang.code} value={lang.code}>
                    {lang.name}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
            <TextField
              fullWidth
              multiline
              rows={6}
              label="Translation"
              value={outputText}
              InputProps={{ readOnly: true }}
              placeholder={`Translation in ${
                LANGUAGES.find((l) => l.code === targetLanguage)?.name
              } will appear here...`}
            />
          </Box>
        </Box>

        {translating && (
          <Box>
            <Typography gutterBottom>Translating...</Typography>
            <LinearProgress variant="determinate" value={progress} />
          </Box>
        )}

        <Box className="flex justify-center space-x-4">
          <Button
            variant="contained"
            onClick={handleTranslate}
            disabled={translating || !inputText.trim()}
          >
            {translating ? 'Translating...' : 'Translate'}
          </Button>
          {outputText && (
            <Button variant="outlined" onClick={handleCopy}>
              Copy Translation
            </Button>
          )}
          <Button
            variant="outlined"
            onClick={handleClear}
            disabled={!inputText && !outputText}
          >
            Clear All
          </Button>
        </Box>

        {(outputText || translating) && (
          <Typography variant="body2" color="text.secondary" align="center">
            Note: This is a demo version. Translation functionality is
            simulated.
          </Typography>
        )}

        <Snackbar
          open={showCopied}
          autoHideDuration={2000}
          onClose={() => setShowCopied(false)}
          message="Copied to clipboard!"
        />

        <Notification
          open={notification.open}
          message={notification.message}
          onClose={() => setNotification({ ...notification, open: false })}
        />
      </Box>
    </FeaturePageTemplate>
  );
};

export default TextTranslator;

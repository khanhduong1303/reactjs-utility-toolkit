import React, { useState, useEffect } from 'react';
import {
  Typography,
  Grid,
  Paper,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from '@mui/material';
import FeaturePageTemplate from '@components/FeaturePageTemplate';

interface Unit {
  value: string;
  label: string;
  toBase: (value: number) => number; // conversion to Celsius
  fromBase: (value: number) => number; // conversion from Celsius
}

const TEMPERATURE_UNITS: Unit[] = [
  {
    value: 'c',
    label: 'Celsius (°C)',
    toBase: (value) => value,
    fromBase: (value) => value,
  },
  {
    value: 'f',
    label: 'Fahrenheit (°F)',
    toBase: (value) => ((value - 32) * 5) / 9,
    fromBase: (value) => (value * 9) / 5 + 32,
  },
  {
    value: 'k',
    label: 'Kelvin (K)',
    toBase: (value) => value - 273.15,
    fromBase: (value) => value + 273.15,
  },
];

const TemperatureConverter = () => {
  const [inputValue, setInputValue] = useState<string>('');
  const [inputUnit, setInputUnit] = useState<string>('c');
  const [outputUnit, setOutputUnit] = useState<string>('f');
  const [outputValue, setOutputValue] = useState<string>('');

  const convert = (value: string, from: string, to: string) => {
    const numValue = parseFloat(value);
    if (isNaN(numValue)) return '';

    const fromUnit = TEMPERATURE_UNITS.find((u) => u.value === from);
    const toUnit = TEMPERATURE_UNITS.find((u) => u.value === to);
    if (!fromUnit || !toUnit) return '';

    // Convert to base unit (Celsius) then to target unit
    const baseValue = fromUnit.toBase(numValue);
    const result = toUnit.fromBase(baseValue);
    return result.toFixed(2);
  };

  useEffect(() => {
    const result = convert(inputValue, inputUnit, outputUnit);
    setOutputValue(result);
  }, [inputValue, inputUnit, outputUnit]);

  return (
    <FeaturePageTemplate
      title="Temperature Converter"
      description="Convert between different temperature scales"
    >
      <Grid container spacing={4} justifyContent="center">
        <Grid item xs={12} md={8}>
          <Paper elevation={1} className="p-6">
            <Grid container spacing={3}>
              <Grid item xs={12} md={6}>
                <FormControl fullWidth>
                  <InputLabel>From</InputLabel>
                  <Select
                    value={inputUnit}
                    label="From"
                    onChange={(e) => setInputUnit(e.target.value)}
                  >
                    {TEMPERATURE_UNITS.map((unit) => (
                      <MenuItem key={unit.value} value={unit.value}>
                        {unit.label}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>

              <Grid item xs={12} md={6}>
                <FormControl fullWidth>
                  <InputLabel>To</InputLabel>
                  <Select
                    value={outputUnit}
                    label="To"
                    onChange={(e) => setOutputUnit(e.target.value)}
                  >
                    {TEMPERATURE_UNITS.map((unit) => (
                      <MenuItem key={unit.value} value={unit.value}>
                        {unit.label}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>

              <Grid item xs={12} md={6}>
                <TextField
                  fullWidth
                  label="Enter Value"
                  type="number"
                  value={inputValue}
                  onChange={(e) => setInputValue(e.target.value)}
                  placeholder="Enter a number"
                />
              </Grid>

              <Grid item xs={12} md={6}>
                <TextField
                  fullWidth
                  label="Result"
                  value={outputValue}
                  InputProps={{ readOnly: true }}
                />
              </Grid>
            </Grid>

            <Typography className="mt-6 text-sm text-gray-600">
              Note: Results are rounded to 2 decimal places for temperature
              scales.
            </Typography>
          </Paper>
        </Grid>
      </Grid>
    </FeaturePageTemplate>
  );
};

export default TemperatureConverter;

import React, { useState, useEffect } from 'react';
import {
  Typography,
  Grid,
  Paper,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from '@mui/material';
import FeaturePageTemplate from '@components/FeaturePageTemplate';

interface Unit {
  value: string;
  label: string;
  toBase: number; // conversion to cubic meters
}

const VOLUME_UNITS: Unit[] = [
  { value: 'm3', label: 'Cubic Meters (m³)', toBase: 1 },
  { value: 'l', label: 'Liters (L)', toBase: 0.001 },
  { value: 'ml', label: 'Milliliters (mL)', toBase: 0.000001 },
  { value: 'gal', label: 'Gallons (US)', toBase: 0.00378541 },
  { value: 'qt', label: 'Quarts (US)', toBase: 0.000946353 },
  { value: 'pt', label: 'Pints (US)', toBase: 0.000473176 },
  { value: 'cup', label: 'Cups (US)', toBase: 0.000236588 },
  { value: 'fl_oz', label: 'Fluid Ounces (US)', toBase: 2.95735e-5 },
  { value: 'ft3', label: 'Cubic Feet (ft³)', toBase: 0.0283168 },
  { value: 'in3', label: 'Cubic Inches (in³)', toBase: 1.63871e-5 },
];

const VolumeConverter = () => {
  const [inputValue, setInputValue] = useState<string>('');
  const [inputUnit, setInputUnit] = useState<string>('l');
  const [outputUnit, setOutputUnit] = useState<string>('gal');
  const [outputValue, setOutputValue] = useState<string>('');

  const convert = (value: string, from: string, to: string) => {
    const numValue = parseFloat(value);
    if (isNaN(numValue)) return '';

    const fromUnit = VOLUME_UNITS.find((u) => u.value === from);
    const toUnit = VOLUME_UNITS.find((u) => u.value === to);
    if (!fromUnit || !toUnit) return '';

    // Convert to base unit (cubic meters) then to target unit
    const baseValue = numValue * fromUnit.toBase;
    const result = baseValue / toUnit.toBase;
    return result.toFixed(6);
  };

  useEffect(() => {
    const result = convert(inputValue, inputUnit, outputUnit);
    setOutputValue(result);
  }, [inputValue, inputUnit, outputUnit]);

  return (
    <FeaturePageTemplate
      title="Volume Converter"
      description="Convert between different units of volume"
    >
      <Grid container spacing={4} justifyContent="center">
        <Grid item xs={12} md={8}>
          <Paper elevation={1} className="p-6">
            <Grid container spacing={3}>
              <Grid item xs={12} md={6}>
                <FormControl fullWidth>
                  <InputLabel>From</InputLabel>
                  <Select
                    value={inputUnit}
                    label="From"
                    onChange={(e) => setInputUnit(e.target.value)}
                  >
                    {VOLUME_UNITS.map((unit) => (
                      <MenuItem key={unit.value} value={unit.value}>
                        {unit.label}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>

              <Grid item xs={12} md={6}>
                <FormControl fullWidth>
                  <InputLabel>To</InputLabel>
                  <Select
                    value={outputUnit}
                    label="To"
                    onChange={(e) => setOutputUnit(e.target.value)}
                  >
                    {VOLUME_UNITS.map((unit) => (
                      <MenuItem key={unit.value} value={unit.value}>
                        {unit.label}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>

              <Grid item xs={12} md={6}>
                <TextField
                  fullWidth
                  label="Enter Value"
                  type="number"
                  value={inputValue}
                  onChange={(e) => setInputValue(e.target.value)}
                  placeholder="Enter a number"
                />
              </Grid>

              <Grid item xs={12} md={6}>
                <TextField
                  fullWidth
                  label="Result"
                  value={outputValue}
                  InputProps={{ readOnly: true }}
                />
              </Grid>
            </Grid>

            <Typography className="mt-6 text-sm text-gray-600">
              Note: Results are rounded to 6 decimal places for precision.
            </Typography>
          </Paper>
        </Grid>
      </Grid>
    </FeaturePageTemplate>
  );
};

export default VolumeConverter;

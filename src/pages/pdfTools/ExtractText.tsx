import React, { useState } from 'react';
import { Typography, Button, Box, Alert, TextField } from '@mui/material';
import { CloudUpload as UploadIcon } from '@mui/icons-material';
import { PDFDocument, PDFTextField } from 'pdf-lib';
import FeaturePageTemplate from '@components/FeaturePageTemplate';
import Notification from '@components/Notification';

const ExtractText = () => {
  const [selectedFile, setSelectedFile] = useState<File | null>(null);
  const [error, setError] = useState('');
  const [processing, setProcessing] = useState(false);
  const [extractedText, setExtractedText] = useState('');
  const [notification, setNotification] = useState({
    open: false,
    message: '',
  });

  const handleFileUpload = async (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const file = event.target.files?.[0];
    if (file) {
      if (file.type === 'application/pdf') {
        setSelectedFile(file);
        setError('');
      } else {
        setError('Please upload a valid PDF file');
      }
    }
  };

  const handleExtract = async () => {
    if (!selectedFile) return;

    try {
      setProcessing(true);
      setError('');

      const fileData = await selectedFile.arrayBuffer();
      const pdfDoc = await PDFDocument.load(fileData);
      const pages = pdfDoc.getPages();

      let text = '';
      pages.forEach((page) => {
        const content = page.doc.getForm().getFields();
        content.forEach((field) => {
          if (field instanceof PDFTextField) {
            const fieldText = field.getText();
            if (fieldText) {
              text += fieldText + '\n';
            }
          }
        });
      });

      setExtractedText(text || 'No text content found in this PDF');
      setNotification({
        open: true,
        message: 'Text extracted successfully!',
      });
    } catch (err) {
      console.error('Extraction error:', err);
      setError('Failed to extract text. Please try again.');
    } finally {
      setProcessing(false);
    }
  };

  const handleSaveText = () => {
    if (!extractedText) return;

    const blob = new Blob([extractedText], { type: 'text/plain' });
    const url = URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.href = url;
    link.download = `extracted_text_${selectedFile?.name.replace(
      '.pdf',
      ''
    )}.txt`;
    link.click();
    URL.revokeObjectURL(url);
  };

  return (
    <FeaturePageTemplate
      title="Extract Text"
      description="Extract text content from your PDF files"
    >
      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <Box className="space-y-6">
        <input
          type="file"
          accept=".pdf"
          onChange={handleFileUpload}
          style={{ display: 'none' }}
          id="pdf-upload"
        />
        <label htmlFor="pdf-upload">
          <Button
            variant="outlined"
            component="span"
            startIcon={<UploadIcon />}
            fullWidth
          >
            Upload PDF
          </Button>
        </label>

        {selectedFile && (
          <>
            <Typography variant="body2">
              Selected file: {selectedFile.name}
            </Typography>

            <Button
              variant="contained"
              onClick={handleExtract}
              disabled={processing}
              fullWidth
            >
              {processing ? 'Extracting...' : 'Extract Text'}
            </Button>

            {extractedText && (
              <>
                <TextField
                  multiline
                  rows={10}
                  fullWidth
                  value={extractedText}
                  InputProps={{ readOnly: true }}
                />
                <Button variant="outlined" onClick={handleSaveText} fullWidth>
                  Save as Text File
                </Button>
              </>
            )}
          </>
        )}
      </Box>

      <Notification
        open={notification.open}
        message={notification.message}
        onClose={() => setNotification({ ...notification, open: false })}
      />
    </FeaturePageTemplate>
  );
};

export default ExtractText;

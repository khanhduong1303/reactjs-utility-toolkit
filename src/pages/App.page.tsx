import React from 'react';
import { HashRouter } from 'react-router-dom';
import { ThemeProvider } from '@mui/material';
import { createTheme } from '@mui/material/styles';
import MainLayout from '../components/Layout/MainLayout';
import AppRoutes from '../routes';

const theme = createTheme({
  // Add your theme configuration here
});

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <HashRouter>
        <MainLayout>
          <AppRoutes />
        </MainLayout>
      </HashRouter>
    </ThemeProvider>
  );
};

export default App;

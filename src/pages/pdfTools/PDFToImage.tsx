import React, { useState } from 'react';
import {
  Typography,
  Grid,
  Button,
  Box,
  Alert,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  SelectChangeEvent,
} from '@mui/material';
import { CloudUpload as UploadIcon } from '@mui/icons-material';
import { PDFDocument } from 'pdf-lib';
import * as pdfjs from 'pdfjs-dist';
import FeaturePageTemplate from '@components/FeaturePageTemplate';

// Initialize PDF.js worker
pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.min.js`;

const PDFToImage = () => {
  const [selectedFile, setSelectedFile] = useState<File | null>(null);
  const [error, setError] = useState('');
  const [converting, setConverting] = useState(false);
  const [imageFormat, setImageFormat] = useState('png');
  const [totalPages, setTotalPages] = useState(0);

  const handleFileUpload = async (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const file = event.target.files?.[0];
    if (file) {
      if (file.type === 'application/pdf') {
        try {
          const fileData = await file.arrayBuffer();
          const pdfDoc = await PDFDocument.load(fileData);
          setTotalPages(pdfDoc.getPageCount());
          setSelectedFile(file);
          setError('');
        } catch (err) {
          setError('Failed to load PDF. Please try again.');
          console.error('Loading error:', err);
        }
      } else {
        setError('Please upload a valid PDF file');
      }
    }
  };

  const handleFormatChange = (event: SelectChangeEvent) => {
    setImageFormat(event.target.value);
  };

  const convertToImage = async () => {
    if (!selectedFile) return;

    try {
      setConverting(true);
      setError('');

      const fileData = await selectedFile.arrayBuffer();
      const pdf = await pdfjs.getDocument({ data: fileData }).promise;

      for (let i = 1; i <= pdf.numPages; i++) {
        const page = await pdf.getPage(i);
        const viewport = page.getViewport({ scale: 2.0 });

        const canvas = document.createElement('canvas');
        canvas.width = viewport.width;
        canvas.height = viewport.height;

        const context = canvas.getContext('2d');
        if (!context) {
          throw new Error('Could not get canvas context');
        }

        await page.render({
          canvasContext: context,
          viewport: viewport,
        }).promise;

        // Convert canvas to image and download
        const imageData = canvas.toDataURL(`image/${imageFormat}`);
        const link = document.createElement('a');
        link.href = imageData;
        link.download = `page_${i}.${imageFormat}`;
        link.click();
      }
    } catch (err) {
      setError('Failed to convert PDF to images. Please try again.');
      console.error('Conversion error:', err);
    } finally {
      setConverting(false);
    }
  };

  return (
    <FeaturePageTemplate
      title="Convert PDF to Images"
      description="Convert each page of your PDF to high-quality images"
    >
      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <Grid container spacing={4} justifyContent="center">
        <Grid item xs={12} md={6}>
          <Box className="space-y-6">
            <input
              type="file"
              accept=".pdf"
              onChange={handleFileUpload}
              className="hidden"
              id="pdf-upload"
            />
            <label htmlFor="pdf-upload">
              <Button
                variant="outlined"
                component="span"
                startIcon={<UploadIcon />}
                fullWidth
              >
                Upload PDF
              </Button>
            </label>

            {selectedFile && (
              <>
                <Typography variant="body2">
                  Total pages: {totalPages}
                </Typography>

                <FormControl fullWidth>
                  <InputLabel>Image Format</InputLabel>
                  <Select
                    value={imageFormat}
                    label="Image Format"
                    onChange={handleFormatChange}
                  >
                    <MenuItem value="png">PNG</MenuItem>
                    <MenuItem value="jpeg">JPEG</MenuItem>
                  </Select>
                </FormControl>

                <Button
                  variant="contained"
                  onClick={convertToImage}
                  fullWidth
                  disabled={converting}
                >
                  {converting ? 'Converting...' : 'Convert to Images'}
                </Button>
              </>
            )}
          </Box>
        </Grid>
      </Grid>
    </FeaturePageTemplate>
  );
};

export default PDFToImage;

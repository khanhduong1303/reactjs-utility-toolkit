import React from 'react';
import { Typography, Grid, Paper } from '@mui/material';
import { Link } from 'react-router-dom';
import {
  PhotoSizeSelectActual as PhotoSizeSelectActualOutlined,
  Transform as TransformOutlined,
  Edit as EditOutlined,
  Compress as CompressOutlined,
  Image as ImageOutlined,
  AutoFixHigh as AutoFixHighOutlined,
  Crop as CropOutlined,
  RotateRight as RotateRightOutlined,
  ColorLens as ColorLensOutlined,
  Filter as FilterOutlined,
  AddPhotoAlternate as AddPhotoAlternateOutlined,
  Collections as CollectionsOutlined,
} from '@mui/icons-material';
import FeaturePageTemplate from '@components/FeaturePageTemplate';

const tools = [
  {
    title: 'Resize Image',
    description: 'Resize and scale images',
    path: '/image-tools/resize',
    icon: <PhotoSizeSelectActualOutlined fontSize="large" />,
  },
  {
    title: 'Convert Image',
    description: 'Convert between image formats',
    path: '/image-tools/convert',
    icon: <TransformOutlined fontSize="large" />,
  },
  {
    title: 'Edit Image',
    description: 'Crop, rotate, and adjust images',
    path: '/image-tools/edit',
    icon: <EditOutlined fontSize="large" />,
  },
  {
    title: 'Compress Image',
    description: 'Reduce image file size',
    path: '/image-tools/compress',
    icon: <CompressOutlined fontSize="large" />,
  },
  {
    title: 'HEIC to JPG',
    description: 'Convert HEIC images to JPG',
    path: '/image-tools/heic-to-jpg',
    icon: <ImageOutlined fontSize="large" />,
  },
  {
    title: 'Image Effects',
    description: 'Apply filters and effects',
    path: '/image-tools/effects',
    icon: <AutoFixHighOutlined fontSize="large" />,
  },
  {
    title: 'Crop Image',
    description: 'Crop and resize image areas',
    path: '/image-tools/crop',
    icon: <CropOutlined fontSize="large" />,
  },
  {
    title: 'Rotate Image',
    description: 'Rotate and flip images',
    path: '/image-tools/rotate',
    icon: <RotateRightOutlined fontSize="large" />,
  },
  {
    title: 'Color Adjust',
    description: 'Adjust image colors and tones',
    path: '/image-tools/color',
    icon: <ColorLensOutlined fontSize="large" />,
  },
  {
    title: 'Apply Filters',
    description: 'Add artistic filters to images',
    path: '/image-tools/filters',
    icon: <FilterOutlined fontSize="large" />,
  },
  {
    title: 'Add Watermark',
    description: 'Add text or image watermarks',
    path: '/image-tools/watermark',
    icon: <AddPhotoAlternateOutlined fontSize="large" />,
  },
  {
    title: 'Batch Process',
    description: 'Process multiple images at once',
    path: '/image-tools/batch',
    icon: <CollectionsOutlined fontSize="large" />,
  },
];

const ImageTools = () => {
  return (
    <FeaturePageTemplate
      title="Image Tools"
      description="Free online tools for image processing and conversion"
    >
      <Grid container spacing={4}>
        {tools.map((tool) => (
          <Grid item xs={12} sm={6} md={4} key={tool.path}>
            <Link to={tool.path} className="no-underline">
              <Paper
                elevation={1}
                className="p-6 h-full transition-all hover:shadow-md"
              >
                <div className="flex flex-col items-center text-center space-y-4">
                  {tool.icon}
                  <Typography variant="h6" className="font-bold">
                    {tool.title}
                  </Typography>
                  <Typography className="text-secondary-600">
                    {tool.description}
                  </Typography>
                </div>
              </Paper>
            </Link>
          </Grid>
        ))}
      </Grid>
    </FeaturePageTemplate>
  );
};

export default ImageTools;

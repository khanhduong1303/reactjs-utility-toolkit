import { makeAutoObservable } from 'mobx';
import { RootStore } from './Stores';
import { MENU } from '@utils/Constants';

export class SettingStore {
  rootStore: RootStore;
  activeMenu: string = MENU.HOME;

  constructor(rootStore: RootStore) {
    makeAutoObservable(this);
    this.rootStore = rootStore;
  }

  setActiveMenu = (menu: string) => {
    this.activeMenu = menu;
  };
}

export default SettingStore;

import React from 'react';
import AdSense from './index';

const FooterBannerAd: React.FC = () => (
  <AdSense slot="5962811094" className="mt-8" />
);

export default FooterBannerAd;

import React from 'react';
import { Typography, Grid, Paper } from '@mui/material';
import { Link } from 'react-router-dom';
import {
  Calculate,
  Schedule,
  Translate,
  Code,
  Build,
} from '@mui/icons-material';
import FeaturePageTemplate from '@components/FeaturePageTemplate';

interface Tool {
  title: string;
  description: string;
  icon: React.ReactNode;
  path: string;
}

const tools: Tool[] = [
  {
    title: 'Unit Converter',
    description: 'Convert between different units of measurement',
    icon: <Calculate className="text-4xl" />,
    path: '/unit-converter',
  },
  {
    title: 'Time Converter',
    description: 'Convert between different time zones',
    icon: <Schedule className="text-4xl" />,
    path: '/time-converter',
  },
  {
    title: 'Text Translator',
    description: 'Translate text between languages',
    icon: <Translate className="text-4xl" />,
    path: '/text-translator',
  },
  {
    title: 'Code Formatter',
    description: 'Format and beautify code',
    icon: <Code className="text-4xl" />,
    path: '/code-formatter',
  },
  {
    title: 'Other Utilities',
    description: 'Additional useful tools',
    icon: <Build className="text-4xl" />,
    path: '/utilities',
  },
];

const OtherTools = () => {
  return (
    <FeaturePageTemplate
      title="Other Tools"
      description="Additional utility tools for various tasks"
    >
      <Grid container spacing={4}>
        {tools.map((tool) => (
          <Grid item xs={12} sm={6} md={4} key={tool.path}>
            <Link to={tool.path} className="no-underline">
              <Paper
                elevation={1}
                className="p-6 h-full transition-all hover:shadow-md"
              >
                <div className="flex flex-col items-center text-center space-y-4">
                  {tool.icon}
                  <Typography variant="h6" className="font-bold">
                    {tool.title}
                  </Typography>
                  <Typography className="text-secondary-600">
                    {tool.description}
                  </Typography>
                </div>
              </Paper>
            </Link>
          </Grid>
        ))}
      </Grid>
    </FeaturePageTemplate>
  );
};

export default OtherTools;

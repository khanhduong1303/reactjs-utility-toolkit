import React, { useState } from 'react';
import {
  Typography,
  Button,
  Box,
  Alert,
  Grid,
  Card,
  CardContent,
  CardMedia,
  CardActionArea,
  Snackbar,
} from '@mui/material';
import { CloudUpload as UploadIcon } from '@mui/icons-material';
import FeaturePageTemplate from '@components/FeaturePageTemplate';

const FILTERS = [
  { id: 'grayscale', name: 'Grayscale', filter: 'grayscale(100%)' },
  { id: 'sepia', name: 'Sepia', filter: 'sepia(100%)' },
  { id: 'invert', name: 'Invert', filter: 'invert(100%)' },
  { id: 'blur', name: 'Blur', filter: 'blur(5px)' },
  { id: 'vintage', name: 'Vintage', filter: 'sepia(50%) contrast(150%)' },
  {
    id: 'dramatic',
    name: 'Dramatic',
    filter: 'contrast(150%) brightness(90%)',
  },
  { id: 'cool', name: 'Cool', filter: 'saturate(80%) hue-rotate(20deg)' },
  { id: 'warm', name: 'Warm', filter: 'saturate(120%) hue-rotate(-10deg)' },
];

const ApplyFilters = () => {
  const [selectedFile, setSelectedFile] = useState<File | null>(null);
  const [error, setError] = useState('');
  const [processing, setProcessing] = useState(false);
  const [selectedFilter, setSelectedFilter] = useState('');
  const [open, setOpen] = useState(false);
  const [message, setMessage] = useState('');

  const handleFileUpload = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0];
    if (file) {
      if (file.type.startsWith('image/')) {
        setSelectedFile(file);
        setError('');
      } else {
        setError('Please upload a valid image file');
      }
    }
  };

  const handleApply = async () => {
    if (!selectedFilter) {
      setError('Please select a filter');
      return;
    }

    setProcessing(true);
    await new Promise((resolve) => setTimeout(resolve, 2000));
    setProcessing(false);
    setMessage(
      'This is a demo version. Filter application is not implemented yet.'
    );
    setOpen(true);
  };

  return (
    <FeaturePageTemplate
      title="Apply Filters"
      description="Enhance your images with beautiful filters"
    >
      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <Box className="space-y-6">
        <input
          type="file"
          accept="image/*"
          onChange={handleFileUpload}
          className="hidden"
          id="image-upload"
        />
        <label htmlFor="image-upload">
          <Button
            variant="outlined"
            component="span"
            startIcon={<UploadIcon />}
            fullWidth
          >
            Upload Image
          </Button>
        </label>

        {selectedFile && (
          <>
            <Typography variant="body2">
              Selected file: {selectedFile.name}
            </Typography>

            <Box>
              <Typography gutterBottom>Preview</Typography>
              <Box
                sx={{
                  width: '100%',
                  height: 300,
                  bgcolor: 'grey.100',
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  filter:
                    FILTERS.find((f) => f.id === selectedFilter)?.filter ||
                    'none',
                }}
              >
                <Typography color="text.secondary">
                  Preview will be shown here
                </Typography>
              </Box>
            </Box>

            <Box>
              <Typography gutterBottom>Available Filters</Typography>
              <Grid container spacing={2}>
                {FILTERS.map((filter) => (
                  <Grid item xs={6} sm={4} md={3} key={filter.id}>
                    <Card
                      variant={
                        selectedFilter === filter.id ? 'elevation' : 'outlined'
                      }
                      elevation={selectedFilter === filter.id ? 8 : 0}
                    >
                      <CardActionArea
                        onClick={() => setSelectedFilter(filter.id)}
                      >
                        <CardMedia
                          component="div"
                          sx={{
                            height: 100,
                            bgcolor: 'grey.200',
                            filter: filter.filter,
                          }}
                        />
                        <CardContent>
                          <Typography
                            variant="body2"
                            align="center"
                            color={
                              selectedFilter === filter.id
                                ? 'primary'
                                : 'textPrimary'
                            }
                          >
                            {filter.name}
                          </Typography>
                        </CardContent>
                      </CardActionArea>
                    </Card>
                  </Grid>
                ))}
              </Grid>
            </Box>

            <Button
              variant="contained"
              onClick={handleApply}
              fullWidth
              disabled={processing || !selectedFilter}
            >
              {processing ? 'Processing Image...' : 'Apply Filter'}
            </Button>
          </>
        )}
      </Box>

      <Snackbar
        open={open}
        autoHideDuration={6000}
        onClose={() => setOpen(false)}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
      >
        <Alert onClose={() => setOpen(false)} severity="info">
          {message}
        </Alert>
      </Snackbar>
    </FeaturePageTemplate>
  );
};

export default ApplyFilters;

import React from 'react';
import AdSense from '.';
import { AD_UNITS } from '../../configs/adsense.config';

interface AdSenseComponentProps {
  onLoad?: () => void;
}

export const BannerAd: React.FC<AdSenseComponentProps> = ({ onLoad }) => {
  return <AdSense {...AD_UNITS.banner} onLoad={onLoad} />;
};

export const InArticleAd: React.FC<AdSenseComponentProps> = ({ onLoad }) => {
  return <AdSense {...AD_UNITS.inArticle} onLoad={onLoad} />;
};

export const InFeedAd: React.FC<AdSenseComponentProps> = ({ onLoad }) => {
  return <AdSense {...AD_UNITS.inFeed} onLoad={onLoad} />;
};

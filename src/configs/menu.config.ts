import { SvgIconComponent } from '@mui/icons-material';
import {
  PictureAsPdfOutlined,
  ImageOutlined,
  VideoFileOutlined,
  TranslateOutlined,
  AccessTimeOutlined,
  CodeOutlined,
  CompressOutlined,
  StraightenOutlined,
  ColorLensOutlined,
  MoreHorizOutlined,
  TextFieldsOutlined,
  QrCodeOutlined,
  DocumentScannerOutlined,
  LinkOutlined,
  FileCopyOutlined,
  CallSplitOutlined,
  RotateRightOutlined,
  MergeTypeOutlined,
  EditOutlined,
  WaterDropOutlined,
  LockOutlined,
  DeleteOutlined,
  PhotoSizeSelectActualOutlined,
  TransformOutlined,
  AutoFixHighOutlined,
  CropOutlined,
  FilterOutlined,
  AddPhotoAlternateOutlined,
  CollectionsOutlined,
  ContentCutOutlined,
  FormatSizeOutlined,
  SpellcheckOutlined,
  FormatQuoteOutlined,
  CompareArrowsOutlined,
  ThermostatOutlined,
  SpeedOutlined,
} from '@mui/icons-material';

export interface MenuItem {
  name: string;
  path: string;
  icon: SvgIconComponent;
  description?: string;
  keywords?: string[];
  subItems?: MenuItem[];
}

export interface MenuSection {
  title: string;
  items: MenuItem[];
}

export const menuConfig: MenuSection[] = [
  {
    title: 'Popular Tools',
    items: [
      {
        name: 'PDF Tools',
        path: '/pdf-tools',
        icon: PictureAsPdfOutlined,
        description: 'Work with PDF files',
        subItems: [
          {
            name: 'Compress PDF',
            path: '/pdf-tools/compress',
            icon: CompressOutlined,
            description: 'Reduce PDF file size',
          },
          {
            name: 'Extract Pages',
            path: '/pdf-tools/extract',
            icon: FileCopyOutlined,
            description: 'Extract pages from PDF',
          },
          {
            name: 'Split PDF',
            path: '/pdf-tools/split',
            icon: CallSplitOutlined,
            description: 'Split PDF into multiple files',
          },
          {
            name: 'Rotate PDF',
            path: '/pdf-tools/rotate',
            icon: RotateRightOutlined,
            description: 'Rotate PDF pages',
          },
          {
            name: 'PDF to Image',
            path: '/pdf-tools/to-image',
            icon: ImageOutlined,
            description: 'Convert PDF to images',
          },
          {
            name: 'Merge PDF',
            path: '/pdf-tools/merge',
            icon: MergeTypeOutlined,
            description: 'Merge multiple PDFs',
          },
          {
            name: 'Edit Metadata',
            path: '/pdf-tools/metadata',
            icon: EditOutlined,
            description: 'Edit PDF metadata',
          },
          {
            name: 'Add Watermark',
            path: '/pdf-tools/watermark',
            icon: WaterDropOutlined,
            description: 'Add watermark to PDF',
          },
          {
            name: 'Convert to PDF',
            path: '/pdf-tools/convert',
            icon: PictureAsPdfOutlined,
            description: 'Convert to PDF',
          },
          {
            name: 'Extract Text',
            path: '/pdf-tools/extract-text',
            icon: TextFieldsOutlined,
            description: 'Extract text from PDF',
          },
          {
            name: 'Protect PDF',
            path: '/pdf-tools/protect',
            icon: LockOutlined,
            description: 'Password protect PDF',
          },
          {
            name: 'Remove Pages',
            path: '/pdf-tools/remove-pages',
            icon: DeleteOutlined,
            description: 'Remove PDF pages',
          },
        ],
      },
      {
        name: 'Image Tools',
        path: '/image-tools',
        icon: ImageOutlined,
        description: 'Work with images',
        subItems: [
          {
            name: 'Resize Image',
            path: '/image-tools/resize',
            icon: PhotoSizeSelectActualOutlined,
            description: 'Resize images',
          },
          {
            name: 'Convert Image',
            path: '/image-tools/convert',
            icon: TransformOutlined,
            description: 'Convert image formats',
          },
          {
            name: 'Edit Image',
            path: '/image-tools/edit',
            icon: EditOutlined,
            description: 'Edit images',
          },
          {
            name: 'Compress Image',
            path: '/image-tools/compress',
            icon: CompressOutlined,
            description: 'Compress images',
          },
          {
            name: 'HEIC to JPG',
            path: '/image-tools/heic-to-jpg',
            icon: ImageOutlined,
            description: 'Convert HEIC to JPG',
          },
          {
            name: 'Image Effects',
            path: '/image-tools/effects',
            icon: AutoFixHighOutlined,
            description: 'Add image effects',
          },
          {
            name: 'Crop Image',
            path: '/image-tools/crop',
            icon: CropOutlined,
            description: 'Crop images',
          },
          {
            name: 'Rotate Image',
            path: '/image-tools/rotate',
            icon: RotateRightOutlined,
            description: 'Rotate images',
          },
          {
            name: 'Color Adjust',
            path: '/image-tools/color',
            icon: ColorLensOutlined,
            description: 'Adjust colors',
          },
          {
            name: 'Apply Filters',
            path: '/image-tools/filters',
            icon: FilterOutlined,
            description: 'Apply image filters',
          },
          {
            name: 'Add Watermark',
            path: '/image-tools/watermark',
            icon: AddPhotoAlternateOutlined,
            description: 'Add watermarks',
          },
          {
            name: 'Batch Process',
            path: '/image-tools/batch',
            icon: CollectionsOutlined,
            description: 'Process multiple images',
          },
        ],
      },
      {
        name: 'Video Tools',
        path: '/video-tools',
        icon: VideoFileOutlined,
        description: 'Work with videos',
        subItems: [
          {
            name: 'Convert Video',
            path: '/video-tools/convert',
            icon: TransformOutlined,
            description: 'Convert video formats',
          },
          {
            name: 'Trim Video',
            path: '/video-tools/trim',
            icon: ContentCutOutlined,
            description: 'Trim videos',
          },
        ],
      },
    ],
  },
  {
    title: 'Text & Code Tools',
    items: [
      {
        name: 'Text Tools',
        path: '/text-tools',
        icon: TextFieldsOutlined,
        description: 'Work with text',
        subItems: [
          {
            name: 'Case Converter',
            path: '/text-tools/case-converter',
            icon: TextFieldsOutlined,
            description: 'Convert text case',
          },
          {
            name: 'Text Formatter',
            path: '/text-tools/formatter',
            icon: FormatSizeOutlined,
            description: 'Format text',
          },
          {
            name: 'Spell Checker',
            path: '/text-tools/spell-check',
            icon: SpellcheckOutlined,
            description: 'Check spelling',
          },
          {
            name: 'Text Translator',
            path: '/text-tools/translator',
            icon: TranslateOutlined,
            description: 'Translate text',
          },
          {
            name: 'Quote Generator',
            path: '/text-tools/quote-generator',
            icon: FormatQuoteOutlined,
            description: 'Generate quotes',
          },
          {
            name: 'Diff Compare',
            path: '/text-tools/diff-compare',
            icon: CompareArrowsOutlined,
            description: 'Compare texts',
          },
        ],
      },
      {
        name: 'Code Formatter',
        path: '/code-formatter',
        icon: CodeOutlined,
        description: 'Format code',
      },
      {
        name: 'URL Tools',
        path: '/url-tools',
        icon: LinkOutlined,
        description: 'Work with URLs',
      },
    ],
  },
  {
    title: 'Other Tools',
    items: [
      {
        name: 'QR Tools',
        path: '/qr-tools',
        icon: QrCodeOutlined,
        description: 'Generate and scan QR codes',
        subItems: [
          {
            name: 'Generate QR',
            path: '/qr-tools/generate',
            icon: QrCodeOutlined,
            description: 'Generate QR codes',
          },
          {
            name: 'Scan QR',
            path: '/qr-tools/scan',
            icon: DocumentScannerOutlined,
            description: 'Scan QR codes',
          },
        ],
      },
      {
        name: 'Translator',
        path: '/translator',
        icon: TranslateOutlined,
        description: 'Translate text',
      },
      {
        name: 'Time Converter',
        path: '/time-converter',
        icon: AccessTimeOutlined,
        description: 'Convert time zones',
      },
      {
        name: 'Unit Converter',
        path: '/unit-converter',
        icon: StraightenOutlined,
        description: 'Convert units',
        subItems: [
          {
            name: 'Length',
            path: '/unit-converter/length',
            icon: StraightenOutlined,
            description: 'Convert length units',
          },
          {
            name: 'Area',
            path: '/unit-converter/area',
            icon: PhotoSizeSelectActualOutlined,
            description: 'Convert area units',
          },
          {
            name: 'Volume',
            path: '/unit-converter/volume',
            icon: WaterDropOutlined,
            description: 'Convert volume units',
          },
          {
            name: 'Weight',
            path: '/unit-converter/weight',
            icon: StraightenOutlined,
            description: 'Convert weight units',
          },
          {
            name: 'Temperature',
            path: '/unit-converter/temperature',
            icon: ThermostatOutlined,
            description: 'Convert temperature units',
          },
          {
            name: 'Speed',
            path: '/unit-converter/speed',
            icon: SpeedOutlined,
            description: 'Convert speed units',
          },
        ],
      },
      {
        name: 'Color Picker',
        path: '/color-picker',
        icon: ColorLensOutlined,
        description: 'Pick and convert colors',
      },
      {
        name: 'Other Tools',
        path: '/other-tools',
        icon: MoreHorizOutlined,
        description: 'More useful tools',
      },
    ],
  },
];

import React, { useState } from 'react';
import {
  Typography,
  Grid,
  Box,
  Button,
  Alert,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  TextField,
  FormControlLabel,
  Switch,
  CircularProgress,
} from '@mui/material';
import { CloudUpload as UploadIcon } from '@mui/icons-material';
import JSZip from 'jszip';
import FeaturePageTemplate from '@components/FeaturePageTemplate';
import Notification from '@components/Notification';

interface JSZipCustomOptions extends JSZip.JSZipFileOptions {
  password?: string;
}

interface JSZipCustomGenerateOptions
  extends JSZip.JSZipGeneratorOptions<'blob'> {
  password?: string;
}

const FileCompressor = () => {
  const [selectedFiles, setSelectedFiles] = useState<File[]>([]);
  const [error, setError] = useState('');
  const [processing, setProcessing] = useState(false);
  const [mode, setMode] = useState<'compress' | 'decompress'>('compress');
  const [password, setPassword] = useState('');
  const [usePassword, setUsePassword] = useState(false);
  const [notification, setNotification] = useState({
    open: false,
    message: '',
  });

  const handleFileUpload = (event: React.ChangeEvent<HTMLInputElement>) => {
    const files = Array.from(event.target.files || []);
    setSelectedFiles(files);
    setError('');
  };

  const compressFiles = async () => {
    if (selectedFiles.length === 0) {
      setError('Please select files to compress');
      return;
    }

    try {
      setProcessing(true);
      setError('');

      const zip = new JSZip();

      for (const file of selectedFiles) {
        const fileData = await file.arrayBuffer();
        const options: JSZipCustomOptions = usePassword ? { password } : {};
        zip.file(file.name, fileData, options);
      }

      const options: JSZipCustomGenerateOptions = {
        type: 'blob',
        compression: 'DEFLATE',
        compressionOptions: { level: 9 },
      };

      if (usePassword) {
        options.password = password;
      }

      const content = await zip.generateAsync(options);

      const url = URL.createObjectURL(content);
      const link = document.createElement('a');
      link.href = url;
      link.download = 'compressed_files.zip';
      link.click();
      URL.revokeObjectURL(url);
    } catch (err) {
      console.error('Compression error:', err);
      setError('Failed to compress files. Please try again.');
    } finally {
      setProcessing(false);
    }
  };

  const decompressFile = async () => {
    if (selectedFiles.length !== 1) {
      setError('Please select one zip file to decompress');
      return;
    }

    try {
      setProcessing(true);
      setError('');

      const fileData = await selectedFiles[0].arrayBuffer();
      const zip = new JSZip();

      await zip.loadAsync(fileData);

      const zipFiles = zip.files;
      for (const fileName in zipFiles) {
        const zipFile = zipFiles[fileName];
        if (!zipFile.dir) {
          const content = await zipFile.async('blob');
          const url = URL.createObjectURL(content);
          const link = document.createElement('a');
          link.href = url;
          link.download = fileName;
          link.click();
          URL.revokeObjectURL(url);
        }
      }
    } catch (error: unknown) {
      console.error('Decompression error:', error);
      if (
        error instanceof Error &&
        error.message.toLowerCase().includes('password')
      ) {
        setError('Incorrect password. Please check and try again.');
      } else {
        setError(
          'Failed to decompress file. Please check if the file is valid.'
        );
      }
    } finally {
      setProcessing(false);
    }
  };

  return (
    <FeaturePageTemplate
      title="File Compressor"
      description="Compress multiple files into a zip archive or decompress zip files"
    >
      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <Grid container spacing={4}>
        <Grid item xs={12} md={6}>
          <Box className="p-6">
            <div className="space-y-6">
              <FormControl fullWidth>
                <InputLabel>Mode</InputLabel>
                <Select
                  value={mode}
                  label="Mode"
                  onChange={(e) =>
                    setMode(e.target.value as 'compress' | 'decompress')
                  }
                >
                  <MenuItem value="compress">Compress Files</MenuItem>
                  <MenuItem value="decompress">Decompress ZIP</MenuItem>
                </Select>
              </FormControl>

              <input
                type="file"
                onChange={handleFileUpload}
                className="hidden"
                id="file-upload"
                multiple={mode === 'compress'}
                accept={mode === 'decompress' ? '.zip' : undefined}
              />
              <label htmlFor="file-upload">
                <Button
                  variant="outlined"
                  component="span"
                  startIcon={<UploadIcon />}
                  fullWidth
                >
                  {mode === 'compress' ? 'Select Files' : 'Select ZIP File'}
                </Button>
              </label>

              {selectedFiles.length > 0 && (
                <Typography variant="body2" className="break-all">
                  Selected: {selectedFiles.map((f) => f.name).join(', ')}
                </Typography>
              )}

              <FormControlLabel
                control={
                  <Switch
                    checked={usePassword}
                    onChange={(e) => setUsePassword(e.target.checked)}
                  />
                }
                label={
                  mode === 'compress'
                    ? 'Protect with password'
                    : 'File is password protected'
                }
              />

              {usePassword && (
                <TextField
                  fullWidth
                  type="password"
                  label="Password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  error={usePassword && !password}
                  helperText={
                    usePassword && !password ? 'Password is required' : ''
                  }
                />
              )}

              <Button
                variant="contained"
                onClick={mode === 'compress' ? compressFiles : decompressFile}
                disabled={
                  processing ||
                  selectedFiles.length === 0 ||
                  (usePassword && !password)
                }
                fullWidth
              >
                {processing ? (
                  <CircularProgress size={24} className="text-white" />
                ) : mode === 'compress' ? (
                  'Compress Files'
                ) : (
                  'Decompress ZIP'
                )}
              </Button>
            </div>
          </Box>
        </Grid>
      </Grid>

      <Notification
        open={notification.open}
        message={notification.message}
        onClose={() => setNotification({ ...notification, open: false })}
      />
    </FeaturePageTemplate>
  );
};

export default FileCompressor;

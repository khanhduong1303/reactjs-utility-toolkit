import React, { useState } from 'react';
import { Typography, Grid, Button, Box, Slider, Alert } from '@mui/material';
import { CloudUpload as UploadIcon } from '@mui/icons-material';
import imageCompression from 'browser-image-compression';
import FeaturePageTemplate from '@components/FeaturePageTemplate';

const CompressImage = () => {
  const [selectedImage, setSelectedImage] = useState<File | null>(null);
  const [previewUrl, setPreviewUrl] = useState<string>('');
  const [quality, setQuality] = useState<number>(75);
  const [error, setError] = useState('');
  const [compressing, setCompressing] = useState(false);
  const [originalSize, setOriginalSize] = useState<string>('');
  const [compressedSize, setCompressedSize] = useState<string>('');

  const handleImageUpload = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0];
    if (file) {
      if (file.type.startsWith('image/')) {
        setSelectedImage(file);
        setError('');
        setOriginalSize((file.size / 1024 / 1024).toFixed(2) + ' MB');
        setCompressedSize('');

        // Create preview
        const reader = new FileReader();
        reader.onloadend = () => {
          setPreviewUrl(reader.result as string);
        };
        reader.readAsDataURL(file);
      } else {
        setError('Please upload a valid image file');
      }
    }
  };

  const handleCompress = async () => {
    if (!selectedImage) return;

    try {
      setCompressing(true);
      setError('');

      const options = {
        maxSizeMB: 1,
        maxWidthOrHeight: 1920,
        useWebWorker: true,
        initialQuality: quality / 100,
      };

      const compressedFile = await imageCompression(selectedImage, options);
      setCompressedSize((compressedFile.size / 1024 / 1024).toFixed(2) + ' MB');

      const url = URL.createObjectURL(compressedFile);
      const link = document.createElement('a');
      link.href = url;
      link.download = `compressed_${selectedImage.name}`;
      link.click();
      URL.revokeObjectURL(url);
    } catch (err) {
      setError('Failed to compress image. Please try again.');
      console.error('Compression error:', err);
    } finally {
      setCompressing(false);
    }
  };

  return (
    <FeaturePageTemplate
      title="Compress Image"
      description="Reduce image file size while maintaining quality"
    >
      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <Grid container spacing={4}>
        <Grid item xs={12} md={6}>
          <Box className="space-y-6">
            <div className="space-y-2">
              <Typography gutterBottom>Quality: {quality}%</Typography>
              <Slider
                value={quality}
                onChange={(_, value) => setQuality(value as number)}
                aria-labelledby="quality-slider"
                valueLabelDisplay="auto"
                min={1}
                max={100}
                disabled={!selectedImage}
              />
            </div>

            <input
              type="file"
              accept="image/*"
              onChange={handleImageUpload}
              className="hidden"
              id="image-upload"
            />
            <label htmlFor="image-upload">
              <Button
                variant="outlined"
                component="span"
                startIcon={<UploadIcon />}
                fullWidth
              >
                Upload Image
              </Button>
            </label>

            {selectedImage && (
              <>
                <div className="space-y-2">
                  <Typography variant="body2">
                    Original size: {originalSize}
                  </Typography>
                  {compressedSize && (
                    <Typography variant="body2">
                      Compressed size: {compressedSize}
                    </Typography>
                  )}
                </div>

                <Button
                  variant="contained"
                  onClick={handleCompress}
                  fullWidth
                  disabled={compressing}
                >
                  {compressing ? 'Compressing...' : 'Compress Image'}
                </Button>
              </>
            )}
          </Box>
        </Grid>

        <Grid item xs={12} md={6}>
          {previewUrl && (
            <Box className="bg-white p-4 rounded-lg">
              <Typography variant="h6" className="mb-4">
                Preview
              </Typography>
              <img
                src={previewUrl}
                alt="Preview"
                className="max-w-full h-auto rounded-lg"
              />
            </Box>
          )}
        </Grid>
      </Grid>
    </FeaturePageTemplate>
  );
};

export default CompressImage;

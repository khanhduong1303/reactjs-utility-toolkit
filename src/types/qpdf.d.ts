declare module 'qpdf' {
  interface EncryptOptions {
    password: string;
    keyLength?: number;
    r?: number;
    p?: number;
    encrypt?: boolean;
    useAes?: boolean;
    allowAll?: boolean;
    allowPrinting?: boolean;
    allowModifyContents?: boolean;
    allowCopy?: boolean;
    allowModifyAnnotations?: boolean;
    allowFillForms?: boolean;
    allowScreenReaders?: boolean;
    allowAssembly?: boolean;
    allowDegradedPrinting?: boolean;
    allowPrintingHighQuality?: boolean;
  }

  interface QPDF {
    encrypt(
      pdfBuffer: Uint8Array | Buffer,
      options: EncryptOptions
    ): Promise<Buffer>;
  }

  const qpdf: QPDF;
  export = qpdf;
}

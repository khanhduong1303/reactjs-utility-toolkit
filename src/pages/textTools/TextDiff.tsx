import React, { useState } from 'react';
import { Button, Box, Alert, TextField } from '@mui/material';
import FeaturePageTemplate from '@components/FeaturePageTemplate';
import Notification from '@components/Notification';

const TextDiff = () => {
  const [text1, setText1] = useState('');
  const [text2, setText2] = useState('');
  const [error, setError] = useState('');
  const [processing, setProcessing] = useState(false);
  const [notification, setNotification] = useState({
    open: false,
    message: '',
  });

  const handleCompare = async () => {
    if (!text1.trim() || !text2.trim()) {
      setError('Please enter text in both fields to compare');
      return;
    }

    setProcessing(true);
    await new Promise((resolve) => setTimeout(resolve, 2000));
    setProcessing(false);
    setNotification({
      open: true,
      message:
        'This is a demo version. Text comparison is not implemented yet.',
    });
  };

  return (
    <FeaturePageTemplate
      title="Text Difference"
      description="Compare two texts and find differences"
    >
      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <Box className="space-y-6">
        <TextField
          fullWidth
          multiline
          rows={8}
          label="First Text"
          value={text1}
          onChange={(e) => setText1(e.target.value)}
        />

        <TextField
          fullWidth
          multiline
          rows={8}
          label="Second Text"
          value={text2}
          onChange={(e) => setText2(e.target.value)}
        />

        <Button
          variant="contained"
          onClick={handleCompare}
          disabled={processing}
          fullWidth
        >
          {processing ? 'Comparing...' : 'Compare Texts'}
        </Button>
      </Box>

      <Notification
        open={notification.open}
        message={notification.message}
        onClose={() => setNotification({ ...notification, open: false })}
      />
    </FeaturePageTemplate>
  );
};

export default TextDiff;

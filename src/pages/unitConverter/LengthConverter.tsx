import React, { useState, useEffect } from 'react';
import {
  Typography,
  Grid,
  Paper,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from '@mui/material';
import FeaturePageTemplate from '@components/FeaturePageTemplate';

interface Unit {
  value: string;
  label: string;
  toBase: number; // conversion to meters
}

const LENGTH_UNITS: Unit[] = [
  { value: 'm', label: 'Meters (m)', toBase: 1 },
  { value: 'km', label: 'Kilometers (km)', toBase: 1000 },
  { value: 'cm', label: 'Centimeters (cm)', toBase: 0.01 },
  { value: 'mm', label: 'Millimeters (mm)', toBase: 0.001 },
  { value: 'in', label: 'Inches (in)', toBase: 0.0254 },
  { value: 'ft', label: 'Feet (ft)', toBase: 0.3048 },
  { value: 'yd', label: 'Yards (yd)', toBase: 0.9144 },
  { value: 'mi', label: 'Miles (mi)', toBase: 1609.344 },
];

const LengthConverter = () => {
  const [inputValue, setInputValue] = useState<string>('');
  const [inputUnit, setInputUnit] = useState<string>('m');
  const [outputUnit, setOutputUnit] = useState<string>('km');
  const [outputValue, setOutputValue] = useState<string>('');

  const convert = (value: string, from: string, to: string) => {
    const numValue = parseFloat(value);
    if (isNaN(numValue)) return '';

    const fromUnit = LENGTH_UNITS.find((u) => u.value === from);
    const toUnit = LENGTH_UNITS.find((u) => u.value === to);
    if (!fromUnit || !toUnit) return '';

    // Convert to base unit (meters) then to target unit
    const baseValue = numValue * fromUnit.toBase;
    const result = baseValue / toUnit.toBase;
    return result.toFixed(6);
  };

  useEffect(() => {
    const result = convert(inputValue, inputUnit, outputUnit);
    setOutputValue(result);
  }, [inputValue, inputUnit, outputUnit]);

  return (
    <FeaturePageTemplate
      title="Length Converter"
      description="Convert between different units of length"
    >
      <Grid container spacing={4} justifyContent="center">
        <Grid item xs={12} md={8}>
          <Paper elevation={1} className="p-6">
            <Grid container spacing={3}>
              <Grid item xs={12} md={6}>
                <FormControl fullWidth>
                  <InputLabel>From</InputLabel>
                  <Select
                    value={inputUnit}
                    label="From"
                    onChange={(e) => setInputUnit(e.target.value)}
                  >
                    {LENGTH_UNITS.map((unit) => (
                      <MenuItem key={unit.value} value={unit.value}>
                        {unit.label}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>

              <Grid item xs={12} md={6}>
                <FormControl fullWidth>
                  <InputLabel>To</InputLabel>
                  <Select
                    value={outputUnit}
                    label="To"
                    onChange={(e) => setOutputUnit(e.target.value)}
                  >
                    {LENGTH_UNITS.map((unit) => (
                      <MenuItem key={unit.value} value={unit.value}>
                        {unit.label}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>

              <Grid item xs={12} md={6}>
                <TextField
                  fullWidth
                  label="Enter Value"
                  type="number"
                  value={inputValue}
                  onChange={(e) => setInputValue(e.target.value)}
                  placeholder="Enter a number"
                />
              </Grid>

              <Grid item xs={12} md={6}>
                <TextField
                  fullWidth
                  label="Result"
                  value={outputValue}
                  InputProps={{ readOnly: true }}
                />
              </Grid>
            </Grid>

            <Typography className="mt-6 text-sm text-gray-600">
              Note: Results are rounded to 6 decimal places for precision.
            </Typography>
          </Paper>
        </Grid>
      </Grid>
    </FeaturePageTemplate>
  );
};

export default LengthConverter;

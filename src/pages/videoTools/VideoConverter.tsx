import React, { useState } from 'react';
import { Typography, Button, Alert } from '@mui/material';
import { CloudUpload as UploadIcon } from '@mui/icons-material';
import FeaturePageTemplate from '@components/FeaturePageTemplate';
import Notification from '@components/Notification';

const VideoConverter = () => {
  const [selectedFile, setSelectedFile] = useState<File | null>(null);
  const [error, setError] = useState('');
  const [processing, setProcessing] = useState(false);
  const [notification, setNotification] = useState({
    open: false,
    message: '',
  });

  const handleFileUpload = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0];
    if (file) {
      if (file.type.startsWith('video/')) {
        setSelectedFile(file);
        setError('');
      } else {
        setError('Please upload a valid video file');
      }
    }
  };

  const handleConvert = async () => {
    setProcessing(true);
    await new Promise((resolve) => setTimeout(resolve, 2000));
    setProcessing(false);
    setNotification({
      open: true,
      message:
        'This is a demo version. Video conversion is not implemented yet.',
    });
  };

  return (
    <FeaturePageTemplate
      title="Video Converter"
      description="Convert videos between different formats"
    >
      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <div className="space-y-6">
        <input
          type="file"
          accept="video/*"
          onChange={handleFileUpload}
          style={{ display: 'none' }}
          id="video-upload"
        />
        <label htmlFor="video-upload">
          <Button
            variant="outlined"
            component="span"
            startIcon={<UploadIcon />}
            fullWidth
          >
            Upload Video
          </Button>
        </label>

        {selectedFile && (
          <>
            <Typography variant="body2">
              Selected file: {selectedFile.name}
            </Typography>

            <Button
              variant="contained"
              onClick={handleConvert}
              disabled={processing}
              fullWidth
            >
              {processing ? 'Converting...' : 'Convert Video'}
            </Button>
          </>
        )}
      </div>

      <Notification
        open={notification.open}
        message={notification.message}
        onClose={() => setNotification({ ...notification, open: false })}
      />
    </FeaturePageTemplate>
  );
};

export default VideoConverter;

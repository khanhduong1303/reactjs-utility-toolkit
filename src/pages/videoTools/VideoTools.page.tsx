import React from 'react';
import { Typography, Grid, Paper } from '@mui/material';
import { Link } from 'react-router-dom';
import {
  Compress,
  Transform,
  ContentCut,
  Merge,
  VideoSettings,
  AudioFile,
} from '@mui/icons-material';
import FeaturePageTemplate from '@components/FeaturePageTemplate';

interface Tool {
  title: string;
  description: string;
  icon: React.ReactNode;
  path: string;
}

const tools: Tool[] = [
  {
    title: 'Compress Video',
    description: 'Reduce video file size while maintaining quality',
    icon: <Compress className="text-4xl" />,
    path: '/video-tools/compress',
  },
  {
    title: 'Convert Video',
    description: 'Convert videos to different formats',
    icon: <Transform className="text-4xl" />,
    path: '/video-tools/convert',
  },
  {
    title: 'Trim Video',
    description: 'Cut and trim video clips',
    icon: <ContentCut className="text-4xl" />,
    path: '/video-tools/trim',
  },
  {
    title: 'Merge Videos',
    description: 'Combine multiple videos into one',
    icon: <Merge className="text-4xl" />,
    path: '/video-tools/merge',
  },
  {
    title: 'Video Settings',
    description: 'Adjust video resolution and quality',
    icon: <VideoSettings className="text-4xl" />,
    path: '/video-tools/settings',
  },
  {
    title: 'Extract Audio',
    description: 'Extract audio from video files',
    icon: <AudioFile className="text-4xl" />,
    path: '/video-tools/extract-audio',
  },
];

const VideoTools = () => {
  return (
    <FeaturePageTemplate
      title="Video Tools"
      description="Free online tools for video processing and conversion"
    >
      <Grid container spacing={3}>
        {tools.map((tool) => (
          <Grid item xs={12} sm={6} md={4} key={tool.path}>
            <Link to={tool.path} style={{ textDecoration: 'none' }}>
              <Paper
                elevation={2}
                sx={{
                  p: 3,
                  height: '100%',
                  borderRadius: 2,
                  transition: 'all 0.2s',
                  '&:hover': {
                    transform: 'translateY(-4px)',
                    boxShadow: 3,
                  },
                }}
              >
                <div
                  style={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    gap: 2,
                  }}
                >
                  {tool.icon}
                  <Typography variant="h6" sx={{ fontWeight: 600 }}>
                    {tool.title}
                  </Typography>
                  <Typography color="text.secondary">
                    {tool.description}
                  </Typography>
                </div>
              </Paper>
            </Link>
          </Grid>
        ))}
      </Grid>
    </FeaturePageTemplate>
  );
};

export default VideoTools;

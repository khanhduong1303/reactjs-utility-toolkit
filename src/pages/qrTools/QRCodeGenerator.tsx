import React, { useState, useEffect, useCallback } from 'react';
import {
  Typography,
  Grid,
  Paper,
  TextField,
  Button,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Box,
  Slider,
  Input,
} from '@mui/material';
import { CloudUpload } from '@mui/icons-material';
import QRCodeStyling from 'qr-code-styling';
import FeaturePageTemplate from '@components/FeaturePageTemplate';

const QR_TYPES = [
  { value: 'text', label: 'Text' },
  { value: 'url', label: 'URL' },
  { value: 'email', label: 'Email' },
  { value: 'phone', label: 'Phone Number' },
  { value: 'sms', label: 'SMS' },
  { value: 'wifi', label: 'WiFi' },
  { value: 'vcard', label: 'vCard Contact' },
  { value: 'mecard', label: 'MeCard Contact' },
  { value: 'geolocation', label: 'Geolocation' },
  { value: 'calendar', label: 'Calendar Event' },
  { value: 'bitcoin', label: 'Bitcoin Address' },
  { value: 'whatsapp', label: 'WhatsApp Message' },
  { value: 'telegram', label: 'Telegram Message' },
  { value: 'twitter', label: 'Twitter Profile' },
  { value: 'facebook', label: 'Facebook Profile' },
  { value: 'linkedin', label: 'LinkedIn Profile' },
];

const DOT_TYPES = [
  { value: 'square', label: 'Square' },
  { value: 'dots', label: 'Dots' },
  { value: 'rounded', label: 'Rounded' },
  { value: 'classy', label: 'Classy' },
  { value: 'classy-rounded', label: 'Classy Rounded' },
  { value: 'extra-rounded', label: 'Extra Rounded' },
];

const CORNER_TYPES = [
  { value: 'square', label: 'Square' },
  { value: 'dot', label: 'Dot' },
  { value: 'extra-rounded', label: 'Extra Rounded' },
];

const CORNER_DOT_TYPES = [
  { value: 'square', label: 'Square' },
  { value: 'dot', label: 'Dot' },
];

const OUTPUT_FORMATS = [
  { value: 'png', label: 'PNG' },
  { value: 'jpeg', label: 'JPEG' },
  { value: 'svg', label: 'SVG' },
];

interface QRCodeGeneratorState {
  type: string;
  value: string;
  wifiNetwork?: string;
  wifiPassword?: string;
  wifiEncryption?: 'WPA' | 'WEP' | 'nopass';
  vCardName?: string;
  vCardOrg?: string;
  vCardTitle?: string;
  vCardEmail?: string;
  vCardPhone?: string;
  vCardAddress?: string;
  vCardUrl?: string;
  emailAddress?: string;
  emailSubject?: string;
  emailBody?: string;
  smsNumber?: string;
  smsMessage?: string;
  geoLatitude?: string;
  geoLongitude?: string;
  eventTitle?: string;
  eventStart?: string;
  eventEnd?: string;
  eventLocation?: string;
  eventDescription?: string;
  size: number;
  style: string;
  cornerStyle: string[];
  fgColor: string;
  bgColor: string;
  includeMargin: boolean;
  errorCorrectionLevel: 'L' | 'M' | 'Q' | 'H';
  centerImage?: File;
  centerImagePreview?: string;
  logoSize: number;
  outputFormat: string;
  downloadSize: string;
  dotType: string;
  cornerType: string;
  cornerDotType: string;
  gradient: {
    enabled: boolean;
    type: 'linear' | 'radial';
    rotation: number;
    colorStops: Array<{ offset: number; color: string }>;
  };
}

const QRCodeGenerator = () => {
  const [state, setState] = useState<QRCodeGeneratorState>({
    type: 'text',
    value: '',
    size: 256,
    style: 'squares',
    cornerStyle: ['square'],
    fgColor: '#000000',
    bgColor: '#ffffff',
    includeMargin: true,
    errorCorrectionLevel: 'H',
    outputFormat: 'png',
    downloadSize: '1024',
    dotType: 'square',
    cornerType: 'square',
    cornerDotType: 'square',
    logoSize: 0.2,
    gradient: {
      enabled: false,
      type: 'linear',
      rotation: 0,
      colorStops: [
        { offset: 0, color: '#000000' },
        { offset: 1, color: '#000000' },
      ],
    },
  });

  const [qrCode, setQrCode] = useState<any>(null);

  const getQRValue = useCallback(() => {
    switch (state.type) {
      case 'wifi':
        return `WIFI:T:${state.wifiEncryption};S:${state.wifiNetwork};P:${state.wifiPassword};;`;
      case 'vcard':
        return `BEGIN:VCARD
VERSION:3.0
FN:${state.vCardName}
ORG:${state.vCardOrg || ''}
TITLE:${state.vCardTitle || ''}
TEL:${state.vCardPhone || ''}
EMAIL:${state.vCardEmail || ''}
ADR:${state.vCardAddress || ''}
URL:${state.vCardUrl || ''}
END:VCARD`;
      case 'email':
        return `mailto:${state.emailAddress}?subject=${encodeURIComponent(
          state.emailSubject || ''
        )}&body=${encodeURIComponent(state.emailBody || '')}`;
      case 'sms':
        return `smsto:${state.smsNumber}:${state.smsMessage || ''}`;
      case 'geolocation':
        return `geo:${state.geoLatitude},${state.geoLongitude}`;
      case 'calendar':
        return `BEGIN:VEVENT
SUMMARY:${state.eventTitle}
DTSTART:${state.eventStart}
DTEND:${state.eventEnd}
LOCATION:${state.eventLocation || ''}
DESCRIPTION:${state.eventDescription || ''}
END:VEVENT`;
      default:
        return state.value;
    }
  }, [
    state.type,
    state.value,
    state.wifiEncryption,
    state.wifiNetwork,
    state.wifiPassword,
    state.vCardName,
    state.vCardOrg,
    state.vCardTitle,
    state.vCardPhone,
    state.vCardEmail,
    state.vCardAddress,
    state.vCardUrl,
    state.emailAddress,
    state.emailSubject,
    state.emailBody,
    state.smsNumber,
    state.smsMessage,
    state.geoLatitude,
    state.geoLongitude,
    state.eventTitle,
    state.eventStart,
    state.eventEnd,
    state.eventLocation,
    state.eventDescription,
  ]);

  const createQRCode = useCallback(() => {
    const newQrCode = new QRCodeStyling({
      width: state.size,
      height: state.size,
      data: getQRValue() || ' ',
      image: state.centerImagePreview,
      dotsOptions: {
        type: state.dotType as any,
        color: state.gradient.enabled ? undefined : state.fgColor,
        gradient: state.gradient.enabled
          ? {
              type: state.gradient.type,
              rotation: state.gradient.rotation,
              colorStops: state.gradient.colorStops,
            }
          : undefined,
      },
      backgroundOptions: {
        color: state.bgColor,
      },
      cornersSquareOptions: {
        type: state.cornerType as any,
        color: state.gradient.enabled ? undefined : state.fgColor,
        gradient: state.gradient.enabled
          ? {
              type: state.gradient.type,
              rotation: state.gradient.rotation,
              colorStops: state.gradient.colorStops,
            }
          : undefined,
      },
      cornersDotOptions: {
        type: state.cornerDotType as any,
        color: state.gradient.enabled ? undefined : state.fgColor,
        gradient: state.gradient.enabled
          ? {
              type: state.gradient.type,
              rotation: state.gradient.rotation,
              colorStops: state.gradient.colorStops,
            }
          : undefined,
      },
      imageOptions: {
        hideBackgroundDots: true,
        imageSize: state.logoSize,
        margin: 0,
        crossOrigin: 'anonymous',
      },
      qrOptions: {
        errorCorrectionLevel: state.errorCorrectionLevel,
        typeNumber: 0,
      },
    });

    setQrCode(newQrCode);

    const container = document.getElementById('qr-code-container');
    if (container) {
      container.innerHTML = '';
      newQrCode.append(container);
    }
  }, [state, getQRValue]);

  useEffect(() => {
    createQRCode();
  }, [createQRCode]);

  const handleStateChange = (field: keyof QRCodeGeneratorState, value: any) => {
    setState((prev) => ({ ...prev, [field]: value }));
  };

  const handleCenterImageUpload = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const file = event.target.files?.[0];
    if (file) {
      const reader = new FileReader();
      reader.onloadend = () => {
        setState((prev) => ({
          ...prev,
          centerImage: file,
          centerImagePreview: reader.result as string,
        }));
      };
      reader.readAsDataURL(file);
    }
  };

  const handleDownload = async () => {
    if (qrCode) {
      const downloadSize = parseInt(state.downloadSize, 10) || 1024;
      qrCode.update({
        width: downloadSize,
        height: downloadSize,
      });

      await qrCode.download({
        name: 'qr-code',
        extension: state.outputFormat,
      });

      qrCode.update({
        width: state.size,
        height: state.size,
      });
    }
  };

  const renderTypeSpecificFields = () => {
    switch (state.type) {
      case 'wifi':
        return (
          <>
            <TextField
              fullWidth
              label="Network Name (SSID)"
              value={state.wifiNetwork || ''}
              onChange={(e) => handleStateChange('wifiNetwork', e.target.value)}
            />
            <TextField
              fullWidth
              label="Password"
              type="password"
              value={state.wifiPassword || ''}
              onChange={(e) =>
                handleStateChange('wifiPassword', e.target.value)
              }
            />
            <FormControl fullWidth>
              <InputLabel>Encryption</InputLabel>
              <Select
                value={state.wifiEncryption || 'WPA'}
                label="Encryption"
                onChange={(e) =>
                  handleStateChange('wifiEncryption', e.target.value)
                }
              >
                <MenuItem value="WPA">WPA/WPA2</MenuItem>
                <MenuItem value="WEP">WEP</MenuItem>
                <MenuItem value="nopass">No Encryption</MenuItem>
              </Select>
            </FormControl>
          </>
        );

      case 'vcard':
        return (
          <>
            <TextField
              fullWidth
              label="Full Name"
              value={state.vCardName || ''}
              onChange={(e) => handleStateChange('vCardName', e.target.value)}
            />
            <TextField
              fullWidth
              label="Organization"
              value={state.vCardOrg || ''}
              onChange={(e) => handleStateChange('vCardOrg', e.target.value)}
            />
            <TextField
              fullWidth
              label="Title"
              value={state.vCardTitle || ''}
              onChange={(e) => handleStateChange('vCardTitle', e.target.value)}
            />
            <TextField
              fullWidth
              label="Email"
              type="email"
              value={state.vCardEmail || ''}
              onChange={(e) => handleStateChange('vCardEmail', e.target.value)}
            />
            <TextField
              fullWidth
              label="Phone"
              value={state.vCardPhone || ''}
              onChange={(e) => handleStateChange('vCardPhone', e.target.value)}
            />
            <TextField
              fullWidth
              label="Address"
              multiline
              rows={2}
              value={state.vCardAddress || ''}
              onChange={(e) =>
                handleStateChange('vCardAddress', e.target.value)
              }
            />
            <TextField
              fullWidth
              label="Website"
              value={state.vCardUrl || ''}
              onChange={(e) => handleStateChange('vCardUrl', e.target.value)}
            />
          </>
        );

      case 'email':
        return (
          <>
            <TextField
              fullWidth
              label="Email Address"
              type="email"
              value={state.emailAddress || ''}
              onChange={(e) =>
                handleStateChange('emailAddress', e.target.value)
              }
            />
            <TextField
              fullWidth
              label="Subject"
              value={state.emailSubject || ''}
              onChange={(e) =>
                handleStateChange('emailSubject', e.target.value)
              }
            />
            <TextField
              fullWidth
              label="Body"
              multiline
              rows={4}
              value={state.emailBody || ''}
              onChange={(e) => handleStateChange('emailBody', e.target.value)}
            />
          </>
        );

      case 'sms':
        return (
          <>
            <TextField
              fullWidth
              label="Phone Number"
              value={state.smsNumber || ''}
              onChange={(e) => handleStateChange('smsNumber', e.target.value)}
            />
            <TextField
              fullWidth
              label="Message"
              multiline
              rows={4}
              value={state.smsMessage || ''}
              onChange={(e) => handleStateChange('smsMessage', e.target.value)}
            />
          </>
        );

      case 'geolocation':
        return (
          <>
            <TextField
              fullWidth
              label="Latitude"
              value={state.geoLatitude || ''}
              onChange={(e) => handleStateChange('geoLatitude', e.target.value)}
            />
            <TextField
              fullWidth
              label="Longitude"
              value={state.geoLongitude || ''}
              onChange={(e) =>
                handleStateChange('geoLongitude', e.target.value)
              }
            />
          </>
        );

      case 'calendar':
        return (
          <>
            <TextField
              fullWidth
              label="Event Title"
              value={state.eventTitle || ''}
              onChange={(e) => handleStateChange('eventTitle', e.target.value)}
            />
            <TextField
              fullWidth
              label="Start Date & Time"
              type="datetime-local"
              value={state.eventStart || ''}
              onChange={(e) => handleStateChange('eventStart', e.target.value)}
              InputLabelProps={{ shrink: true }}
            />
            <TextField
              fullWidth
              label="End Date & Time"
              type="datetime-local"
              value={state.eventEnd || ''}
              onChange={(e) => handleStateChange('eventEnd', e.target.value)}
              InputLabelProps={{ shrink: true }}
            />
            <TextField
              fullWidth
              label="Location"
              value={state.eventLocation || ''}
              onChange={(e) =>
                handleStateChange('eventLocation', e.target.value)
              }
            />
            <TextField
              fullWidth
              label="Description"
              multiline
              rows={4}
              value={state.eventDescription || ''}
              onChange={(e) =>
                handleStateChange('eventDescription', e.target.value)
              }
            />
          </>
        );

      default:
        return (
          <TextField
            fullWidth
            label="Enter Value"
            value={state.value}
            onChange={(e) => handleStateChange('value', e.target.value)}
            multiline={state.type === 'text'}
            rows={state.type === 'text' ? 4 : 1}
          />
        );
    }
  };

  return (
    <FeaturePageTemplate
      title="QR Code Generator"
      description="Create and customize QR codes for various data types"
    >
      <Grid container spacing={4}>
        <Grid item xs={12} md={6}>
          <Paper elevation={1} className="p-6">
            <div className="space-y-6">
              <div className="space-y-4">
                <Typography variant="h6" className="font-bold">
                  Content
                </Typography>
                <FormControl fullWidth>
                  <InputLabel>QR Code Type</InputLabel>
                  <Select
                    value={state.type}
                    label="QR Code Type"
                    onChange={(e) => handleStateChange('type', e.target.value)}
                  >
                    {QR_TYPES.map((type) => (
                      <MenuItem key={type.value} value={type.value}>
                        {type.label}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
                {renderTypeSpecificFields()}
              </div>

              <div className="space-y-4">
                <Typography variant="h6" className="font-bold">
                  Style
                </Typography>
                <div className="grid grid-cols-2 gap-4">
                  <FormControl fullWidth>
                    <InputLabel>Dot Style</InputLabel>
                    <Select
                      value={state.dotType}
                      label="Dot Style"
                      onChange={(e) =>
                        handleStateChange('dotType', e.target.value)
                      }
                    >
                      {DOT_TYPES.map((type) => (
                        <MenuItem key={type.value} value={type.value}>
                          {type.label}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>

                  <FormControl fullWidth>
                    <InputLabel>Corner Style</InputLabel>
                    <Select
                      value={state.cornerType}
                      label="Corner Style"
                      onChange={(e) =>
                        handleStateChange('cornerType', e.target.value)
                      }
                    >
                      {CORNER_TYPES.map((type) => (
                        <MenuItem key={type.value} value={type.value}>
                          {type.label}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </div>

                <FormControl fullWidth>
                  <InputLabel>Corner Dot Style</InputLabel>
                  <Select
                    value={state.cornerDotType}
                    label="Corner Dot Style"
                    onChange={(e) =>
                      handleStateChange('cornerDotType', e.target.value)
                    }
                  >
                    {CORNER_DOT_TYPES.map((type) => (
                      <MenuItem key={type.value} value={type.value}>
                        {type.label}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </div>

              <div className="space-y-4">
                <Typography variant="h6" className="font-bold">
                  Colors
                </Typography>
                <FormControl fullWidth>
                  <InputLabel>Gradient</InputLabel>
                  <Select
                    value={
                      state.gradient.enabled ? state.gradient.type : 'none'
                    }
                    label="Gradient"
                    onChange={(e) => {
                      const value = e.target.value;
                      if (value === 'none') {
                        handleStateChange('gradient', {
                          ...state.gradient,
                          enabled: false,
                        });
                      } else {
                        handleStateChange('gradient', {
                          ...state.gradient,
                          enabled: true,
                          type: value as 'linear' | 'radial',
                        });
                      }
                    }}
                  >
                    <MenuItem value="none">None</MenuItem>
                    <MenuItem value="linear">Linear</MenuItem>
                    <MenuItem value="radial">Radial</MenuItem>
                  </Select>
                </FormControl>

                {state.gradient.enabled ? (
                  <>
                    <Typography gutterBottom>Gradient Rotation</Typography>
                    <Slider
                      value={state.gradient.rotation}
                      onChange={(_, value) =>
                        handleStateChange('gradient', {
                          ...state.gradient,
                          rotation: value as number,
                        })
                      }
                      min={0}
                      max={360}
                      step={1}
                      marks={[
                        { value: 0, label: '0°' },
                        { value: 180, label: '180°' },
                        { value: 360, label: '360°' },
                      ]}
                      valueLabelDisplay="auto"
                    />

                    <div className="grid grid-cols-2 gap-4">
                      <div>
                        <Typography gutterBottom>Start Color</Typography>
                        <Input
                          type="color"
                          value={state.gradient.colorStops[0].color}
                          onChange={(e) =>
                            handleStateChange('gradient', {
                              ...state.gradient,
                              colorStops: [
                                { offset: 0, color: e.target.value },
                                state.gradient.colorStops[1],
                              ],
                            })
                          }
                          fullWidth
                        />
                      </div>
                      <div>
                        <Typography gutterBottom>End Color</Typography>
                        <Input
                          type="color"
                          value={state.gradient.colorStops[1].color}
                          onChange={(e) =>
                            handleStateChange('gradient', {
                              ...state.gradient,
                              colorStops: [
                                state.gradient.colorStops[0],
                                { offset: 1, color: e.target.value },
                              ],
                            })
                          }
                          fullWidth
                        />
                      </div>
                    </div>
                  </>
                ) : (
                  <div className="grid grid-cols-2 gap-4">
                    <div>
                      <Typography gutterBottom>Foreground Color</Typography>
                      <Input
                        type="color"
                        value={state.fgColor}
                        onChange={(e) =>
                          handleStateChange('fgColor', e.target.value)
                        }
                        fullWidth
                      />
                    </div>
                    <div>
                      <Typography gutterBottom>Background Color</Typography>
                      <Input
                        type="color"
                        value={state.bgColor}
                        onChange={(e) =>
                          handleStateChange('bgColor', e.target.value)
                        }
                        fullWidth
                      />
                    </div>
                  </div>
                )}
              </div>

              <div className="space-y-4">
                <Typography variant="h6" className="font-bold">
                  Center Logo
                </Typography>
                <div className="flex items-center space-x-4">
                  <input
                    type="file"
                    accept="image/*"
                    onChange={handleCenterImageUpload}
                    style={{ display: 'none' }}
                    id="logo-upload"
                  />
                  <label htmlFor="logo-upload">
                    <Button
                      variant="outlined"
                      component="span"
                      startIcon={<CloudUpload />}
                    >
                      Upload Logo
                    </Button>
                  </label>
                  {state.centerImagePreview && (
                    <img
                      src={state.centerImagePreview}
                      alt="Center Logo"
                      className="w-12 h-12 object-contain border rounded"
                    />
                  )}
                </div>
                {state.centerImagePreview && (
                  <div className="space-y-2">
                    <Typography gutterBottom>Logo Size</Typography>
                    <Slider
                      value={state.logoSize * 100}
                      onChange={(_, value) =>
                        handleStateChange('logoSize', (value as number) / 100)
                      }
                      min={10}
                      max={30}
                      step={1}
                      marks={[
                        { value: 10, label: '10%' },
                        { value: 20, label: '20%' },
                        { value: 30, label: '30%' },
                      ]}
                      valueLabelDisplay="auto"
                      valueLabelFormat={(value) => `${value}%`}
                    />
                    <Typography variant="body2" color="text.secondary">
                      Adjust logo size while maintaining QR code scanability
                      (recommended: 20%)
                    </Typography>
                  </div>
                )}
              </div>

              <div className="space-y-4">
                <Typography variant="h6" className="font-bold">
                  Export Settings
                </Typography>
                <div className="grid grid-cols-2 gap-4">
                  <FormControl fullWidth>
                    <InputLabel>Format</InputLabel>
                    <Select
                      value={state.outputFormat}
                      label="Format"
                      onChange={(e) =>
                        handleStateChange('outputFormat', e.target.value)
                      }
                    >
                      {OUTPUT_FORMATS.map((format) => (
                        <MenuItem key={format.value} value={format.value}>
                          {format.label}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                  <TextField
                    fullWidth
                    label="Size (px)"
                    type="number"
                    value={state.downloadSize}
                    onChange={(e) =>
                      handleStateChange('downloadSize', e.target.value)
                    }
                    inputProps={{ min: 128, step: 1 }}
                  />
                </div>

                <FormControl fullWidth>
                  <InputLabel>Error Correction</InputLabel>
                  <Select
                    value={state.errorCorrectionLevel}
                    label="Error Correction"
                    onChange={(e) =>
                      handleStateChange('errorCorrectionLevel', e.target.value)
                    }
                  >
                    <MenuItem value="L">Low (7%)</MenuItem>
                    <MenuItem value="M">Medium (15%)</MenuItem>
                    <MenuItem value="Q">Quartile (25%)</MenuItem>
                    <MenuItem value="H">High (30%)</MenuItem>
                  </Select>
                </FormControl>

                <Button
                  variant="contained"
                  onClick={handleDownload}
                  fullWidth
                  size="large"
                  disabled={!getQRValue()}
                >
                  Download QR Code
                </Button>
              </div>
            </div>
          </Paper>
        </Grid>

        <Grid item xs={12} md={6}>
          <Paper elevation={1} className="p-6 sticky top-4">
            <Box className="flex flex-col items-center space-y-4">
              <Typography variant="h6" className="font-bold">
                Preview
              </Typography>
              <div
                id="qr-code-container"
                className="flex justify-center items-center min-h-[256px] w-full"
              />
              <Typography variant="body2" color="text.secondary">
                Preview updates automatically as you make changes
              </Typography>
            </Box>
          </Paper>
        </Grid>
      </Grid>
    </FeaturePageTemplate>
  );
};

export default QRCodeGenerator;

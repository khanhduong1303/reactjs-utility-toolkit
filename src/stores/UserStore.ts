import { makeAutoObservable } from 'mobx';
import { RootStore } from './Stores';

declare global {
  interface Window {
    localStorage: Storage;
  }
}

export class UserStore {
  rootStore: RootStore;
  theme: 'light' | 'dark' = 'light';

  constructor(rootStore: RootStore) {
    makeAutoObservable(this);
    this.rootStore = rootStore;

    // Load theme from localStorage
    const savedTheme = window.localStorage.getItem('theme') as 'light' | 'dark';
    if (savedTheme) {
      this.theme = savedTheme;
    }
  }

  setTheme = (theme: 'light' | 'dark') => {
    this.theme = theme;
    window.localStorage.setItem('theme', theme);
  };
}

export default UserStore;

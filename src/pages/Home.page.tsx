import React from 'react';
import { Container, Typography, Grid, Paper } from '@mui/material';
import { Link } from 'react-router-dom';
import {
  PictureAsPdf,
  Image,
  Videocam,
  TextFields,
  Transform,
} from '@mui/icons-material';
import {
  BannerAd,
  InArticleAd,
  InFeedAd,
} from '@components/AdSense/components';

const tools = [
  {
    title: 'PDF Tools',
    description: 'Compress, extract, split, and rotate PDF files',
    icon: <PictureAsPdf className="text-4xl" />,
    path: '/pdf-tools',
  },
  {
    title: 'Image Tools',
    description: 'Convert, resize, and edit images',
    icon: <Image className="text-4xl" />,
    path: '/image-tools',
  },
  {
    title: 'Video Tools',
    description: 'Convert and edit video files',
    icon: <Videocam className="text-4xl" />,
    path: '/video-tools',
  },
  {
    title: 'OCR Tools',
    description: 'Extract text from images',
    icon: <TextFields className="text-4xl" />,
    path: '/ocr-tools',
  },
  {
    title: 'File Converter',
    description: 'Convert files between different formats',
    icon: <Transform className="text-4xl" />,
    path: '/file-converter',
  },
];

const Home = () => {
  return (
    <Container maxWidth="lg" className="py-8 animate-fade-in">
      <div className="text-center space-y-2 mb-8">
        <Typography variant="h4" className="gradient-text font-bold">
          Welcome to Utility Toolkit
        </Typography>
        <Typography className="text-secondary-600">
          A collection of tools to help you work with files
        </Typography>
        <BannerAd />
      </div>

      <Grid container spacing={4}>
        <Grid item xs={12} md={9}>
          <Grid container spacing={4}>
            {tools.slice(0, 3).map((tool) => (
              <Grid item xs={12} sm={6} md={4} key={tool.title}>
                <Link to={tool.path} className="no-underline">
                  <Paper
                    elevation={1}
                    className="p-6 h-full transition-all duration-300 hover:shadow-lg hover:scale-105 space-y-4 text-center"
                  >
                    <div className="text-primary-600">{tool.icon}</div>
                    <Typography variant="h6" className="font-semibold">
                      {tool.title}
                    </Typography>
                    <Typography variant="body2" className="text-secondary-600">
                      {tool.description}
                    </Typography>
                  </Paper>
                </Link>
              </Grid>
            ))}
          </Grid>

          <div className="my-8">
            <InFeedAd />
          </div>

          <Grid container spacing={4}>
            {tools.slice(3).map((tool) => (
              <Grid item xs={12} sm={6} md={4} key={tool.title}>
                <Link to={tool.path} className="no-underline">
                  <Paper
                    elevation={1}
                    className="p-6 h-full transition-all duration-300 hover:shadow-lg hover:scale-105 space-y-4 text-center"
                  >
                    <div className="text-primary-600">{tool.icon}</div>
                    <Typography variant="h6" className="font-semibold">
                      {tool.title}
                    </Typography>
                    <Typography variant="body2" className="text-secondary-600">
                      {tool.description}
                    </Typography>
                  </Paper>
                </Link>
              </Grid>
            ))}
          </Grid>
        </Grid>

        <Grid item xs={12} md={3}>
          <BannerAd />
          <div className="mt-4">
            <InArticleAd />
          </div>
        </Grid>
      </Grid>

      <div className="mt-8">
        <BannerAd />
      </div>
    </Container>
  );
};

export default Home;

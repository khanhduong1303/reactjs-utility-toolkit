import React, { useState } from 'react';
import {
  Grid,
  Paper,
  TextField,
  Button,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Alert,
  CircularProgress,
  SelectChangeEvent,
} from '@mui/material';
import { SwapHoriz as SwapIcon } from '@mui/icons-material';
import FeaturePageTemplate from '@components/FeaturePageTemplate';
import Notification from '@components/Notification';

interface Language {
  code: string;
  name: string;
}

// Using MyMemory Translation API (free, no key required)
const TRANSLATE_API = 'https://api.mymemory.translated.net';

// Common languages
const LANGUAGES: Language[] = [
  { code: 'en', name: 'English' },
  { code: 'vi', name: 'Vietnamese' },
  { code: 'zh', name: 'Chinese' },
  { code: 'es', name: 'Spanish' },
  { code: 'hi', name: 'Hindi' },
  { code: 'ar', name: 'Arabic' },
  { code: 'bn', name: 'Bengali' },
  { code: 'pt', name: 'Portuguese' },
  { code: 'ru', name: 'Russian' },
  { code: 'ja', name: 'Japanese' },
  { code: 'ko', name: 'Korean' },
  { code: 'fr', name: 'French' },
  { code: 'de', name: 'German' },
  { code: 'it', name: 'Italian' },
  { code: 'nl', name: 'Dutch' },
  { code: 'pl', name: 'Polish' },
  { code: 'tr', name: 'Turkish' },
  { code: 'th', name: 'Thai' },
];

const Translator = () => {
  const [sourceText, setSourceText] = useState('');
  const [translatedText, setTranslatedText] = useState('');
  const [sourceLanguage, setSourceLanguage] = useState('en');
  const [targetLanguage, setTargetLanguage] = useState('vi');
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);
  const [notification, setNotification] = useState({
    open: false,
    message: '',
  });

  const handleTranslate = async () => {
    if (!sourceText) {
      setError('Please enter text to translate');
      return;
    }

    try {
      setLoading(true);
      setError('');

      const langPair = `${sourceLanguage}|${targetLanguage}`;
      const response = await fetch(
        `${TRANSLATE_API}/get?q=${encodeURIComponent(
          sourceText
        )}&langpair=${langPair}`
      );

      if (!response.ok) throw new Error('Translation failed');

      const data = await response.json();
      if (data.responseStatus === 200 && data.responseData?.translatedText) {
        setTranslatedText(data.responseData.translatedText);
      } else {
        throw new Error(data.responseDetails || 'No translation received');
      }
    } catch (err) {
      console.error('Translation error:', err);
      setError('Failed to translate text. Please try again later.');
    } finally {
      setLoading(false);
    }
  };

  const swapLanguages = () => {
    setSourceLanguage(targetLanguage);
    setTargetLanguage(sourceLanguage);
    setSourceText(translatedText);
    setTranslatedText(sourceText);
  };

  const handleSourceLanguageChange = (event: SelectChangeEvent<string>) => {
    setSourceLanguage(event.target.value);
  };

  const handleTargetLanguageChange = (event: SelectChangeEvent<string>) => {
    setTargetLanguage(event.target.value);
  };

  return (
    <FeaturePageTemplate
      title="Translator"
      description="Translate text between different languages"
    >
      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <Grid container spacing={4} justifyContent="center">
        <Grid item xs={12} md={10}>
          <Paper elevation={1} className="p-6">
            <Grid container spacing={3}>
              <Grid item xs={12} md={5}>
                <FormControl fullWidth className="mb-4">
                  <InputLabel>Source Language</InputLabel>
                  <Select
                    value={sourceLanguage}
                    label="Source Language"
                    onChange={handleSourceLanguageChange}
                  >
                    {LANGUAGES.map((lang) => (
                      <MenuItem key={lang.code} value={lang.code}>
                        {lang.name}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
                <TextField
                  multiline
                  rows={12}
                  fullWidth
                  placeholder="Enter text to translate"
                  value={sourceText}
                  onChange={(e) => setSourceText(e.target.value)}
                  className="bg-white/50"
                  sx={{
                    '& .MuiOutlinedInput-root': {
                      fontSize: '1rem',
                      lineHeight: '1.5',
                      minHeight: '300px',
                      '& textarea': {
                        height: '100% !important',
                      },
                    },
                  }}
                />
              </Grid>

              <Grid
                item
                xs={12}
                md={2}
                className="flex items-center justify-center"
              >
                <Button
                  variant="outlined"
                  onClick={swapLanguages}
                  className="my-2 p-3"
                  sx={{
                    minWidth: 'auto',
                    borderRadius: '50%',
                    width: 48,
                    height: 48,
                  }}
                >
                  <SwapIcon />
                </Button>
              </Grid>

              <Grid item xs={12} md={5}>
                <FormControl fullWidth className="mb-4">
                  <InputLabel>Target Language</InputLabel>
                  <Select
                    value={targetLanguage}
                    label="Target Language"
                    onChange={handleTargetLanguageChange}
                  >
                    {LANGUAGES.map((lang) => (
                      <MenuItem key={lang.code} value={lang.code}>
                        {lang.name}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
                <TextField
                  multiline
                  rows={12}
                  fullWidth
                  placeholder="Translation will appear here"
                  value={translatedText}
                  InputProps={{ readOnly: true }}
                  className="bg-white/50"
                  sx={{
                    '& .MuiOutlinedInput-root': {
                      fontSize: '1rem',
                      lineHeight: '1.5',
                      minHeight: '300px',
                      backgroundColor: 'rgba(0, 0, 0, 0.02)',
                      '& textarea': {
                        height: '100% !important',
                      },
                    },
                  }}
                />
              </Grid>

              <Grid item xs={12} className="text-center mt-4">
                <Button
                  variant="contained"
                  onClick={handleTranslate}
                  disabled={loading || !sourceText}
                  className="min-w-[200px] py-3"
                  size="large"
                >
                  {loading ? (
                    <CircularProgress size={24} className="text-white" />
                  ) : (
                    'Translate'
                  )}
                </Button>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
      </Grid>

      <Notification
        open={notification.open}
        message={notification.message}
        onClose={() => setNotification({ ...notification, open: false })}
      />
    </FeaturePageTemplate>
  );
};

export default Translator;

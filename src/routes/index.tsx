import React, { lazy, Suspense } from 'react';
import { Routes, Route } from 'react-router-dom';
import PDFTools from '@pages/pdfTools/PDFTools.page';
import CompressPDF from '@pages/pdfTools/CompressPDF';
import ExtractPages from '@pages/pdfTools/ExtractPages';
import SplitPDF from '@pages/pdfTools/SplitPDF';
import RotatePDF from '@pages/pdfTools/RotatePDF';
import PDFToImage from '@pages/pdfTools/PDFToImage';
import MergePDF from '@pages/pdfTools/MergePDF';
import EditMetadata from '@pages/pdfTools/EditMetadata';
import AddWatermark from '@pages/pdfTools/AddWatermark';
import ConvertPDF from '@pages/pdfTools/ConvertPDF';
import ExtractText from '@pages/pdfTools/ExtractText';
import ProtectPDF from '@pages/pdfTools/ProtectPDF';
import RemovePages from '@pages/pdfTools/RemovePages';
import ImageTools from '@pages/imageTools/ImageTools.page';
import VideoTools from '@pages/videoTools/VideoTools.page';
import Translator from '@pages/translator/Translator.page';
import TimeConverter from '@pages/timeConverter/TimeConverter.page';
import CodeFormatter from '@pages/codeFormatter/CodeFormatter.page';
import FileCompressor from '@pages/fileCompressor/FileCompressor.page';
import UnitConverter from '@pages/unitConverter/UnitConverter.page';
import LengthConverter from '@pages/unitConverter/LengthConverter';
import WeightConverter from '@pages/unitConverter/WeightConverter';
import TemperatureConverter from '@pages/unitConverter/TemperatureConverter';
import SpeedConverter from '@pages/unitConverter/SpeedConverter';
import AreaConverter from '@pages/unitConverter/AreaConverter';
import VolumeConverter from '@pages/unitConverter/VolumeConverter';
import ColorPicker from '@pages/colorPicker/ColorPicker.page';
import OtherTools from '@pages/otherTools/OtherTools.page';
import OCRTools from '@pages/ocrTools/OCRTools.page';
import ResizeImage from '@pages/imageTools/ResizeImage';
import ConvertImage from '@pages/imageTools/ConvertImage';
import EditImage from '@pages/imageTools/EditImage';
import CompressImage from '@pages/imageTools/CompressImage';
import HeicToJpg from '@pages/imageTools/HeicToJpg';
import ImageEffects from '@pages/imageTools/ImageEffects';
import ConvertVideo from '@pages/videoTools/ConvertVideo';
import TrimVideo from '@pages/videoTools/TrimVideo';
import TextTools from '@pages/textTools/TextTools.page';
import URLTools from '@pages/urlTools/URLTools.page';
import CaseConverter from '@pages/textTools/CaseConverter';
import DiffCompare from '@pages/textTools/DiffCompare';
import TextFormatter from '@pages/textTools/TextFormatter';
import SpellChecker from '@pages/textTools/SpellChecker';
import TextTranslator from '@pages/textTools/TextTranslator';
import QuoteGenerator from '@pages/textTools/QuoteGenerator';

const QRTools = lazy(() => import('@pages/qrTools/QRTools.page'));
const QRCodeGenerator = lazy(() => import('@pages/qrTools/QRCodeGenerator'));
const ScanQR = lazy(() => import('@pages/qrTools/ScanQR'));

const AppRoutes = () => {
  return (
    <Routes>
      <Route path="/" element={<PDFTools />} />
      <Route path="/pdf-tools" element={<PDFTools />} />
      <Route path="/pdf-tools/compress" element={<CompressPDF />} />
      <Route path="/pdf-tools/extract" element={<ExtractPages />} />
      <Route path="/pdf-tools/split" element={<SplitPDF />} />
      <Route path="/pdf-tools/rotate" element={<RotatePDF />} />
      <Route path="/pdf-tools/to-image" element={<PDFToImage />} />
      <Route path="/pdf-tools/merge" element={<MergePDF />} />
      <Route path="/pdf-tools/metadata" element={<EditMetadata />} />
      <Route path="/pdf-tools/watermark" element={<AddWatermark />} />
      <Route path="/pdf-tools/convert" element={<ConvertPDF />} />
      <Route path="/pdf-tools/extract-text" element={<ExtractText />} />
      <Route path="/pdf-tools/protect" element={<ProtectPDF />} />
      <Route path="/pdf-tools/remove-pages" element={<RemovePages />} />
      <Route path="/image-tools" element={<ImageTools />} />
      <Route path="/image-tools/resize" element={<ResizeImage />} />
      <Route path="/image-tools/convert" element={<ConvertImage />} />
      <Route path="/image-tools/edit" element={<EditImage />} />
      <Route path="/image-tools/compress" element={<CompressImage />} />
      <Route path="/image-tools/heic-to-jpg" element={<HeicToJpg />} />
      <Route path="/image-tools/effects" element={<ImageEffects />} />
      <Route path="/video-tools" element={<VideoTools />} />
      <Route path="/video-tools/convert" element={<ConvertVideo />} />
      <Route path="/video-tools/trim" element={<TrimVideo />} />
      <Route path="/translator" element={<Translator />} />
      <Route path="/time-converter" element={<TimeConverter />} />
      <Route path="/code-formatter" element={<CodeFormatter />} />
      <Route path="/file-compressor" element={<FileCompressor />} />
      <Route path="/unit-converter" element={<UnitConverter />} />
      <Route path="/unit-converter/length" element={<LengthConverter />} />
      <Route path="/unit-converter/weight" element={<WeightConverter />} />
      <Route
        path="/unit-converter/temperature"
        element={<TemperatureConverter />}
      />
      <Route path="/unit-converter/speed" element={<SpeedConverter />} />
      <Route path="/unit-converter/area" element={<AreaConverter />} />
      <Route path="/unit-converter/volume" element={<VolumeConverter />} />
      <Route path="/color-picker" element={<ColorPicker />} />
      <Route path="/other-tools" element={<OtherTools />} />
      <Route path="/ocr-tools" element={<OCRTools />} />
      <Route
        path="/qr-tools"
        element={
          <Suspense fallback={<div>Loading...</div>}>
            <QRTools />
          </Suspense>
        }
      />
      <Route
        path="/qr-tools/generate"
        element={
          <Suspense fallback={<div>Loading...</div>}>
            <QRCodeGenerator />
          </Suspense>
        }
      />
      <Route
        path="/qr-tools/scan"
        element={
          <Suspense fallback={<div>Loading...</div>}>
            <ScanQR />
          </Suspense>
        }
      />
      <Route path="/text-tools" element={<TextTools />} />
      <Route path="/text-tools/case-converter" element={<CaseConverter />} />
      <Route path="/text-tools/diff-compare" element={<DiffCompare />} />
      <Route path="/text-tools/formatter" element={<TextFormatter />} />
      <Route path="/text-tools/spell-check" element={<SpellChecker />} />
      <Route path="/text-tools/translator" element={<TextTranslator />} />
      <Route path="/text-tools/quote-generator" element={<QuoteGenerator />} />
      <Route path="/url-tools" element={<URLTools />} />
    </Routes>
  );
};

export default AppRoutes;

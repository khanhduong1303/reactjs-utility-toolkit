import React, { useState } from 'react';
import {
  Typography,
  Button,
  Box,
  Alert,
  TextField,
  Slider,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  ToggleButton,
  ToggleButtonGroup,
  Snackbar,
} from '@mui/material';
import {
  CloudUpload as UploadIcon,
  FormatBold,
  FormatItalic,
} from '@mui/icons-material';
import FeaturePageTemplate from '@components/FeaturePageTemplate';

const POSITIONS = [
  { value: 'top-left', label: 'Top Left' },
  { value: 'top-center', label: 'Top Center' },
  { value: 'top-right', label: 'Top Right' },
  { value: 'center-left', label: 'Center Left' },
  { value: 'center', label: 'Center' },
  { value: 'center-right', label: 'Center Right' },
  { value: 'bottom-left', label: 'Bottom Left' },
  { value: 'bottom-center', label: 'Bottom Center' },
  { value: 'bottom-right', label: 'Bottom Right' },
];

const FONTS = [
  'Arial',
  'Times New Roman',
  'Helvetica',
  'Georgia',
  'Verdana',
  'Courier New',
];

const AddWatermark = () => {
  const [selectedFile, setSelectedFile] = useState<File | null>(null);
  const [error, setError] = useState('');
  const [processing, setProcessing] = useState(false);
  const [watermarkText, setWatermarkText] = useState('');
  const [position, setPosition] = useState('center');
  const [opacity, setOpacity] = useState(50);
  const [fontSize, setFontSize] = useState(24);
  const [fontFamily, setFontFamily] = useState('Arial');
  const [textStyle, setTextStyle] = useState<string[]>([]);
  const [open, setOpen] = useState(false);
  const [message, setMessage] = useState('');

  const handleFileUpload = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0];
    if (file) {
      if (file.type.startsWith('image/')) {
        setSelectedFile(file);
        setError('');
      } else {
        setError('Please upload a valid image file');
      }
    }
  };

  const handleTextStyleChange = (
    _: React.MouseEvent<HTMLElement>,
    newStyles: string[]
  ) => {
    setTextStyle(newStyles);
  };

  const handleApply = async () => {
    if (!watermarkText.trim()) {
      setError('Please enter watermark text');
      return;
    }

    // Demo implementation
    setProcessing(true);
    await new Promise((resolve) => setTimeout(resolve, 2000));
    setProcessing(false);
    setMessage(
      'This is a demo version. Watermark feature is not implemented yet.'
    );
    setOpen(true);
  };

  return (
    <FeaturePageTemplate
      title="Add Watermark"
      description="Add text or image watermarks to your images"
    >
      {error && (
        <Alert severity="error" className="mb-4" onClose={() => setError('')}>
          {error}
        </Alert>
      )}

      <Box className="space-y-6">
        <input
          type="file"
          accept="image/*"
          onChange={handleFileUpload}
          className="hidden"
          id="image-upload"
        />
        <label htmlFor="image-upload">
          <Button
            variant="outlined"
            component="span"
            startIcon={<UploadIcon />}
            fullWidth
          >
            Upload Image
          </Button>
        </label>

        {selectedFile && (
          <>
            <Typography variant="body2">
              Selected file: {selectedFile.name}
            </Typography>

            <Box>
              <Typography gutterBottom>Preview</Typography>
              <Box
                sx={{
                  width: '100%',
                  height: 300,
                  bgcolor: 'grey.100',
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  position: 'relative',
                }}
              >
                <Typography color="text.secondary">
                  Preview will be shown here
                </Typography>
                {watermarkText && (
                  <Typography
                    sx={{
                      position: 'absolute',
                      ...getPositionStyle(position),
                      opacity: opacity / 100,
                      fontSize,
                      fontFamily,
                      fontWeight: textStyle.includes('bold')
                        ? 'bold'
                        : 'normal',
                      fontStyle: textStyle.includes('italic')
                        ? 'italic'
                        : 'normal',
                      color: 'white',
                      textShadow: '1px 1px 2px rgba(0,0,0,0.5)',
                      padding: 2,
                    }}
                  >
                    {watermarkText}
                  </Typography>
                )}
              </Box>
            </Box>

            <TextField
              fullWidth
              label="Watermark Text"
              value={watermarkText}
              onChange={(e) => setWatermarkText(e.target.value)}
            />

            <FormControl fullWidth>
              <InputLabel>Position</InputLabel>
              <Select
                value={position}
                label="Position"
                onChange={(e) => setPosition(e.target.value)}
              >
                {POSITIONS.map((pos) => (
                  <MenuItem key={pos.value} value={pos.value}>
                    {pos.label}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <Box>
              <Typography gutterBottom>Opacity</Typography>
              <Slider
                value={opacity}
                onChange={(_, value) => setOpacity(value as number)}
                valueLabelDisplay="auto"
                min={0}
                max={100}
                marks={[
                  { value: 0, label: '0%' },
                  { value: 50, label: '50%' },
                  { value: 100, label: '100%' },
                ]}
              />
            </Box>

            <Box>
              <Typography gutterBottom>Font Size</Typography>
              <Slider
                value={fontSize}
                onChange={(_, value) => setFontSize(value as number)}
                valueLabelDisplay="auto"
                min={12}
                max={72}
                marks={[
                  { value: 12, label: '12px' },
                  { value: 24, label: '24px' },
                  { value: 48, label: '48px' },
                  { value: 72, label: '72px' },
                ]}
              />
            </Box>

            <FormControl fullWidth>
              <InputLabel>Font Family</InputLabel>
              <Select
                value={fontFamily}
                label="Font Family"
                onChange={(e) => setFontFamily(e.target.value)}
              >
                {FONTS.map((font) => (
                  <MenuItem
                    key={font}
                    value={font}
                    style={{ fontFamily: font }}
                  >
                    {font}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <Box>
              <Typography gutterBottom>Text Style</Typography>
              <ToggleButtonGroup
                value={textStyle}
                onChange={handleTextStyleChange}
                aria-label="text style"
              >
                <ToggleButton value="bold" aria-label="bold">
                  <FormatBold />
                </ToggleButton>
                <ToggleButton value="italic" aria-label="italic">
                  <FormatItalic />
                </ToggleButton>
              </ToggleButtonGroup>
            </Box>

            <Button
              variant="contained"
              onClick={handleApply}
              fullWidth
              disabled={processing || !watermarkText.trim()}
            >
              {processing ? 'Processing Image...' : 'Add Watermark'}
            </Button>
          </>
        )}
      </Box>

      <Snackbar
        open={open}
        autoHideDuration={6000}
        onClose={() => setOpen(false)}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
      >
        <Alert onClose={() => setOpen(false)} severity="info">
          {message}
        </Alert>
      </Snackbar>
    </FeaturePageTemplate>
  );
};

const getPositionStyle = (position: string) => {
  switch (position) {
    case 'top-left':
      return { top: 16, left: 16 };
    case 'top-center':
      return { top: 16, left: '50%', transform: 'translateX(-50%)' };
    case 'top-right':
      return { top: 16, right: 16 };
    case 'center-left':
      return { top: '50%', left: 16, transform: 'translateY(-50%)' };
    case 'center':
      return {
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
      };
    case 'center-right':
      return { top: '50%', right: 16, transform: 'translateY(-50%)' };
    case 'bottom-left':
      return { bottom: 16, left: 16 };
    case 'bottom-center':
      return { bottom: 16, left: '50%', transform: 'translateX(-50%)' };
    case 'bottom-right':
      return { bottom: 16, right: 16 };
    default:
      return {};
  }
};

export default AddWatermark;

import React from 'react';
import AdSense from './index';

const MainBannerAd: React.FC = () => (
  <AdSense slot="5962811092" className="mb-8" />
);

export default MainBannerAd;

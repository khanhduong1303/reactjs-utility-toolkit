import { useState } from 'react';

declare global {
  interface Window {
    localStorage: Storage;
  }
}

export const useAds = (feature: string) => {
  const [isLoading, setIsLoading] = useState(false);

  const getUsageCount = (): number => {
    const count = window.localStorage.getItem(`${feature}_usage_count`);
    return count ? parseInt(count, 10) : 0;
  };

  const incrementUsage = () => {
    const count = getUsageCount();
    window.localStorage.setItem(
      `${feature}_usage_count`,
      (count + 1).toString()
    );
  };

  const showAd = async (): Promise<boolean> => {
    setIsLoading(true);
    try {
      // Simulate ad display
      await new Promise((resolve) => setTimeout(resolve, 1000));
      window.localStorage.setItem(`${feature}_last_ad`, Date.now().toString());
      window.localStorage.setItem(`${feature}_usage_count`, '0');
      return true;
    } catch (error) {
      console.error('Error showing ad:', error);
      return false;
    } finally {
      setIsLoading(false);
    }
  };

  return {
    isLoading,
    showAd,
    incrementUsage,
  };
};

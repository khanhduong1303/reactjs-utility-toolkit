import React from 'react';
import AdSense from './index';

const SidebarBannerAd: React.FC = () => (
  <AdSense slot="5962811093" className="sticky top-4" />
);

export default SidebarBannerAd;

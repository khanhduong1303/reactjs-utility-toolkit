import React from 'react';
import { Typography, Grid, Paper } from '@mui/material';
import { Link } from 'react-router-dom';
import {
  PictureAsPdf,
  Image,
  Videocam,
  TextFields,
  Transform,
} from '@mui/icons-material';
import FeaturePageTemplate from '@components/FeaturePageTemplate';

const tools = [
  {
    title: 'Media Tools',
    description: 'Process and convert video & image files',
    tools: [
      {
        name: 'Video Tools',
        icon: <Videocam className="text-4xl text-primary-500" />,
        description: 'Convert, trim, and edit videos',
        path: '/video-tools',
      },
      {
        name: 'Image Tools',
        icon: <Image className="text-4xl text-primary-500" />,
        description: 'Convert, compress, and edit images',
        path: '/image-tools',
      },
    ],
  },
  {
    title: 'Text & Document',
    description: 'Work with text and document files',
    tools: [
      {
        name: 'OCR Tools',
        icon: <TextFields className="text-4xl text-primary-500" />,
        description: 'Extract text from images',
        path: '/ocr-tools',
      },
      {
        name: 'PDF Tools',
        icon: <PictureAsPdf className="text-4xl text-primary-500" />,
        description: 'Convert and edit PDF files',
        path: '/pdf-tools',
      },
    ],
  },
  {
    title: 'Developer Tools',
    description: 'Useful tools for developers',
    tools: [
      {
        name: 'Code Formatter',
        icon: <Transform className="text-4xl text-primary-500" />,
        description: 'Format and beautify code',
        path: '/code-formatter',
      },
      {
        name: 'Unit Converter',
        icon: <Transform className="text-4xl text-primary-500" />,
        description: 'Convert between units',
        path: '/unit-converter',
      },
      {
        name: 'File Compressor',
        icon: <Transform className="text-4xl text-primary-500" />,
        description: 'Compress files and folders',
        path: '/file-compressor',
      },
    ],
  },
  {
    title: 'Design Tools',
    description: 'Tools for designers and creatives',
    tools: [
      {
        name: 'Color Picker',
        icon: <Transform className="text-4xl text-primary-500" />,
        description: 'Pick and convert colors',
        path: '/color-picker',
      },
      {
        name: 'Time Converter',
        icon: <Transform className="text-4xl text-primary-500" />,
        description: 'Convert between time zones',
        path: '/time-converter',
      },
    ],
  },
  {
    title: 'Utilities',
    description: 'General purpose utilities',
    tools: [
      {
        name: 'Translator',
        icon: <Transform className="text-4xl text-primary-500" />,
        description: 'Translate text between languages',
        path: '/translator',
      },
      {
        name: 'File Converter',
        icon: <Transform className="text-4xl text-primary-500" />,
        description: 'Convert between file formats',
        path: '/file-converter',
      },
      {
        name: 'Other Tools',
        icon: <Transform className="text-4xl text-primary-500" />,
        description: 'More useful utilities',
        path: '/other-tools',
      },
    ],
  },
];

const Home = () => {
  return (
    <FeaturePageTemplate
      title="Online Utility Tools"
      description="Free online tools for everyday tasks"
    >
      <Grid container spacing={4}>
        {tools.map((tool) => (
          <Grid item xs={12} sm={6} md={4} key={tool.title}>
            <Link to={tool.tools[0].path} className="no-underline">
              <Paper
                elevation={1}
                className="p-6 h-full transition-all hover:shadow-md"
              >
                <div className="flex flex-col items-center text-center space-y-4">
                  {tool.tools[0].icon}
                  <Typography variant="h6" className="font-bold">
                    {tool.title}
                  </Typography>
                  <Typography className="text-secondary-600">
                    {tool.description}
                  </Typography>
                </div>
              </Paper>
            </Link>
          </Grid>
        ))}
      </Grid>
    </FeaturePageTemplate>
  );
};

export default Home;

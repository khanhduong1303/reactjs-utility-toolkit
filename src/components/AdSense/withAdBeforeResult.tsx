import React, { useState } from 'react';
import { Box, Button, Typography } from '@mui/material';
import { InFeedAd } from './components';

export interface WithAdBeforeResultProps {
  onShowResult?: () => void;
  showAd?: boolean;
}

export const withAdBeforeResult = <P extends object>(
  WrappedComponent: React.ComponentType<P>,
  options: { adTimeout?: number } = {}
) => {
  return function WithAdBeforeResultComponent(
    props: P & WithAdBeforeResultProps
  ) {
    const { onShowResult, showAd = true, ...rest } = props;
    const [hasViewedAd, setHasViewedAd] = useState(false);
    const [isAdLoading, setIsAdLoading] = useState(true);
    const { adTimeout = 3000 } = options;

    const handleAdLoad = () => {
      setIsAdLoading(false);
      // Auto proceed after timeout
      setTimeout(() => {
        setHasViewedAd(true);
        onShowResult?.();
      }, adTimeout);
    };

    if (!showAd) {
      return <WrappedComponent {...(rest as P)} />;
    }

    if (!hasViewedAd) {
      return (
        <Box className="text-center p-4 space-y-4">
          <Typography variant="h6" className="text-secondary-600">
            Please view the advertisement to continue
          </Typography>
          <InFeedAd onLoad={handleAdLoad} />
          {!isAdLoading && (
            <Button
              variant="contained"
              color="primary"
              onClick={() => {
                setHasViewedAd(true);
                onShowResult?.();
              }}
            >
              Continue to Result
            </Button>
          )}
        </Box>
      );
    }

    return <WrappedComponent {...(rest as P)} />;
  };
};

export default withAdBeforeResult;
